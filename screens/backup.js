//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, Platform } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../components/colors';
import { ThemeContext } from '../theme-context';

// create a component
const Backup = ({navigation}) => {

    const { dark, theme, toggle } = React.useContext(ThemeContext);

    return (
        <View style={[styles.container, {backgroundColor: theme.backgroundColor}]}>

            <View style={{flexDirection:'row'}}>
            <TouchableOpacity onPress={()=>navigation.navigate('Settings')}>
            <MaterialCommunityIcons  
            name="arrow-left" color={dark? theme.icon :'#000'} size={30} style={Platform.OS === 'android' ? {padding: 15,}: 
            {padding: 15, paddingTop: 40}}
            />
            </TouchableOpacity>
            <Text style={Platform.OS === 'android' ? styles.header: styles.headerIos}>Backup</Text>
            </View>

            <View style={styles.IconSection}>
            <MaterialCommunityIcons  
            name="cloud-refresh" color='#256173' size={40} style={{padding: 15,}}
            />
            </View>

            <View style={{flexDirection:'row', paddingHorizontal:70, paddingTop: 200}}>
               <Text style={styles.fieldText}>Last Backup:</Text>
               <Text style={{...{paddingHorizontal:30,}, ...styles.fieldText}}>2021-07-20 15:53:11</Text>
            </View>

            <TouchableOpacity>
                <View style={{paddingHorizontal: 90, paddingTop: 20,}}>
                <View style={styles.button}>
                <Text style={styles.buttonText}>Backup/Sync Now</Text>
                </View>
                </View>
            </TouchableOpacity>

        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        //alignItems: 'center',
        //backgroundColor: '#2c3e50',
    },
    IconSection: {
      //padding:5,
      backgroundColor: '#d3d3d3', 
      alignItems: 'center'
    },
    fieldText: {
        fontSize: 14,
        marginTop: 5,
        fontWeight: '600',
        fontFamily: 'WorkSans-Regular',
        lineHeight: 25,
        color: Colors.primary,
    },
    button: {
        backgroundColor: Colors.primary,
        padding: 5,
        //marginTop: 20,
        width: '100%',
        borderRadius:5,
        alignItems:'center'
    },
    buttonText:{
      fontWeight: 'bold',
      color: '#fff',
    },
    header:{
        padding:10, 
        fontFamily:'WorkSans-Regular', 
        fontSize: 20, 
        color: Colors.primary,
    },
    headerIos: {
        padding:15, 
        fontFamily:'WorkSans-Regular', 
        fontSize: 20, 
        color: Colors.primary, 
        paddingTop: 42,
    },
});

//make this component available to the app
export default Backup;
