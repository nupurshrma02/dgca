//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, Platform } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../components/colors';
import { ThemeContext } from '../theme-context';


// create a component
const Gallery = ({navigation}) => {

    const { dark, theme, toggle } = React.useContext(ThemeContext);

    return (
        <ScrollView>
        <View style={[styles.container, {backgroundColor: theme.backgroundColor}]}>

            <View style={{flexDirection:'row'}}>
            <TouchableOpacity onPress={()=>navigation.navigate('Settings')}>
                <MaterialCommunityIcons  
                name="arrow-left" color={dark ? theme.icon : '#000'} size={30} style={Platform.OS === 'android' ? {padding: 15,}: 
                {padding: 15, paddingTop: 40}}
                />
            </TouchableOpacity>
            <Text style={Platform.OS=== 'android' ? styles.header: styles.headerIos}>Gallery</Text>
            </View>
            <Text style={styles.photos}>Photos</Text>
        
        {/* Profile pictures */}
        <View style={styles.headline}>
            <Text style={styles.HeadlineText}>Profile</Text>
        </View>
        <ScrollView  horizontal={true} >
        <View style = {styles.photoSection}>
            { dark ? <Image source = {require('../images/userWhite.png')}
            style={{height:50, width:50}}/> 
            :<Image source = {require('../images/user.png')}
            style={{height:50, width:50}}/>}

            <View style={{paddingHorizontal: 20,}}>
            { dark ? <Image source = {require('../images/userWhite.png')}
            style={{height:50, width:50}}/> 
            :<Image source = {require('../images/user.png')}
            style={{height:50, width:50}}/>}
            </View>

            <View style={{paddingHorizontal: 20,}}>
            { dark ? <Image source = {require('../images/userWhite.png')}
            style={{height:50, width:50}}/> 
            :<Image source = {require('../images/user.png')}
            style={{height:50, width:50}}/>}
            </View>

            <View style={{paddingHorizontal: 20,}}>
            { dark ? <Image source = {require('../images/userWhite.png')}
            style={{height:50, width:50}}/> 
            :<Image source = {require('../images/user.png')}
            style={{height:50, width:50}}/>}
            </View>

            <View style={{paddingHorizontal: 20,}}>
            { dark ? <Image source = {require('../images/userWhite.png')}
            style={{height:50, width:50}}/> 
            :<Image source = {require('../images/user.png')}
            style={{height:50, width:50}}/>}
            </View>

            <View style={{paddingHorizontal: 20,}}>
            { dark ? <Image source = {require('../images/userWhite.png')}
            style={{height:50, width:50}}/> 
            :<Image source = {require('../images/user.png')}
            style={{height:50, width:50}}/>}
            </View>
        </View>
        </ScrollView>

        {/* People pictures */}
        <View style={styles.headline}>
            <Text style={styles.HeadlineText}>People</Text>
        </View>
        <View style = {styles.photoSection}>
            <Text style={{color: theme.color}}>Photos will be shown here...</Text>
        </View>

        {/* Aircraft pictures */}
        <View style={styles.headline}>
            <Text style={styles.HeadlineText}>Aircraft</Text>
        </View>
        <View style = {styles.photoSection}>
            <Text style={{color: theme.color}}>Photos will be shown here...</Text>
        </View>

        {/* Airport pictures */}
        <View style={styles.headline}>
            <Text style={styles.HeadlineText}>Airport</Text>
        </View>
        <View style = {styles.photoSection}>
            <Text style={{color: theme.color}}>Photos will be shown here...</Text>
        </View>

        {/* Docs pictures */}
        <View style={styles.headline}>
            <Text style={styles.HeadlineText}>Docs</Text>
        </View>
        <View style = {styles.photoSection}>
            <Text style={{color: theme.color}}>Photos will be shown here...</Text>
        </View>

        </View>
        </ScrollView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
    },
    photos:{
        fontSize: 28,
        //marginTop:10,
        //fontWeight: 'bold',
        marginHorizontal:15,
        color: Colors.primary,
        fontFamily: 'WorkSans-ExtraBold',
    },
    headline: {
        padding: 20,
        backgroundColor: Colors.primary,
        width: '100%',
        justifyContent:'center',
    },
    HeadlineText:{
        color:'#fff',
        fontSize: 14,
        fontFamily: 'WorkSans-Regular',
    },
    photoSection:{
        padding: 30,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    header:{
        padding:15, 
        fontFamily:'WorkSans-Regular', 
        fontSize: 20, 
        color: Colors.primary,
    },
    headerIos: {
        padding:15, 
        fontFamily:'WorkSans-Regular', 
        fontSize: 20, 
        color: Colors.primary, 
        paddingTop: 42,
    },
});

//make this component available to the app
export default Gallery;
