//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Platform, Linking, TextInput, ScrollView } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import Colors from '../components/colors';
import { ThemeContext } from '../theme-context';

// create a component
const SettingScreen = ({navigation}) => {

    const { dark, theme, toggle } = React.useContext(ThemeContext);

    return (
        <ScrollView>
        <View style={[styles.container, { backgroundColor: theme.backgroundColor }]}>
            <View style={{flexDirection: 'row'}}>
            <MaterialCommunityIcons  
            name="cog-outline" color='#256173' size={50} style={Platform.OS === 'ios' ? {lineHeight:80,}: {lineHeight:57,} } />
            <Text style={Platform.OS === 'android' ? styles.settings : styles.settingsIos}>Settings</Text>
            </View>
            {/* <Text style={styles.mainLine}>Lorem Ipsum</Text> */}
            
            <TouchableOpacity style={styles.fields} onPress={()=> navigation.navigate('Profile')}>
                <MaterialCommunityIcons  
                name="shield-star" color='#256173' size={20} style={{lineHeight:23,}} />
                <Text style={styles.text}> Pilot Details</Text>
                <MaterialCommunityIcons  
                name="help-circle-outline" color='#256173' size={25} style={{lineHeight:23, paddingLeft:220}} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.fields} onPress={()=> navigation.navigate('BuildLogbook')}>
                <MaterialCommunityIcons  
                name="book-open-page-variant" color='#256173' size={20} style={{lineHeight:23,}} />
                <Text style={styles.text}> Build log Book</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.fields} onPress={()=> navigation.navigate('Display')}>
                <MaterialCommunityIcons  
                name="border-style" color='#256173' size={20} style={{lineHeight:23,}} />
                <Text style={styles.text}> Display</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.fields} onPress={()=> navigation.navigate('Gallery')}>
                <MaterialCommunityIcons  
                name="image-multiple" color='#256173' size={20} style={{lineHeight:23,}} />
                <Text style={styles.text}> Gallery</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.fields} onPress={()=> navigation.navigate('Support')}>
                <MaterialCommunityIcons  
                name="headset" color='#256173' size={20} style={{lineHeight:23,}} />
                <Text style={styles.text}> Support</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.fields} onPress={()=> navigation.navigate('Backup')}>
                <MaterialCommunityIcons  
                name="cloud-sync" color='#256173' size={20} style={{lineHeight:23,}} />
                <Text style={styles.text}> Backup</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.fields} onPress={() => {
                     Linking.openURL( 'https://www.youtube.com/results?search_query=narain+aviation' );
                    }}>
                <MaterialCommunityIcons  
                name="video-plus" color='#256173' size={20} style={{lineHeight:23,}} />
                <Text style={styles.text}> Help Videos</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.fields} onPress={()=> {}}>
                <MaterialCommunityIcons  
                name="currency-usd" color='#256173' size={20} style={{lineHeight:23,}} />
                <Text style={styles.text}> Invite & Earn</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.fields} onPress={()=>{ AsyncStorage.clear();
                navigation.replace('Auth');}}>
                <MaterialCommunityIcons  
                name="logout" color='#256173' size={20} style={{lineHeight:23,}} />
                <Text style={styles.text}> Logout</Text>
            </TouchableOpacity>

            <View>
            </View>
        </View>
        </ScrollView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
        padding: 25,
        height: Dimensions.get('window').height,
    },
    settings:{
        fontSize: 28,
        marginTop:10,
        //fontWeight: 'bold',
        marginHorizontal:5,
        color: Colors.primary,
        fontFamily: 'WorkSans-ExtraBold',
    },
    settingsIos: {
        fontSize: 28,
        marginTop:25,
        //fontWeight: 'bold',
        marginHorizontal:5,
        color: Colors.primary,
        fontFamily: 'WorkSans-ExtraBold',
    },
    mainLine:{
        color: Colors.accent,
        marginHorizontal:15,
        marginTop:5,
        fontSize: 15,
        fontFamily: 'WorkSans-VariableFont_wght',
    },
    fields: {
        marginTop: 30,
        borderBottomWidth: 0.2,
        borderBottomColor: Colors.accent,
        width:'100%',
        flexDirection:'row',
    },
    text:{
        marginBottom: 15,
        fontSize: 15,
        color: Colors.primary,
        //fontWeight: 'bold',
        fontFamily:'WorkSans-Regular'
    },
});

//make this component available to the app
export default SettingScreen 
