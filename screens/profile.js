//import liraries
import React, { Component } from 'react';
import {ActivityIndicator, View, Text, StyleSheet, TouchableOpacity, Image, TextInput, Dimensions, Platform, Alert, Modal, Pressable } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../components/colors';
import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
import DropDownPicker from 'react-native-dropdown-picker';
import { RadioButton } from 'react-native-paper';
import { ThemeContext } from '../theme-context';
import AsyncStorage from '@react-native-community/async-storage';
import { rosterImportdata } from '../store/actions/rosterImportsAction';
import SQLite from 'react-native-sqlite-storage';

import {BaseUrl} from '../components/url.json';
import { Row } from 'native-base';
import { useSelector, useDispatch } from 'react-redux';

const db = SQLite.openDatabase(
  {
    name: 'autoflightlogdb.db',
    createFromLocation: 1,
    //location: 'www/autoflightlogdb.db',
  },
  () => {
    //alert('successfully executed');
  },
  error => {
    alert('db error');
  },
);

// create a component
const P1 = ({navigation}) => {

  const dataDispatcher = useDispatch();

  const [animating, setAnimating] = React.useState(true);
  const [name,setName] = React.useState('');
  //const [email,setEmail] = React.useState('');
  const [lt,setLt] = React.useState('');
  const [ln, setLn] = React.useState('');
  const [date, setDate] = React.useState('');
  const [code,setCode] = React.useState('');
  const [mn,setMn] = React.useState('');

  const [op, setOp] = React.useState('');
  const [np, setNp] = React.useState('');
  const [cp, setCp] = React.useState('');
  

  const [egca, setEgca] = React.useState('Commercial');

  const [open, setOpen] = React.useState(false);
  const [value, setValue] = React.useState(null);
  const [items, setItems] = React.useState([
  {label: 'India', value: 'india'},
  {label: 'Afghanistan', value: 'afg'},
  {label: 'Aland islands', value: 'islands'},
  ]);

  const [modalVisible, setModalVisible] = React.useState(false);
  const [eId, setEId] = React.useState('32715')
  const [ePwd, setEPwd] = React.useState('7325211')
  const [airlineValue, setAirlineValue] = React.useState('Indigo');
  const [airline, setAirline] = React.useState([
  {label: 'spicejet', value: 'spicejet'},
  {label: 'Indigo', value: 'Indigo'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  // {label: 'spicejet', value: 'spicejet'},
  ]);
  //const [airline, setAirline] = React.useState([]);

  //Modal fields
  const [fromDate, setFromDate] = React.useState('22-07-2021')
  const [toDate, setToDate] = React.useState('08-08-2021')

  const [roasterData , setRoasterData ] = React.useState([])
  const [dataFetched , setDataFetched ] = React.useState(false)

 // const checkStatus = roasterData === null ? dataFetched : setDataFetched(true)

 //React.useEffect(() => {getAirlines()}, []);
 
//  const getAirlines = () => {
//   db.transaction(tx => {
//     tx.executeSql('SELECT * from Airline_table', [], (tx, result) => {
//       if (result.rows.length > 0) {
//         //alert('data available ');
//         console.log('result', result)
//       }
//       else{
//         alert('helo')
//       }
//       for (let i = 0 ; i <= result.rows.length ; i++) {
//         //console.log('name: ', result.rows.item(i).ident, 'loginlink: ', result.rows.item(i).loginUrl)
//         airline.push({
//           //id: result.rows.item(i).id,
//           airline_name: result.rows.item(i).airline_name,
//           loginUrl : result.rows.item(i).loginUrl,
//         });

//       //console.log(airline);
//       console.log("iddd---->", result.rows.item(i).id)
//       setAirline(airline);
//       }
//     });
//   });
// }

  const checkEcrewFields = () => {
    if(eId !== '' || ePwd !== '' || airlineValue !== null){
      setModalVisible(true)
    }
    else {
      Alert.alert('Please fill the required details')
    }
  }

  const [image, setImage] = React.useState(null);
  const [imageData, setImageData] = React.useState('');
  const [imageFilename, setImageFilename] = React.useState('');

  const profile = async() => {
    let user = await AsyncStorage.getItem('userdetails');
    user = JSON.parse(user);
    console.log('user id=>', user.id);
    //setName(user.name);

    await fetch(BaseUrl+'edit_profile',{
        method : 'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "user_id": user.id,
          "name":name,
          "licance_type": lt,
          "licance_number": ln,
          "validity": date,
          "country" : value,
          "country_code": code,
          "mobile": mn,
          "profile_pic": image,
                      
     })
    }).then(res => res.json())
    .then(resData => {
       console.log(resData);
       Alert.alert(resData.message);
    });
 }

//  const loader = () => {
//    if (roasterData=== null) {
//     <ActivityIndicator
//     animating={animating}
//     color="#000"
//     size="large"
//     style={styles.activityIndicator}
//     />
//    }
//  }

 const Roaster = async() => {
  setDataFetched(true)
  let user = await AsyncStorage.getItem('userdetails');
  user = JSON.parse(user);

    await fetch(BaseUrl+'roasterImport',{
      method : 'POST',
      headers:{
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "user_id": user.id,
        "user": eId, //ned to be dynamic
        "pass": ePwd, //ned to be dynamic
        "airline_type": airlineValue, //ned to be dynamic
        "from": fromDate,
        "to": toDate,
    })
  }).then(res => res.json())
  .then(resData => {
     console.log(resData);
     console.log('data ---->', resData.data)
    //  var data = resData.data.map(function(item) {
    //   return { 
    //     id : item.id,
    //     Dept_date: item.Dept_date,
    //     Dept_place: item.Dept_place,
    //     Dept_time: item.Dept_time,
    //     Arrival_place : item.Arrival_place,
    //     Arrival_time : item.Arrival_time,
    //     Aircraft_Reg: item.Aircraft_Reg,
    //     Aircraft_type : item.Aircraft_type,
    //   };
    // });
    //console.log('mapped data--->',data);
     //setRoasterData(data)
     console.log('roaster data--->' ,roasterData)
     //Alert.alert(resData.message);
     
     dataDispatcher(rosterImportdata({data: resData.data}))
     Alert.alert("Message",'Data fetched successfully');
     setDataFetched(false)
  });
  setDataFetched(false)
}

 const change_pwd = async() => {
  let user = await AsyncStorage.getItem('userdetails');
  user = JSON.parse(user);

  await fetch(BaseUrl+'change_password',{
    method : 'POST',
    headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      "user_id": user.id,
      "old_password":op,
      "new_password": np,
      "confirm_password": cp,                  
 })
}).then(res => res.json())
.then(resData => {
   console.log(resData);
   Alert.alert(resData.message);
});
}

const egca_upload = async() => {
  let user = await AsyncStorage.getItem('userdetails');
  user = JSON.parse(user);

  await fetch(BaseUrl+'update_egca_upload',{
    method : 'POST',
    headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      "user_id": user.id,
      "egca_upload": egca,                
 })
}).then(res => res.json())
.then(resData => {
   console.log(resData);
   Alert.alert(resData.message);
});
}

  const handler = async() => {
    let user = await AsyncStorage.getItem('userdetails');
    user=  JSON.parse(user);
    setName(user.name)
  }
   

    const selectingImage = () => {
        ImagePicker.showImagePicker({quality: 0.3}, responseGet => {
          console.log('Response = ', responseGet);
   
          if (responseGet.didCancel) {
            console.log('User cancelled image picker');
          } else if (responseGet.error) {
            console.log('ImagePicker Error: ', responseGet.error);
          } else {
            const source = {uri: responseGet.uri};
   
            // You can also display the image using data:
            // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            // this.setState({
            //   setImage: source,
            //   imageData: responseGet.data,
            //   imageFilename: responseGet.fileName,
            // });
            console.log(source)
               setImage(source)

          }
        });
      };

      const { dark, theme, toggle } = React.useContext(ThemeContext);

      //dataDispatcher(rosterImportdata({data: roasterData}))

      const getReduxData = useSelector(state => state.rosterImport.data);
      console.log('from pilot details', getReduxData);

      

    return (
        <ScrollView>
        <View style={modalVisible === false ? [{ flex:1, backgroundColor: '#fff',}, {backgroundColor: theme.backgroundColor}]: [{backgroundColor:'rgba(0,0,0,0.3)'}]}>

          <View style={{flexDirection:'row'}}>
          <TouchableOpacity onPress={()=>navigation.navigate('Settings')}>
            <MaterialCommunityIcons  
            name="arrow-left" color={dark ? theme.icon : '#000'} size={30} style={Platform.OS === 'android' ? {padding: 15,}: 
            {padding: 15, paddingTop: 40}} />
          </TouchableOpacity>
          <Text style={Platform.OS=== 'android' ? styles.header: styles.headerIos}> Pilot Details</Text>
          </View>

          {/* headline for personal details */}
          <View style={styles.headline}>
             <Text style={styles.HeadlineText}>Personal details</Text>
          </View>

          {/* Personal details */}
          <View style={styles.centerComponents}>
          <TouchableOpacity onPress={()=>selectingImage()}>
            { dark ? <Image source={
                image === null
                ? require('../images/userWhite.png')
                : image
                } 
                style={{height:70, width:70}}/> 
                :<Image source={
                image === null
                ? require('../images/user.png')
                : image
                } 
                style={{height:70, width:70}}/>}
          </TouchableOpacity>

          <View style={styles.fields}>
             <TextInput 
             placeholder='Name'
             placeholderTextColor = "#266173"
             value={name}
             onChangeText={name=== '' ? handler(): name=>setName(name)}
             />
          </View>

          {/* <View style={styles.fields}>
             <TextInput 
             placeholder='Email'
             placeholderTextColor = "#266173"
             />
          </View> */}

          <View style={{...styles.fields, ...styles.fields1}}>
             <Text style={styles.fieldText}> Licence Type </Text>
             <TextInput 
             placeholder='Licence Type'
             placeholderTextColor = "#393F45"
             value={lt}
             onChangeText={lt => setLt(lt)}
             />
          </View>

          <View style={{...styles.fields, ...styles.fields1}}>
             <Text style={styles.fieldText}> Licence Number </Text>
             <TextInput 
             placeholder='Licence Number'
             placeholderTextColor = "#393F45"
             value={ln}
             onChangeText={ln => setLn(ln)}
             />
          </View>

            <DatePicker
            style={styles.datePickerStyle}
            date={date} // Initial date from state
            mode="date" // The enum of date, datetime and time
            placeholder="validity"
            placeholderTextColor = "#266173"
            format="DD-MM-YYYY"
            //minDate="01-01-2016"
            //maxDate="01-01-2019"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
                dateIcon: {
                //display: 'none',
                position: 'absolute',
                left: 4,
                top: 4,
                marginLeft: 0,
                },
                dateInput: {
                  borderWidth:0.2,
                  borderRadius: 5,
                  borderColor: '393F45',
                  width: '100%',
                  //padding:20,
                },
            }}
            onDateChange={(date) => {
                setDate(date);
            }}
            />
        
        <DropDownPicker
            open={open}
            value={value}
            items={items}
            setOpen={setOpen}
            setValue={setValue}
            setItems={setItems}
            placeholder="Select Country"
            style={[{
                borderWidth: 0.2,
                borderColor: "#393F45",
                marginTop:10
            }, {backgroundColor:theme.backgroundColor}]}
            textStyle={{
                fontSize: 14,
                color: "#266173",
              }}
            dropDownContainerStyle={{backgroundColor: theme.backgroundColor, borderColor: "#266173"}}
        />

        <View style={{flexDirection:'row', 
                      justifyContent:'space-between', 
                      width:'100%'}}>
            <View style={styles.mobileCode}>
            <TextInput 
             placeholder='+91'
             placeholderTextColor = "#266173"
             value={code}
             onChangeText={code => setCode(code)}
             />
             </View>
             <View style={styles.mobile}>
             <TextInput 
             placeholder='Mobile No.'
             placeholderTextColor = "#266173"
             value={mn}
             onChangeText={mn => setMn(mn)}
             />
             </View>
        </View>

            <TouchableOpacity onPress={profile}>
                <View style={styles.button}>
                <Text style={styles.buttonText}>Save</Text>
                </View>
            </TouchableOpacity>

        </View>
        
        {/* headline for change password */}
        <View style={styles.headline}>
             <Text style={styles.HeadlineText}> Change Password</Text>
        </View>

        {/* password details */}
        <View style={styles.centerComponents}>
        <View style={styles.fields}>
             <TextInput 
             placeholder='Old Password'
             placeholderTextColor = "#266173"
             value={op}
             onChangeText={op => setOp(op)}
             />
        </View>
        <View style={styles.fields}>
             <TextInput 
             placeholder='New Password'
             placeholderTextColor = "#266173"
             value={np}
             onChangeText={np => setNp(np)}
             />
        </View>
        <View style={styles.fields}>
             <TextInput 
             placeholder='Confirm Password'
             placeholderTextColor = "#266173"
             value={cp}
             onChangeText={cp => setCp(cp)}
             />
        </View>
        <TouchableOpacity onPress={change_pwd}>
                <View style={styles.button}>
                <Text style={styles.buttonText}>Save</Text>
                </View>
        </TouchableOpacity>
        </View>
        
        {/* heading of Ecrew login  */}
        <View style={styles.headline}>
             <Text style={styles.HeadlineText}> Ecrew-Login</Text>
        </View>

        {/* Ecrew login */}
        <View style={styles.centerComponents}>
        <View style={styles.fields}>
             <TextInput 
             placeholder='Ecrew Id *'
             placeholderTextColor = "#266173"
             value = {eId}  //need to be dynamic
             onChangeText = {(inputText)=> setEId(inputText)}
             />
        </View>
        <View style={styles.fields}>
             <TextInput 
             placeholder='Ecrew Password *'
             placeholderTextColor = "#266173"
             value = {ePwd}
             onChangeText = {(inputText) => setEPwd(inputText)}
             />
        </View>
        <DropDownPicker
            open={open}
            value={airlineValue}
            items={airline}
            setOpen={setOpen}
            setValue={setAirlineValue}
            setItems={setAirline}
            placeholder="Select Airline *"
            style={modalVisible === false?[{
                borderColor: "#266173",
                borderRadius: 5,
                marginTop:10,
                borderWidth: 0.2,
              }, {backgroundColor: theme.backgroundColor}]: [{
              borderColor: "#266173",
              borderRadius: 5,
              marginTop:10,
              borderWidth: 0.2,
              },{backgroundColor:'rgba(0,0,0,0.1)'}]}
            textStyle={{
                fontSize: 14,
                color: "#266173",
              }}
            dropDownContainerStyle={{backgroundColor: theme.backgroundColor, borderColor: "#266173"}}
        />
        
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setModalVisible(!modalVisible);
            }}
            >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
              {/* <Text style={styles.modalText}>Hello World!</Text> */}
              {/* <Text>Import Roster for 3 months</Text> */}
              <DatePicker
                    //style={styles.datePickerStyle}
                    date={fromDate} // Initial date from state
                    mode="date" // The enum of date, datetime and time
                    placeholder="From"
                    placeholderTextColor = "#266173"
                    format= "DD-MM-YYYY"
                    //minDate="01-01-2016"
                    //maxDate="01-01-2019"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    suffixIcon={null}
                    customStyles={{
                        dateInput: {
                        borderWidth:0.2,
                        //borderRadius: 5,
                        borderColor: '#000',
                        width: '100%',
                        //marginRight:-80,
                        //padding:20,
                        },
                        dateIcon: {
                        width:0,
                        height:0,
                        },
                    }}
                    onDateChange={(fromDate) => {
                        setFromDate(fromDate);
                    }}
                    />
                    <DatePicker
                    //style={styles.datePickerStyle}
                    date={toDate} // Initial date from state
                    mode="date" // The enum of date, datetime and time
                    placeholder="To"
                    placeholderTextColor = "#266173"
                    format = "DD-MM-YYYY" 
                    //minDate="01-01-2016"
                    //maxDate="01-01-2019"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    suffixIcon={null}
                    customStyles={{
                        dateInput: {
                        borderWidth:0.2,
                        //borderRadius: 5,
                        borderColor: '#000',
                        width: '100%',
                        //marginRight:-80,
                        //padding:20,
                        },
                        dateIcon: {
                        width:0,
                        height:0,
                        },
                    }}
                    onDateChange={(toDate) => {
                        setToDate(toDate);
                    }}
                    />
                {dataFetched === true ?
                  <ActivityIndicator
                    animating={true}
                    color="#000"
                    size="large"
                    style={styles.activityIndicator}
                    />: 
                    null}
              
              <View style={{flexDirection:'row', justifyContent:'space-around'}}>
              <Pressable
              style={[styles.Modalbutton]}
              onPress={() => setModalVisible(!modalVisible)}
              >
              <Text style={{color: '#fff'}}>cancel</Text>
              </Pressable>
              <Pressable
              style={[styles.Modalbutton, styles.buttonClose]}
              onPress={Roaster}
              >
              <Text style={{color: '#fff'}}>Import Log Data</Text>
              </Pressable>
              </View>

              </View>
              </View>
          </Modal>

            <TouchableOpacity onPress={checkEcrewFields}>
                <View style={styles.button}>
                <Text style={styles.buttonText}>Roaster Import</Text>
                </View>
            </TouchableOpacity>
          
          </View>

        {/* heading of EGCA Upload  */}
        <View style={styles.headline}>
             <Text style={styles.HeadlineText}> EGCA Upload</Text>
        </View>

      {/* Egca upload */}
      <View style={styles.fieldWithoutBottom}>
        <View style={{flexDirection:'row'}}> 
         <RadioButton
            value="Training"
            status={ egca === 'Training' ? 'checked' : 'unchecked' }
            onPress={() => setEgca('Training')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
          <Text style={styles.fieldTextRadio}>Training</Text>
        </View>
        <View style={{flexDirection:'row', paddingLeft:78,}}>
        <RadioButton
            value="Test"
            status={ egca === 'Test' ? 'checked' : 'unchecked' }
            onPress={() => setEgca('Test')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
          <Text style={styles.fieldTextRadio}>Test</Text>
        </View>
      </View>
      <View style={styles.fieldWithoutBottom}>
        <View style={{flexDirection:'row',}}> 
         <RadioButton
            value="Commercial"
            status={ egca === 'Commercial' ? 'checked' : 'unchecked' }
            onPress={() => setEgca('Commercial')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
          <Text style={styles.fieldTextRadio}>Commercial</Text>
        </View>
        <View style={{flexDirection:'row', paddingLeft:50}}>
        <RadioButton
            value="Non-commercial"
            status={ egca === 'Non-commercial' ? 'checked' : 'unchecked' }
            onPress={() => setEgca('Non-commercial')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
          <Text style={styles.fieldTextRadio}>Non-commercial</Text>
        </View>
      </View>

      <TouchableOpacity style={{alignItems:'center'}} onPress={egca_upload}>
                <View style={styles.button}>
                <Text style={styles.buttonText}>Upload</Text>
                </View>
      </TouchableOpacity>

    </View>
        </ScrollView>
    );
};

// define your styles
const styles = StyleSheet.create({
    centerComponents: {
        //flex: 1,
        //justifyContent: 'center',
        alignItems: 'center',
        //marginTop:10,
        padding:30,
        //backgroundColor: '#fff',
    },
    headline: {
        padding: 20,
        backgroundColor: Colors.primary,
        width: '100%',
        justifyContent:'center',
        marginTop:10,
    },
    HeadlineText:{
        color:'#fff',
        fontSize: 14,
        fontFamily: 'WorkSans-Regular',
    },
    fields:{
        borderWidth:0.2,
        borderRadius: 5,
        borderColor: Colors.accent,
        width: '100%',
        marginTop: 5,
        paddingHorizontal: 10,
        paddingVertical: Platform.OS === 'ios' ? 15 : null,
    },
    fieldText: {
        fontSize: 14,
        //marginTop: 5,
        fontWeight: '600',
        fontFamily: 'WorkSans-Regular',
        lineHeight: 45,
        color: Colors.primary,
        },
    fields1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    datePickerStyle: {
        width: '100%',
        borderRadius: 5,
        borderWidth:0.2,
        borderColor: Colors.accent,
        marginTop: 10,
        //padding: 3,
      },
    mobileCode:{
        borderWidth:0.2,
        borderRadius:5,
        borderColor: Colors.accent,
        width: '35%',
        marginTop:10,
        paddingVertical: Platform.OS === 'ios' ? 15 : null,
    },
    mobile:{
        borderWidth:0.2,
        borderRadius:5,
        borderColor: Colors.accent,
        width: '60%',
        marginTop:10,
        paddingLeft:10,
        paddingVertical: Platform.OS === 'ios' ? 15 : null,
    },
    button: {
        backgroundColor: Colors.primary,
        padding: 15,
        marginTop: 20,
        width: Dimensions.get('window').width*0.5,
        borderRadius:10,
        alignItems:'center'
    },
    Modalbutton:{
      backgroundColor: Colors.primary,
        padding: 5,
        marginTop: 20,
        width: Dimensions.get('window').width*0.3,
        borderRadius:10,
        alignItems:'center'
    },
    buttonText:{
      fontWeight: 'bold',
      color: '#fff',
    },
    fieldWithoutBottom: {
        paddingHorizontal:15, 
        paddingVertical:10, 
        width:'100%',
        flexDirection:'row',
        //justifyContent: 'space-evenly',
    },
    fieldTextRadio: {
        fontSize: 14,
        //marginTop: 5,
        fontWeight: '600',
        fontFamily: 'WorkSans-Regular',
        lineHeight: 30,
        color: Colors.primary,
        },
    header:{
          padding:15, 
          fontFamily:'WorkSans-Regular', 
          fontSize: 20, 
          color: Colors.primary,
        },
    headerIos: {
          padding:15, 
          fontFamily:'WorkSans-Regular', 
          fontSize: 20, 
          color: Colors.primary, 
          paddingTop: 42,
        },
    centeredView: {
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          marginTop: 22
        },
    modalView: {
          margin: 20,
          backgroundColor: "white",
          borderRadius: 20,
          padding: 35,
          alignItems: "center",
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 2
          },
          shadowOpacity: 0.25,
          shadowRadius: 4,
          elevation: 5
        },
        activityIndicator: {
          alignItems: 'center',
          height: 80,
        },
});

//make this component available to the app
export default P1;
