//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Linking, Dimensions, Platform } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../components/colors';
import { ThemeContext } from '../theme-context';

// create a component
const Support = ({navigation}) => {

    const { dark, theme, toggle } = React.useContext(ThemeContext);

    return (
        <ScrollView>
        <View style={[styles.container, {backgroundColor: theme.backgroundColor}]}>

        <View style={{flexDirection: 'row'}}>
            <TouchableOpacity onPress={()=>navigation.navigate('Settings')}>
            <MaterialCommunityIcons  
            name="arrow-left" color={dark ? theme.icon : '#000'} size={30} style={Platform.OS === 'android' ? {padding: 0,}: 
            {padding: 15, paddingTop: 40}}/>
            </TouchableOpacity>
            <Text style={Platform.OS === 'android' ? styles.header: styles.headerIos}>Support</Text>
        </View>
            <Text style={styles.Email}>Email Us</Text>
            
            <Text style={styles.mainLine}>Version 3.5.0 Build 1.0</Text>

            <Text style={styles.mainText}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
            when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
            </Text>
            
            {/* supports */}
            <View style={{flexDirection: 'row', marginTop:50, marginHorizontal:15,}}>
                <Text style={styles.supportText}>Techsupport:</Text>
                <Text style={{color: 'blue', paddingHorizontal:50}}
                      onPress={() => Linking.openURL('linkwouldcomehere')}>Lorem Ipsum is simply dummy
                </Text>
            </View>

            <View style={{flexDirection: 'row', marginTop:30, marginHorizontal:15,}}>
                <Text style={styles.supportText}>Subscriptions:</Text>
                <Text style={{color: 'blue', paddingHorizontal:46}}
                      onPress={() => Linking.openURL('linkwouldcomehere')}>Lorem Ipsum is simply dummy
                </Text>
            </View>

            <View style={{flexDirection: 'row', marginTop:30, marginHorizontal:15,}}>
                <Text style={styles.supportText}>Youtube Tutorials:</Text>
                <Text style={{color: 'blue', paddingHorizontal:23}}
                      onPress={() => Linking.openURL('linkwouldcomehere')}>Lorem Ipsum is simply dummy
                </Text>
            </View>

            <View style={{flexDirection: 'row', marginTop:30, marginHorizontal:15,}}>
                <Text style={styles.supportText}>Whatsapp Tech Support:</Text>
                <Text style={{color: 'blue', paddingHorizontal:64}}
                      onPress={() => Linking.openURL('linkwouldcomehere')}>+91-8765736373
                </Text>
            </View>

            <Text style={{...styles.supportText , ...{marginTop:50, marginHorizontal:15}}}>
            Please Help us improve the app with {'\n'}your valuable Feedback
            </Text>
            <View style={{flexDirection: 'row', marginTop:20, marginHorizontal:15,}}>
                <Text style={styles.supportText}>Feedback:</Text>
                <Text style={{color: 'blue', paddingHorizontal:64}}
                      onPress={() => Linking.openURL('linkwouldcomehere')}>Lorem Ipsum is simply dummy
                </Text>
            </View>

            <TouchableOpacity>
                <View style={{padding:10,}}>
                <View style={styles.button}>
                <Text style={styles.buttonText}>Annual Subscription</Text>
                </View>
                </View>
            </TouchableOpacity>
        
        </View>
        </ScrollView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
        padding: 10,
    },
    Email:{
        fontSize: 28,
        //marginTop:10,
        //fontWeight: 'bold',
        marginHorizontal:15,
        color: Colors.primary,
        fontFamily: 'WorkSans-ExtraBold',
    },
    mainLine:{
        color: '#c0c0c0',
        marginHorizontal:15,
        marginTop:5,
        fontSize: 15,
        fontFamily: 'WorkSans-Regular',
    },
    mainText: {
        color: Colors.primary,
        marginHorizontal:15,
        marginTop:30,
        fontSize: 15,
        fontFamily: 'WorkSans-Regular', 
    },
    button: {
        backgroundColor: Colors.primary,
        padding: 15,
        marginTop: 20,
        width: Dimensions.get('window').width* 0.9,
        borderRadius:10,
        alignItems:'center'
    },
    buttonText:{
      fontWeight: 'bold',
      color: '#fff',
    },
    supportText:{
        fontWeight: '600',
        fontFamily: 'WorkSans-Regular',
        color: Colors.primary,
    },
    header:{
        paddingHorizontal:15, 
        fontFamily:'WorkSans-Regular', 
        fontSize: 20, 
        color: Colors.primary,
    },
    headerIos: {
        padding:15, 
        fontFamily:'WorkSans-Regular', 
        fontSize: 20, 
        color: Colors.primary, 
        paddingTop: 42,
    },
});

//make this component available to the app
export default Support;
