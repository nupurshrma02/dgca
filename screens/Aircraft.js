//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TextInput, FlatList, TouchableOpacity, Alert } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { ThemeContext } from '../theme-context';
import AsyncStorage from '@react-native-community/async-storage';
import { ParamsContext } from '../params-context';

import {BaseUrl} from '../components/url.json';
import {Logbook} from '../styles/styles';
import { set } from 'react-native-reanimated';

import { useSelector, useDispatch } from "react-redux";
import {fetchAircrafts} from '../store/actions/aircraftAction';
import {connect} from 'react-redux';
import {addUser} from '../store/actions/aircraftAction'
import {DummyAircrafts} from '../components/dummyAircraft'
import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase(
  {
    name: 'autoflightlogdb.db',
    createFromLocation: 1,
  },
  () => {
    //alert('successfully executed');
  },
  error => {
    alert('db error');
  },
);




// create a component
const Aircraft = ({navigation, route}) => {

 //const {fromScreen} = route.params;
 const [focused,setFocused] = React.useState(false);

 const onFocusChange = () => setFocused(true);
 const onFocusCancelled = () => setFocused(false);

 const [data,setData] = React.useState([]);
 const [filteredData,setFilteredData] = React.useState([]);
 const [search,setSearch] = React.useState('')
 const [selectedId, setSelectedId] = React.useState(null);
 const [An, setAn] = React.useState('')

 const { dark, theme, toggle } = React.useContext(ThemeContext);

 const [, setParams] = React.useContext(ParamsContext);
 const [, setParamsDisplay] = React.useContext(ParamsContext)
 const [, setParamsLogbook] = React.useContext(ParamsContext) 
 const [, setParamsBuildLogbook] = React.useContext(ParamsContext)

 const item = ({item}) => {

 }

//  Fetch_to_buildLogBook = (itemProp,name) => {
//    if(selectedId){
//     navigation.navigate('BuildLogbook',{
//       itemId: itemProp,
//       itemName: name,
//     });
//     const itemid = itemProp;
//     console.log('iddddddddddddd======>>>>>',itemid);
//     console.log('id---->',selectedId);
//   }
// }

const AircraftList = useDispatch(addUser);
console.log('AircrfatList=====', AircraftList)

React.useEffect(() => {getAircrafts()}, [])

// const fetch_buildLogBook = async() => {
//       let user = await AsyncStorage.getItem('userdetails');
//       user = JSON.parse(user);
    
//       await fetch(BaseUrl+'display_logbook',{
//         method : 'POST',
//         headers:{
//             'Accept': 'application/json',
//             'Content-Type': 'application/json'
//         },
//         body: JSON.stringify({
//           "user_id": user.id,               
//       })
//     }).then(res => res.json())
//     .then(resData => {
//       //setBuildLogBook(resData.message);
//       console.log('data---->', resData.message);
//       // for (var j = 0; j < resData.message.length; j++){
//       //     console.log(resData.message[j].aircraft_name);
//       //     setAn(resData.message[j].aircraft_name)
//       //     }
//       setData(resData.message);
//       //setFilteredData(resData.message);
//       console.log('data--------->', data);
//     });
//   };

//   const Search = async() => {
//     let user = await AsyncStorage.getItem('userdetails');
//     user = JSON.parse(user);
  
//     await fetch(BaseUrl+'searchBuildLogbook',{
//       method : 'POST',
//       headers:{
//           'Accept': 'application/json',
//           'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({
//         "aircraft_name" : An,               
//    })
//   }).then(res => res.json())
//   .then(resData => {
//     //setBuildLogBook(resData.message);
//     console.log('data---->', resData.message);
//   });
// };

// const searchFilter = (inputText) => {
//   if(inputText) {
//     const newData = data.filter((item) =>{
//       const itemData = item.aircraft_name ? item.aircraft_name.toUpperCase()
//       : "".toUpperCase();
//       const textData = inputText.toUpperCase();
//       return itemData.indexOf(textData) > -1;
//     });
//     setFilteredData(newData);
//     setSearch(inputText);
// }
// else {
//   setFilteredData(data);
//   setSearch(inputText);
// }
// }

// React.useEffect(() => {searchFilter()}, [])

// const searchFilter = (inputText) => {
//   if(inputText) {
//     const newData = DummyAircrafts.filter((item) =>{
//       const itemData = item.AircraftType ? item.AircraftType.toUpperCase()
//       : "".toUpperCase();
//       const textData = inputText.toUpperCase();
//       return itemData.indexOf(textData) > -1;
//     });
//     setFilteredData(newData);
//     setSearch(inputText);
// }
// else {
//   setFilteredData(DummyAircrafts);
//   setSearch(inputText);
// }
// }

// from sqlite

// React.useEffect(() => {
//   let data = [];
//   db.transaction(tx => {
//     tx.executeSql('SELECT * from AirlineTable', [], (tx, result) => {
//       if (result.rows.length > 0) {
//         alert('data available ');
//         console.log('result', result)
//       }
//       for (let i = 1; i <= result.rows.length; i++) {
//         //console.log('name: ', result.rows.item(i).airline_name, 'loginlink: ', result.rows.item(i).loginUrl)
//         data.push({
//           name: result.rows.item(i).airline_name,
//           id: result.rows.item(i).airline_id,
//           category1: result.rows.item(i).category1,
//           engine1: result.rows.item(i).engine1,
//           engine_name1: result.rows.item(i).engine_name1,
//           Class1: result.rows.item(i).class1,
//         });
//         console.log('sqlite===>',data);
  
//       }
//       //console.log(result);
//       //console.log(result.rows.item(0).airline_name)
//       // result.rows.item.map((index, content) => {
//       //   data.push({name:content.airline_name, loginlink: content.loginUrl})
//       // });
//       // );
//     });
//   });
// },[]);

const searchQuery = (dataToSearch) => {
  let SearchedData = [];
  let SingleResult = '';
  setSearch(dataToSearch)
  console.log('Searching for ', dataToSearch);
  db.transaction(tx => {
    tx.executeSql('SELECT * FROM Aircrafts WHERE AircraftType  LIKE "%'+dataToSearch+'%"', [], (tx, result1) => {
      if (result1.rows.length > 0) {
        //alert('data available ');
        console.log('Searched result raw: ', result1)
        for (let i = 0; i <= result1.rows.length; i++) {
          SingleResult = {
            id : result1.rows.item(i).id,
            type : result1.rows.item(i).AircraftType,
            aircraft_id : result1.rows.item(i).aircraft_id,
            category : result1.rows.item(i).Category,
            engine : result1.rows.item(i).Engine,
            engineName : result1.rows.item(i).EngineName,
            class : result1.rows.item(i).Class,
            crew : result1.rows.item(i).Crew,
          }
          SearchedData.push(SingleResult);
          console.log('single', SingleResult)
          console.log(' Searched data', SearchedData);
          setFilteredData(SearchedData);
        }
        //setFilteredData(SearchedData);
        console.log('Searched Result array: ', SearchedData)
      }else{
        setFilteredData([]);
        console.log('No Data found')
      }
    });
  });
}

const getAircrafts = () => {
  let data = [];
  db.transaction(tx => {
    tx.executeSql('SELECT * from Aircrafts limit 20', [], (tx, result) => {
      console.log(result);
      for (let i = 0 ; i <= result.rows.length ; i++) {
        data.push({
          id:     result.rows.item(i).id,
          type :  result.rows.item(i).AircraftType,
          aircraft_id : result.rows.item(i).aircraft_id,
          category : result.rows.item(i).Category,
          engine : result.rows.item(i).Engine,
          EngineName : result.rows.item(i).EngineName,
          class : result.rows.item(i).Class,
          crew : result.rows.item(i).Crew,
        });
        console.log('Aircrafts',data);
      
      // setData(data);
      setFilteredData(data);
      }
      });
  });
}

//from sqlite

  const Item = ({ item, onPress, backgroundColor, textColor }) => (
    <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>
      <Text style={[styles.title, textColor]}>{item.type}</Text>
    </TouchableOpacity>
  );

const renderItem = ({item}) => {
    //console.log('typeeeee---->',item.AircraftType);
    const backgroundColor = item.id === selectedId ? "#E8E8E8" : "#fff";
    const color ='#000';
    // const fetchToBuildLogBook = item.id === selectedId ? navigation.navigate('BuildLogbook',{
    //   itemId: item.id,
    //   itemName: item.aircraft_name,
    // }) : '';

    const selectParams = () =>{ 
    if(item.id === selectedId && route.params.fromScreen)
    {
      setParams(previousParams => ({
        ...(previousParams || {}),
        childParam: 'value',
        itemId: item.id,
        itemName: item.type,
        itemId : item.aircraft_id,
        itemCategory: item.category,
        itemEngine: item.engine,
        itemEngineName: item.EngineName,
        itemEngineClass: item.class,
        
      }));
      navigation.goBack();
    }
    else if(item.id === selectedId && route.params.fromScreenDisplay)
    {
      setParamsDisplay(previousParams => ({
        ...(previousParams || {}),
        childParam1 : 'value1',
        displayAirType : item.type,
        displayAirId : item.aircraft_id,
      }));
      navigation.goBack();
    }
    else if(item.id === selectedId && route.params.fromScreenLogbook)
    {
      setParamsLogbook(previousParams => ({
        ...(previousParams || {}),
        childParam2 : 'value2',
        logBookAirType : item.type, 
        logBookAircraftId : item.aircraft_id,
      }));
      navigation.goBack();
    }
    else if(item.id === selectedId && route.params.fromScreenBuildLogbbook)
    {
      setParamsBuildLogbook(previousParams => ({
        ...(previousParams || {}),
        childParam3 : 'value3',
        BuildlogBookAirType : item.type,
        BuildlogBookAirId : item.aircraft_id,
        BuildLogbookCategory : item.category,
        BuildLogbookEngine : item.engine,
        BuildLogbookEngineName : item.EngineName,
        BuildLogbookClass : item.class,
        BuildLogbookCrew : item.crew,
      }));
      navigation.goBack();
    }
  }

    return (
      <Item
        item={item}
        onPress={() => {setSelectedId(item.id); selectParams()}}
        backgroundColor={{ backgroundColor }}
        textColor={{ color }}
      />
    );
}

return (
        <View style={[styles.container, {backgroundColor: theme.backgroundColor}]}>
          <View style={styles.header}>
          <MaterialCommunityIcons name="arrow-left" color={'#fff'} size={20} style={{padding:6}} onPress={()=>navigation.goBack()} />
          <Text style={styles.aircrafts}>Aircrafts</Text>
          </View>
            <View style={{backgroundColor:'#F3F3F3', padding:10, flexDirection:'row'}}>
               <View style={(focused) ? styles.searchbar2 : styles.searchbar}>
                 <MaterialCommunityIcons name="magnify" color={'#000'} size={25} style={{padding:6}} />
                 <TextInput 
                 onFocus={onFocusChange}
                 placeholder='Search' 
                 placeholderTextColor = "#D0D0D0"
                 value={search}
                 onChangeText={(inputText)=>searchQuery(inputText)}
                 style={{marginTop: -7, fontSize:15, width:100,}}
                 />
               </View>
               {focused ? <Text style={styles.cancelButton} onPress={onFocusCancelled}>Cancel</Text> : null}
            </View>
            <FlatList
            data={filteredData}
            renderItem={renderItem}
            keyExtractor={(item) => item.type}
            numColumns={1}
            extraData={selectedId}
            />
            <View style={styles.footer}>
            <TouchableOpacity onPress={()=> navigation.navigate('SetAircraft')}>
                <View style={styles.button}>
                <Text style={styles.buttonText}>ADD</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=> navigation.goBack()}>
                <View style={styles.button}>
                <Text style={styles.buttonText}>Dismiss</Text>
                </View>
            </TouchableOpacity>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        //alignItems: 'center',
        //backgroundColor: '#2c3e50',
    },
    header:{
      padding: 5,
      flexDirection: 'row',
      backgroundColor: '#256173'
    },
    aircrafts: {
      fontSize: 15,
      color: '#fff',
      fontWeight: '700',
      fontFamily:'WorkSans-Regular',
      paddingTop: 5
    },
    searchbar: {
      //paddingLeft: 10,
      backgroundColor: '#fff',
      //padding: 10,
      width: '100%',
      //borderRadius: 10,
      flexDirection: 'row',
      //paddingVertical: 10,
    },
    searchbar2: {
        //paddingLeft: 10,
        backgroundColor: '#fff',
        width: '80%',
        //borderRadius: 10,
        flexDirection: 'row',
        //paddingVertical: 10,
      },
      cancelButton: {
          fontSize: 15,
          marginLeft: 10,
          marginTop: 5,
          //paddingHorizontal:150,
      },
      item:{
        padding:20,
        borderBottomWidth:1,
        borderBottomColor:'#E5E5E5',
      },
      footer: {
        padding: 20,
        flexDirection: 'row',
        justifyContent: 'space-around'
      },
      button: {
        backgroundColor: '#256173',
        padding: 15,
        width: Dimensions.get('window').width*0.3,
        borderRadius:30,
        alignItems:'center'
    },
    buttonText:{
      fontWeight: 'bold',
      color: '#fff',
    }
});

//make this component available to the app
export default Aircraft;
