//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput, ScrollView, Dimensions, Platform, Alert, SafeAreaView } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { RadioButton } from 'react-native-paper';
import { ThemeContext } from '../theme-context';
import { MaskedTextInput} from "react-native-mask-text";
import AsyncStorage from '@react-native-community/async-storage';
//import DropDownPicker from 'react-native-dropdown-picker';
import ModalDropdown from 'react-native-modal-dropdown';
import { useScrollToTop } from '@react-navigation/native';
import { ParamsContext } from '../params-context';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import {TimePicker} from 'react-native-simple-time-picker';


// var startTime = moment("12:16:59 am", "HH:mm:ss a"),
// endTime = moment("06:12:07 pm", "HH:mm:ss a");

// var dif = moment.duration(endTime.diff(startTime));
// console.log([dif.hours(), dif.minutes(), dif.seconds()].join(':'));
// console.log('dif in Mins: ', (dif.hours() * 60) + dif.minutes());


import {BaseUrl} from '../components/url.json';

import Colors from '../components/colors';
import { color } from 'react-native-reanimated';
import { Root } from 'native-base';

// let getDifference = (time1, time2) => {
//   let [h1, m1] = time1.split(':')
//   let [h2, m2] = time2.split(':')

//   return ((+h1 + (+m1 / 60)) + (+h2 + (+m2 / 60)))
// }
//   console.log('time---->',getDifference("02:20", "03:48"))



// create a component
const BuildBook = ({navigation, route}) => {

  //const {itemId} = route.params;
  //const {itemName} = route.params;

    const ref = React.useRef(null);
    useScrollToTop(ref);

    const [params] = React.useContext(ParamsContext);

    React.useEffect(() => {
      if (params.childParam) {
        console.log('The value of child param is: ', params.childParam);
        // without edit
        //setCategory(params.BuildLogbookCategory)
        //editable
        editSetEngineName(params.itemEngineName)
        EditSetCategory(params.itemCategory)
        editSetEngine(params.itemEngine)
        editSetClass(params.itemEngineClass)
        editSetday_pic(params.itemDay_pic)
        editSetday_sic(params.itemDay_sic)
        editSetDay_dual(params.itemDay_dual)
        editSetDayp1_us(params.itemDay_p1_us)
        editSetDayp1_ut(params.itemDay_p1_ut)
        editSetNight_pic(params.itemNight_pic)
        editSetNight_sic(params.itemNight_sic)
        editSetNight_dual(params.itemNight_dual)
        editSetNightp1_us(params.itemNight_p1_us)
        editSetNightp1_ut(params.itemNight_p1_ut)
        editSetActual(params.itemInstrumental_time_actual)
        editSetSimulated(params.itemInstrumental_time_simulated)
        editSetSimulator(params.itemSimulator)
        editSetInstructional_day(params.itemInstructional_flying_day)
        editSetInstructional_night(params.itemInstructional_flying_night)
        editSetDay_to(params.itemDay_to)
        editSetNight_to(params.itemNight_to)
        editSetDayLanding(params.itemDayLanding)
        editSetNightLanding(params.itemNightLanding)
        editSetRemark(params.itemRemark)
        editSetDay_total(params.itemDay_total)
        editSetNight_total(params.itemNight_total)
        editSetTotal_flyingTime(params.itemTotal_flying_time)
        editSetInstructional_total(params.itemInstructional_flying_total)
      }
    }, [params]);

    React.useEffect(() => {
      if (params.childParam3) {
        console.log('The value of child param is: ', params.childParam3);
        // without edit
        setCategory(params.BuildLogbookCategory)
        setEngine(params.BuildLogbookEngine)
        setEngineName(params.BuildLogbookEngineName)
        setClass(params.BuildLogbookClass)
      }
    }, [params]);

    //const[id,setId] = React.useState('')
    const[edit, setEdit] = React.useState(false);

    const[data, setData] = React.useState([]);
    const[aircraftType, setAircraftType] = React.useState('');
    const[engineName, setEngineName] = React.useState('');

    //Time
    const[day_pic, setday_pic] = React.useState('');
    const[day_sic, setday_sic] = React.useState('');
    const[dayp1_us, setDayp1_us] = React.useState('');
    const[dayp1_ut, setDayp1_ut] = React.useState('');
    const[day_total, setDay_total] = React.useState('');
    const[night_pic, setnight_pic] = React.useState('');
    const[night_sic, setnight_sic] = React.useState('');
    const[nightp1_us, setNightp1_us] = React.useState('');
    const[nightp1_ut, setNightp1_ut] = React.useState('');
    const[night_total, setNight_total] = React.useState('');
    const[day_dual,setDay_dual] = React.useState('');
    const[night_dual,setNight_dual] = React.useState('');
    const[total_flying_time, setTotal_flyingTime] = React.useState('');
    const[actual, setActual] = React.useState('');
    const[simulated, setSimulated] = React.useState('');
    const[simulator, setSimulator] = React.useState('');
    const[instructional_day, setInstructional_day] = React.useState('');
    const[instructional_night, setInstructional_night] = React.useState('');
    const[instructional_total, setInstructional_total] = React.useState('');
    const [stl_day, setStl_Day] = React.useState('')
    const [stl_night, setStl_Night] = React.useState('')
    const [stl_total, setStl_Total] = React.useState('')
    //const cat =  params.itemCategory

    console.log('Saved Aircraft name==>', params.itemCategory);
    const [category, setCategory] = React.useState('');
    const [engine, setEngine] = React.useState('');
    const [Class, setClass] = React.useState('');

    console.log('class', Class)

    const[day_to, setDay_to] = React.useState('');
    const[dayLanding, setDayLanding] = React.useState('');
    const[night_to, setNight_to] = React.useState('');
    const[nightLanding, setNightLanding] = React.useState('');
    const[remark, setRemark] = React.useState('');

    //Editable fields
    const [editCategory, EditSetCategory] = React.useState('')
    const [editEngine, editSetEngine] = React.useState('')
    const [editClass, editSetClass] = React.useState('')
    const [editEngineName, editSetEngineName] = React.useState(params.itemEngineName)

    //editableTime
    const[editDay_pic, editSetday_pic] = React.useState('');
    const[editDay_sic, editSetday_sic] = React.useState('');
    const[editDayp1_us, editSetDayp1_us] = React.useState('');
    const[editDayp1_ut, editSetDayp1_ut] = React.useState('');
    const[editDay_total, editSetDay_total] = React.useState('');
    const[editNight_pic, editSetNight_pic] = React.useState('');
    const[editNight_sic, editSetNight_sic] = React.useState('');
    const[editNightp1_us, editSetNightp1_us] = React.useState('');
    const[editNightp1_ut, editSetNightp1_ut] = React.useState('');
    const[editNight_total, editSetNight_total] = React.useState('');
    const[editDay_dual, editSetDay_dual] = React.useState('');
    const[editNight_dual,editSetNight_dual] = React.useState('');
    const[editTotal_flying_time, editSetTotal_flyingTime] = React.useState('');
    const[editActual, editSetActual] = React.useState('');
    const[editSimulated, editSetSimulated] = React.useState('');
    const[editSimulator, editSetSimulator] = React.useState('');
    const[editInstructional_day, editSetInstructional_day] = React.useState('');
    const[editInstructional_night, editSetInstructional_night] = React.useState('');
    const[editInstructional_total, editSetInstructional_total] = React.useState('');

    const[editDay_to, editSetDay_to] = React.useState('');
    const[editNight_to, editSetNight_to] = React.useState('');
    const[editDayLanding, editSetDayLanding] = React.useState('');
    const[editNightLanding, editSetNightLanding] = React.useState('');
    const[editRemark, editSetRemark] = React.useState('');
 
    

      const editable = () => {
        setEdit(edit => !edit); 
      };

      //Api to delete the buildlogbook
      const Delete = async() => {
        let user = await AsyncStorage.getItem('userdetails');
        user = JSON.parse(user);
      
        await fetch(BaseUrl+'delete_logbook',{
          method : 'POST',
          headers:{
              'Accept': 'application/json',
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            "user_id": user.id,
            "id": params.itemId,
          })
      }).then(res => res.json())
      .then(resData => {
         console.log(resData);
         Alert.alert(resData.message);
      });
      }

      //Api to get the logbook

      //React.useEffect(() => {getLogBook()}, [category]);

      const getLogBook = async() => {
        await fetch(BaseUrl+'getBuildLogbook',{
          method : 'POST',
          headers:{
              'Accept': 'application/json',
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            "id": params.itemId,
          })
      }).then(res => res.json())
      .then(resData => {
         console.log('getLogbookData',resData);
         for (var j = 0; j < resData.data.length; j++){
           console.log('id', resData.data[j].id)
         }
        });
      }

    //Api to edit the logbook
    const Edit = async() => {
      let user = await AsyncStorage.getItem('userdetails');
      user = JSON.parse(user);
    
      await fetch(BaseUrl+'update_logbook',{
        method : 'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "user_id": user.id,
          "id": params.itemId,
          "aircraft_name": params.itemName,
          "category": editCategory,
          "engine": editEngine,
          "engine_name": editEngineName,
          "engine_class": editClass,
          "day_pic":editDay_pic,
          "day_sic":editDay_sic,
          "day_p1_us":editDayp1_us,
          "day_p1_ut":editDayp1_ut,
          "day_dual":editDay_dual,
          "total":editDay_total,
          "night_pic":editNight_pic,
          "night_sic":editNight_sic,
          "night_p1_us":editNightp1_us,
          "night_p1_ut":editNightp1_ut,
          "night_dual":editNight_dual,
          "night_total":editNight_total,
          "total_flying_time":editTotal_flying_time,
          "instrumental_time_actual":editActual,
          "instrumental_time_simulated":editSimulated,
          "simulator":editSimulator,
          "instructional_flying_day":editInstructional_day,
          "instructional_flying_night":editInstructional_night,
          "instructional_flying_total":editInstructional_total,
          "day_to":editDay_to,
          "night_to":editNight_to,
          "day_landing":editDayLanding,
          "night_landing":editNightLanding,
          "remark":editRemark,

        })
    }).then(res => res.json())
    .then(resData => {
       console.log(resData);
       Alert.alert(resData.message);
    });
    }

// Dark Theme
const { dark, theme, toggle } = React.useContext(ThemeContext);
    

    //Time Calculation for Day 
    var Daytime1 = day_pic === '' ? '00:00' : day_pic ;
    var Daytime2 = day_sic === '' ? '00:00' : day_sic;
    var Daytime3 = dayp1_us === '' ? '00:00' : dayp1_us;
    var Daytime4 = dayp1_ut === '' ? '00:00' : dayp1_ut  ;
    var Daytime5 = day_dual === '' ? '00:00' : day_dual;

    var hour_day_me=0;
    var minute_day_me=0;
    //var second=0;

    var splitTime1= Daytime1.split(':');
    var splitTime2= Daytime2.split(':');
    var splitTime3= Daytime3.split(':');
    var splitTime4= Daytime4.split(':');
    var splitTime5= Daytime5.split(':');

    if (splitTime1.length == 0 ? splitTime1[1] ='00' : splitTime1[1])

    hour_day_me = parseInt(splitTime1[0])+parseInt(splitTime2[0])+parseInt(splitTime3[0])+parseInt(splitTime4[0]);
    minute_day_me = parseInt(splitTime1[1])+parseInt(splitTime2[1])+parseInt(splitTime3[1])+parseInt(splitTime4[1]);
    hour_day_me = Math.trunc(hour_day_me + minute_day_me/60);
    minute_day_me = minute_day_me%60;

    const meTime_day = +hour_day_me+':'+minute_day_me;

    var hour_day_se=0;
    var minute_day_se=0;

    hour_day_se = parseInt(splitTime1[0])+parseInt(splitTime5[0]);
    minute_day_se = parseInt(splitTime1[1])+parseInt(splitTime5[1]); 
    hour_day_se = Math.trunc(hour_day_se + minute_day_se/60);
    minute_day_se = minute_day_se%60;

    //Alert.alert('sum of above time= '+hour+':'+minute);
    const seTime_day = +hour_day_se+':'+minute_day_se;

    //Time Calculation for Night
    var NightTime1 = night_pic === '' ? '00:00' : night_pic ;
    var NightTime2 = night_sic === '' ? '00:00' : night_sic;
    var NightTime3 = nightp1_us === '' ? '00:00' : nightp1_us;
    var NightTime4 = nightp1_ut === '' ? '00:00' : nightp1_ut;
    var NightTime5 = night_dual === '' ? '00:00' : night_dual;

    var hour_night_me=0;
    var minute_night_me=0;
    //var second=0;

    var NsplitTime1= NightTime1.split(':');
    var NsplitTime2= NightTime2.split(':');
    var NsplitTime3= NightTime3.split(':');
    var NsplitTime4= NightTime4.split(':');
    var NsplitTime5= NightTime5.split(':');

    hour_night_me = parseInt(NsplitTime1[0])+parseInt(NsplitTime2[0])+parseInt(NsplitTime3[0])+parseInt(NsplitTime4[0]);
    minute_night_me = parseInt(NsplitTime1[1])+parseInt(NsplitTime2[1])+parseInt(NsplitTime3[1])+parseInt(NsplitTime4[1]);
    hour_night_me = Math.trunc(hour_night_me + minute_night_me/60);
    minute_night_me = minute_night_me%60;

    const meTime_night = +hour_night_me+':'+minute_night_me;

    var hour_night_se=0;
    var minute_night_se=0;

    hour_night_se = parseInt(NsplitTime1[0])+parseInt(NsplitTime5[0]);
    minute_night_se = parseInt(NsplitTime1[1])+parseInt(NsplitTime5[1]); 
    hour_night_se = Math.trunc(hour_night_se + minute_night_se/60);
    minute_night_se = minute_night_se%60;

    //Alert.alert('sum of above time= '+hour+':'+minute);
    const seTime_night = +hour_night_se+':'+minute_night_se;

    //Total Day/Night
    var TotalFlyingTime1ME = meTime_day;
    var TotalFlyingTime2ME = meTime_night;
    var TotalFlyingTime3SE = seTime_day;
    var TotalFlyingTime4SE = seTime_night;

    var hour_total_me=0;
    var minute_total_me=0;
    var hour_total_se=0;
    var minute_total_se=0;

    var TsplitTime1ME= TotalFlyingTime1ME.split(':');
    var TsplitTime2ME= TotalFlyingTime2ME.split(':');

    var TsplitTime3SE= TotalFlyingTime3SE.split(':');
    var TsplitTime4SE= TotalFlyingTime4SE.split(':');

    hour_total_me = parseInt(TsplitTime1ME[0])+parseInt(TsplitTime2ME[0]);
    minute_total_me = parseInt(TsplitTime1ME[1])+parseInt(TsplitTime2ME[1]); 
    hour_total_me = Math.trunc(hour_total_me + minute_total_me/60);
    minute_total_me = minute_total_me%60;

    hour_total_se = parseInt(TsplitTime3SE[0])+parseInt(TsplitTime4SE[0]);
    minute_total_se = parseInt(TsplitTime3SE[1])+parseInt(TsplitTime4SE[1]); 
    hour_total_se = Math.trunc(hour_total_se + minute_total_se/60);
    minute_total_se = minute_total_se%60; 

    const TotalTime_me = +hour_total_me+':'+minute_total_me;
    const TotalTime_se = +hour_total_se+':'+minute_total_se;

    //instructional timings
    var instructional_dayTime = instructional_day === '' ? '00:00' : instructional_day;
    var instructional_NightTime = instructional_night === '' ? '00:00' : instructional_night;

    var hour = 0;
    var minute = 0;

    var split_instructional_day= instructional_dayTime.split(':');
    var split_instructional_night= instructional_NightTime.split(':');


    hour = parseInt(split_instructional_day[0])+parseInt(split_instructional_night[0]);
    minute = parseInt(split_instructional_day[1])+parseInt(split_instructional_night[1]); 
    hour = Math.trunc(hour + minute/60);
    minute = minute%60;

    const total_instructional = +hour+':'+minute;

    //api to upload the logbook
    const upload_logbook = async() => {
      let user = await AsyncStorage.getItem('userdetails');
      user = JSON.parse(user);
      console.log('user id=>', user.id);
      //setName(user.name);
  
      await fetch(BaseUrl+'add_logbook',{
          method : 'POST',
          headers:{
              'Accept': 'application/json',
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            "user_id": user.id,
            //"aircraftPhoto": 'hello',
            "aircraft_name": params.BuildlogBookAirType,
            "category": category,
            "engine": engine,
            "engine_name": engineName,
            "engine_class" : Class,
            "day_pic" : day_pic,
            "day_sic" : day_sic,
            "day_dual" : day_dual,
            "day_p1_us" : dayp1_us,
            "day_p1_ut" : dayp1_ut,
            "total": Class === 'seLand' || Class=== 'seSea' ? seTime_day : meTime_day,
            //"total" : day_total,
            "night_pic": night_pic,
            "night_sic" : night_sic,
            "night_dual" : night_dual,
            "night_p1_us" : nightp1_us,
            "night_p1_ut" : nightp1_ut,
            "night_total" : Class === 'seLand' || Class=== 'seSea' ? seTime_night : meTime_night,
            //"night_total": night_total,
            "total_flying_time" : Class === 'seLand' || Class=== 'seSea' ? TotalTime_se : TotalTime_me,
            //"total_flying_time": total_flying_time,
            "instrumental_time_actual" : actual,
            "instrumental_time_simulated" : simulated,
            "simulator" : simulator,
            "instructional_flying_day" : instructional_day,
            "instructional_flying_night": instructional_night,
            "instructional_flying_total": total_instructional,
            //"instructional_flying_total" : instructional_total,
            "day_to" : day_to,
            "night_to" : night_to,
            "day_landing" : dayLanding,
            "night_landing" : nightLanding,
            "remark" : remark,
            "stl_day" : stl_day,
            "stl_night" : stl_night,
            "stl_total" : totalStl,

       })
      }).then(res => res.json())
      .then(resData => {
         console.log(resData);
         aircraftType === '' ? Alert.alert('Aircraft Type is required'): '';
         if(resData.message === 'Record inserted successfully .'){
         Alert.alert(resData.message);
         }
         else{
         Platform.OS === 'ios' ? Alert.alert(resData.error) : ''; 
         }
      });
   }

  // stl hours calculation
   const resp = {
    data: [
      {tempoGasto: stl_day === '' ? '00:00' : stl_day},
      {tempoGasto: stl_night === '' ? '00:00' : stl_night},
    ]
  };
  
  const sumHoras = [0, 0];
  
  for (let i=0; i < resp.data.length; i++){
    const [hours, minutes] = resp.data[i].tempoGasto.split(':').map(s => parseInt(s, 10));
    
    // hours
    sumHoras[0] += hours;
    
    // minutes
     
    if (sumHoras[1] > 59){
      Alert.alert('invalid input')
    }
    else if ((sumHoras[i] + minutes) > 59) {
      const diff = sumHoras[1] + minutes - 60;
      sumHoras[0] += 1;
      sumHoras[1] = diff;
      Alert.alert('minutes should be valid')
    }
    else if (isNaN(minutes)){
      sumHoras[1] = sumHoras[1] 
    }
    else {
      sumHoras[1] += minutes;
    }
  }
  
  const totalStl =sumHoras[1] === 0 ? sumHoras.join(':'+'0') :sumHoras.join(':');


  return (
    <SafeAreaView>
    <ScrollView ref={ref}>
    <View style={[styles.container, {backgroundColor: theme.backgroundColor}]}>

    <View style={{flexDirection:'row'}}>
    <TouchableOpacity onPress={()=>navigation.navigate('Settings')}>
        <MaterialCommunityIcons  
        name="arrow-left" color={dark ? theme.icon : '#000'} size={30} style={Platform.OS === 'android' ? {padding: 15,}: 
        {padding: 15, paddingTop: 40}}
        />
    </TouchableOpacity>
    <Text style={Platform.OS === 'android' ? styles.header: styles.headerIos}>Build-Logbook</Text>
    </View>
    
    
    <View style={styles.headline}>
          <Text style={styles.HeadlineText}>First Aircraft <Text style={{color:'red'}}>*</Text></Text>
    </View>

    <View style={{...styles.fieldWithoutBottom, ...styles.otherEnd, ...{zIndex:99}}}>
           <TouchableOpacity onPress={getLogBook}><Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Aircraft Type</Text></TouchableOpacity>
                {edit?<View style={{flexDirection:'row'}}>
                  <Text style={{marginTop:8, color:Colors.primary}}>{params.itemName?params.itemName:<Text>Select AirType</Text> }</Text>
                  <TouchableOpacity onPress={()=>navigation.navigate('Aircraft', {fromScreen:'BuildLogBook'})}>
                  <MaterialCommunityIcons 
                  name="arrow-down-drop-circle-outline" color={'#256173'} size={18} style={{marginTop:8, paddingHorizontal:3}} />
                  </TouchableOpacity>
                  </View>:
                  <TouchableOpacity onPress={()=>navigation.navigate('Aircraft',{fromScreenBuildLogbbook:'BL'})}>
                  <Text style={styles.fieldText1}>{params.BuildlogBookAirType?params.BuildlogBookAirType:<Text>AirCraft Type</Text> }</Text>
                  </TouchableOpacity>}
    </View>

    <View style={styles.headline}>
          <Text style={styles.HeadlineText}>Category <Text style={{color:'red'}}>*</Text></Text>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={{flexDirection:'row'}}> 
        <RadioButton.Group value={edit?editCategory:category} 
         onValueChange={edit?editCategory=>EditSetCategory(editCategory):category=>setCategory(category)}>
         <RadioButton
            value={"airPlane"}
            //status={category === 'airPlane' ? 'checked' : 'unchecked' }
            //onPress={() => setCategory('airPlane')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
        </RadioButton.Group>
          <Text style={styles.fieldTextRadio}>Air Plane</Text>
        </View>
        <View style={{flexDirection:'row', paddingLeft:78,}}>
        <RadioButton.Group value={edit?editCategory:category} 
          onValueChange={edit?editCategory=>EditSetCategory(editCategory):category=>setCategory(category)}>
          <RadioButton
            value="microlight"
            //status={ category === 'microlight' ? 'checked' : 'unchecked' }
            //onPress={() => setCategory('microlight')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
          </RadioButton.Group>
          <Text style={styles.fieldTextRadio}>Microlight</Text>
        </View>
      </View>
      <View style={styles.fieldWithoutBottom}>
        <View style={{flexDirection:'row',}}>
        <RadioButton.Group value={edit?editCategory:category} 
          onValueChange={edit?editCategory=>EditSetCategory(editCategory):category=>setCategory(category)}> 
         <RadioButton
            value="Helicopter"
            //status={ category === 'helicopter' ? 'checked' : 'unchecked' }
            //onPress={() => setCategory('helicopter')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
          </RadioButton.Group>
          <Text style={styles.fieldTextRadio}>Helicopter</Text>
        </View>
        <View style={{flexDirection:'row', paddingLeft:70}}>
        <RadioButton.Group value={edit?editCategory:category} 
          onValueChange={edit?editCategory=>EditSetCategory(editCategory):category=>setCategory(category)}>
        <RadioButton
            value="glider"
            //status={ category === 'glider' ? 'checked' : 'unchecked' }
            //onPress={() => setCategory('glider')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
          </RadioButton.Group>
          <Text style={styles.fieldTextRadio}>Glider</Text>
        </View>
      </View>

    <View style={styles.headline}>
          <Text style={styles.HeadlineText}>Engine <Text style={{color:'red'}}>*</Text></Text>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={{flexDirection:'row'}}> 
        <RadioButton.Group value={edit?editEngine:engine} 
         onValueChange={edit?editEngine=>editSetEngine(editEngine):engine=>setEngine(engine)}>
         <RadioButton
            value="Jet"
            //status={ engine === 'jet' ? 'checked' : 'unchecked' }
            //onPress={() => setEngine('jet')}
            color = '#256173'
            uncheckedColor = '#256173'
            //labelStyle={{marginRight: 20}}
          />
          </RadioButton.Group>
          <Text style={styles.fieldTextRadio}>Jet</Text>
        </View>
        <View style={{flexDirection:'row', paddingLeft:40,}}>
        <RadioButton.Group value={edit?editEngine:engine} 
          onValueChange={edit?editEngine=>editSetEngine(editEngine):engine=>setEngine(engine)}>
          <RadioButton
            value="turboProp"
            //status={ engine === 'turboProp' ? 'checked' : 'unchecked' }
            //onPress={() => setEngine('turboProp')}
            color = '#256173'
            uncheckedColor = '#256173'
            //labelStyle={{marginRight: 20}}
          />
          </RadioButton.Group>
          <Text style={styles.fieldTextRadio}>Turbo Prop</Text>
        </View>
        <View style={{flexDirection:'row', paddingLeft:25,}}>
        <RadioButton.Group value={edit?editEngine:engine} 
          onValueChange={edit?editEngine=>editSetEngine(editEngine):engine=>setEngine(engine)}>
        <RadioButton
            value="turboShaft"
            //status={ engine === 'turboShaft' ? 'checked' : 'unchecked' }
            //onPress={() => setEngine('turboShaft')}
            color = '#256173'
            uncheckedColor = '#256173'
            //labelStyle={{marginRight: 20}}
          />
          </RadioButton.Group>
          <Text style={styles.fieldTextRadio}>Turbo-Shaft</Text>
        </View>
      </View>
      <View style={styles.fieldWithoutBottom}>
          <View style={styles.underline}>
        <View style={{flexDirection:'row',}}> 
        <RadioButton.Group value={edit?editEngine:engine} 
          onValueChange={edit?editEngine=>editSetEngine(editEngine):engine=>setEngine(engine)}>
         <RadioButton
            value="Piston"
            //status={ engine === 'piston' ? 'checked' : 'unchecked' }
            //onPress={() => setEngine('piston')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
          </RadioButton.Group>
          <Text style={styles.fieldTextRadio}>Piston</Text>
        </View>
        <View style={{flexDirection:'row', paddingLeft:23}}>
        <RadioButton.Group value={edit?editEngine:engine} 
          onValueChange={edit?editEngine=>editSetEngine(editEngine):engine=>setEngine(engine)}>
        <RadioButton
            value="notPowered"
            //status={ engine === 'notPowered' ? 'checked' : 'unchecked' }
            //onPress={() => setEngine('notPowered')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
          </RadioButton.Group>
          <Text style={styles.fieldTextRadio}>Not Powered</Text>
        </View>
      </View>
      </View>

      <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Engine Name</Text>
           <TextInput 
            placeholder='Engine Name'
            placeholderTextColor='#393F45'
            value={edit?editEngineName:engineName}
            onChangeText={edit?editEngineName=> editSetEngineName(editEngineName):engineName=>setEngineName(engineName)}
            style={{marginTop: -5}} />
        </View>
        </View>

           <View style={styles.halfViews}>
              <View style={{justifyContent:'center', width:'30%'}}>
                  <Text style={styles.fieldText}>Class</Text>
              </View>
              <View style={{width:'70%'}}>
              <View style={styles.fieldWithoutBottom}>
        <View style={{flexDirection:'row'}}> 
        <RadioButton.Group value={edit?editClass:Class} 
          onValueChange={edit?editClass=>editSetClass(editClass): Class=>setClass(Class)}>
         <RadioButton
            value="meLand"
            //status={ Class === 'meLand' ? 'checked' : 'unchecked' }
            //onPress={() => setClass('meLand')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
          </RadioButton.Group>
          <Text style={styles.fieldTextRadio}>ME Land</Text>
        </View>
        <View style={{flexDirection:'row', paddingLeft: 20,}}>
        <RadioButton.Group value={edit?editClass:Class} 
          onValueChange={edit?editClass=>editSetClass(editClass): Class=>setClass(Class)}>
        <RadioButton
            value="meSea"
            //status={ Class === 'meSea' ? 'checked' : 'unchecked' }
            //onPress={() => setClass('meSea')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
          </RadioButton.Group>
          <Text style={styles.fieldTextRadio}>ME Sea</Text>
        </View>
      </View>
      <View style={styles.fieldWithoutBottom}>
        <View style={{flexDirection:'row',}}>
        <RadioButton.Group value={edit?editClass:Class} 
          onValueChange={edit?editClass=>editSetClass(editClass): Class=>setClass(Class)}> 
         <RadioButton
            value="seLand"
            //status={ Class === 'seLand' ? 'checked' : 'unchecked' }
            //onPress={() => setClass('seLand')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
        </RadioButton.Group>
          <Text style={styles.fieldTextRadio}>SE Land</Text>
        </View>
      <View style={{flexDirection:'row', paddingLeft:25}}>
      <RadioButton.Group value={edit?editClass:Class} 
          onValueChange={edit?editClass=>editSetClass(editClass): Class=>setClass(Class)}>
        <RadioButton
            value="seSea"
            //status={ Class === 'seSea' ? 'checked' : 'unchecked' }
            //onPress={() => setClass('seSea')}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
          />
      </RadioButton.Group>
          <Text style={styles.fieldTextRadio}>SE Sea</Text>
        </View>
      </View>
      </View>
      </View>

    <View style={styles.headline}>
        <Text style={styles.HeadlineText}>Time</Text>
    </View>

    <View style={{...styles.fields, ...styles.extra}}>
       <Text style={{...styles.fieldText, ...{fontWeight:'700'}}}>Day</Text>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>PIC/P1</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit? editDay_pic : day_pic}
                    onChangeText={edit?(inputText) => editSetday_pic(inputText):(day_pic) => setday_pic(day_pic)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>
    { Class=== 'seLand' || Class=== 'seSea' || editClass==='seLand' || editClass==='seSea' ? <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Dual</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit? editDay_dual : day_dual}
                    onChangeText={edit?(inputText) => editSetDay_dual(inputText):day_dual => setDay_dual(day_dual)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View> : <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>SIC/P2</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit? editDay_sic : day_sic}
                    onChangeText={edit?(inputText) => editSetday_sic(inputText):day_sic => setday_sic(day_sic)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View> }
    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>P1(U/S)</Text>
           { Class=== 'seLand' || Class=== 'seSea' || editClass==='seLand' || editClass==='seSea' ? <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>N/A</Text>:
           <MaskedTextInput
           mask= '99:99'
           value={edit? editDayp1_us : dayp1_us}
           onChangeText={edit?(inputText) => editSetDayp1_us(inputText):dayp1_us => setDayp1_us(dayp1_us)}
           keyboardType="numeric"
           placeholder="hh:mm"
       />}
        </View>
    </View>
    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>P1(U/T)</Text>
           { Class=== 'seLand' || Class=== 'seSea' || editClass==='seLand' || editClass==='seSea' ? <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>N/A</Text>:
           <MaskedTextInput
           mask= '99:99'
           value={edit? editDayp1_ut : dayp1_ut}
           onChangeText={edit?(inputText) => editSetDayp1_ut(inputText):dayp1_ut => setDayp1_ut(dayp1_ut)}
           keyboardType="numeric"
           placeholder="hh:mm"
       />}
        </View>
    </View>
    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Total</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit? editDay_total : Class === 'seLand' || Class=== 'seSea'  ? seTime_day : meTime_day}
                    onChangeText={day_total => setDay_total(day_total)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>

    <View style={{...styles.fields, ...styles.extra}}>
       <Text style={{...styles.fieldText, ...{fontWeight:'700'}}}>Night</Text>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>PIC/P1</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit? editNight_pic : night_pic}
                    onChangeText={edit?(inputText) => editSetNight_pic(inputText):night_pic => setnight_pic(night_pic)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>
    { Class=== 'seLand' || Class=== 'seSea' || editClass==='seLand' || editClass==='seSea' ? <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Dual</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit? editNight_dual : night_dual}
                    onChangeText={edit?(inputText) => editSetNight_dual(inputText):night_dual => setNight_dual(night_dual)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View> : <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>SIC/P2</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit? editNight_sic :night_sic}
                    onChangeText={edit?(inputText) => editSetNight_sic(inputText):night_sic => setnight_sic(night_sic)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View> }
    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>P1(U/S)</Text>
           { Class=== 'seLand' || Class=== 'seSea' || editClass==='seLand' || editClass==='seSea' ? <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>N/A</Text>:
           <MaskedTextInput
           mask= '99:99'
           value={edit? editNightp1_us : nightp1_us}
           onChangeText={edit?(inputText) => editSetNightp1_us(inputText):nightp1_us => setNightp1_us(nightp1_us)}
           keyboardType="numeric"
           placeholder="hh:mm"
       />}
        </View>
    </View>
    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>P1(U/T)</Text>
           { Class=== 'seLand' || Class=== 'seSea' || editClass==='seLand' || editClass==='seSea'? <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>N/A</Text>:
           <MaskedTextInput
           mask= '99:99'
           value={edit? editNightp1_ut :nightp1_ut}
           onChangeText={edit?(inputText) => editSetNightp1_ut(inputText):nightp1_ut => setNightp1_ut(nightp1_ut)}
           keyboardType="numeric"
           placeholder="hh:mm"
       />}
        </View>
    </View>
    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Total</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit? editNight_total:Class === 'seLand' || Class=== 'seSea' ? seTime_night : meTime_night}
                    onChangeText={night_total => setNight_total(night_total)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Total Flying Time *</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit? editTotal_flying_time :Class === 'seLand' || Class=== 'seSea' ? TotalTime_se : TotalTime_me}
                    //value={total_flying_time}
                    onChangeText={total_flying_time => setTotal_flyingTime(total_flying_time)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>STL Hours</Text>
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Day</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={stl_day}
                    onChangeText={(InputText) => setStl_Day(InputText)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Night</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={stl_night}
                    onChangeText={InputText => setStl_Night(InputText)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Total</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={totalStl}
                    onChangeText={InputText => setStl_Total(InputText)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Instrument Time</Text>
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Actual</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit?editActual:actual}
                    onChangeText={edit?(inputText) => editSetActual(inputText):actual => setActual(actual)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Simulated</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit? editSimulated : simulated}
                    onChangeText={edit?(inputText) => editSetSimulated(inputText):simulated => setSimulated(simulated)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Simulator</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit?editSimulator:simulator}
                    onChangeText={edit?(inputText) => editSetSimulator(inputText):simulator => setSimulator(simulator)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Instructional Flying</Text>
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Day</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit?editInstructional_day:instructional_day}
                    onChangeText={edit?(inputText) => editSetInstructional_day(inputText):instructional_day => setInstructional_day(instructional_day)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Night</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit?editInstructional_night:instructional_night}
                    onChangeText={edit?(inputText) => editSetInstructional_night(inputText):instructional_night => setInstructional_night(instructional_night)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Total</Text>
           <MaskedTextInput
                    mask= '99:99'
                    value={edit? editInstructional_total:total_instructional}
                    onChangeText={instructional_total => setInstructional_total(instructional_total)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
        </View>
    </View>

    <View style={styles.headline}>
        <Text style={styles.HeadlineText}>T/O & Landing</Text>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Day T/O</Text>
           <TextInput
           placeholder='Please Enter Number'
           placeholderTextColor='#393F45'
           value={edit?editDay_to:day_to}
           onChangeText={edit?(inputText) => editSetDay_to(inputText):day_to => setDay_to(day_to)}
           style={{marginTop: -5}} />
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Night T/O</Text>
           <TextInput
           placeholder='Please Enter Number'
           placeholderTextColor='#393F45'
           value={edit?editNight_to:night_to}
           onChangeText={edit?(inputText) => editSetNight_to(inputText):night_to => setNight_to(night_to)}
           style={{marginTop: -5}} />
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Day Landing</Text>
           <TextInput
           placeholder='Please Enter Number'
           placeholderTextColor='#393F45'
           value={edit?editDayLanding:dayLanding}
           onChangeText={edit?(inputText) => editSetDayLanding(inputText):dayLanding => setDayLanding(dayLanding)}
           style={{marginTop: -5}} />
        </View>
    </View>

    <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Night Landing</Text>
           <TextInput
           placeholder='Please Enter Number'
           placeholderTextColor='#393F45'
           value={edit?editNightLanding:nightLanding}
           onChangeText={edit?(inputText) => editSetNightLanding(inputText):nightLanding => setNightLanding(nightLanding)}
           style={{marginTop: -5}} />
        </View>
    </View>

    <View style={styles.headline}>
        <Text style={styles.HeadlineText}>Remarks</Text>
    </View>

    <View style={{padding:20,}}>
    <View style={[styles.remarksBox, {borderColor: theme.color}]}>
    <TextInput
           placeholder=' Your Remarks'
           placeholderTextColor='#393F45'
           value={edit? editRemark :remark}
           onChangeText={edit?(inputText) => editSetRemark(inputText):remark => setRemark(remark)}
           style={Platform.OS=== 'android' ? {marginTop: -20}: {marginTop:-10}} />
    </View>
    </View>

    {edit === true ? console.log('hello') : console.log('world')}
    
    <View style={styles.bottomButtonSection}>
    {edit ?<TouchableOpacity onPress={editable}>
      <View style={styles.button}>
      <Text style={styles.buttonText}>cancel</Text>
      </View>
    </TouchableOpacity>:<TouchableOpacity onPress={editable}>
      <View style={styles.button}>
      <Text style={styles.buttonText}>Edit</Text>
      </View>
    </TouchableOpacity>}

    {edit?<TouchableOpacity onPress={Edit}>
      <View style={{paddingHorizontal:85,}}>
      <View style={styles.button}>
      <Text style={styles.buttonText}> Save </Text>
      </View>
      </View>
    </TouchableOpacity>:<TouchableOpacity onPress={upload_logbook}>
      <View style={{paddingHorizontal:45,}}>
      <View style={styles.button}>
      <Text style={styles.buttonText}> Save & Add another {'\n'} Aircraft</Text>
      </View>
      </View>
    </TouchableOpacity>}

    {edit?<TouchableOpacity onPress={Delete}>
      <View style={styles.button}>
      <Text style={styles.buttonText}>Delete</Text>
      </View>
    </TouchableOpacity>:<TouchableOpacity onPress={()=>{}}>
      <View style={styles.button}>
      <Text style={styles.buttonText}>Upload{'\n'}Logbook</Text>
      </View>
    </TouchableOpacity>}
    </View>

    </View>
    </ScrollView>
    </SafeAreaView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
    },
    headline: {
        padding: 20,
        backgroundColor: Colors.primary,
        width: '100%',
        justifyContent:'center',
    },
    HeadlineText:{
        color:'#fff',
        fontSize: 14,
        fontFamily: 'WorkSans-Regular',
    },
    fieldWithoutBottom: {
        paddingHorizontal:15, 
        //paddingVertical:10, 
        width:'100%',
        flexDirection:'row'
    },
    otherEnd: {
        justifyContent: 'space-between'
    },
    fieldText1: {
        fontSize: 14,
        marginTop: 5,
        fontWeight: '600',
        fontFamily: 'WorkSans-Regular',
        lineHeight: 25,
        color: Colors.accent
    },
    fieldText: {
        fontSize: 14,
        //marginTop: 5,
        fontWeight: '600',
        fontFamily: 'WorkSans-Regular',
        lineHeight: 25,
        color: Colors.primary,
        },
    fieldTextRadio: {
            fontSize: 14,
            //marginTop: 5,
            fontWeight: '600',
            fontFamily: 'WorkSans-Regular',
            lineHeight: 30,
            color: Colors.primary,
            },
        underline:{
                borderBottomWidth: 0.6,
                borderBottomColor: Colors.accent,
                //paddingVertical:15,
                width: '100%',
                flexDirection: 'row'
            },
        fields:{
                borderBottomWidth: 0.6,
                borderBottomColor: Colors.accent,
                //paddingHorizontal:15,
                //paddingVertical:15,
                width: '100%',
                justifyContent:'space-between',
                flexDirection:'row',
            },
        halfViews: {
               paddingHorizontal: 15,
               flexDirection: 'row',
            },
        extra: {
            padding:15,
        },
        extra1:{
          paddingHorizontal: 25,
          paddingVertical: 15
        },
        remarksBox: {
          borderWidth:1, 
          borderRadius:10,
          borderColor: '#000', 
          width: Dimensions.get('window').width * 0.9,
          padding: Platform.OS=== 'android' ? 10: 20,
        },
        bottomButtonSection:{
          padding: 5,
          backgroundColor: '#c0c0c0',
          width: '100%',
          flexDirection: 'row',
        },
        button: {
          backgroundColor: Colors.primary,
          padding: 10,
          marginTop: 5,
          //width: Dimensions.get('window').width*0.2,
          width:'100%',
          borderRadius:10,
          alignItems:'center'
        },
        buttonText:{
          fontWeight: 'bold',
          color: '#fff',
          textAlign: 'center'
        },
        header:{
          padding:15, 
          fontFamily:'WorkSans-Regular', 
          fontSize: 20, 
          color: Colors.primary,
        },
        headerIos: {
          padding:15, 
          fontFamily:'WorkSans-Regular', 
          fontSize: 20, 
          color: Colors.primary, 
          paddingTop: 42,
        },
});

//make this component available to the app
export default BuildBook;
