//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput, Dimensions, Platform } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { RadioButton, List, Switch } from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
//import CheckBox from '@react-native-community/checkbox';
import { Checkbox } from 'react-native-paper';
import { ThemeContext } from '../theme-context';
import { DisplayContext } from '../display-context';
import {TimePicker} from 'react-native-simple-time-picker';
import ModalDropdown from 'react-native-modal-dropdown';
//import DatePicker from 'react-native-date-picker'
import AsyncStorage from '@react-native-community/async-storage';
import { ParamsContext } from '../params-context';
import { useDispatch, useSelector } from 'react-redux';
import { buildLogBook } from '../store/actions/ActionBuildLogbbok';

import {BaseUrl} from '../components/url.json';


import Colors from '../components/colors';

// create a component
const Display = ({navigation}) => {
    // const tokenChanger = useDispatch();
    // const changetheYakenValue = () => {
    //     tokenChanger(buildLogBook({data: 'I am new content'}));
    // }
    // const userTokenFetch = useSelector(state => state.buildLogBook.userToken)
    //console.warn(userTokenFetch);

    const[aircraftId, setAircraftId] = React.useState('')

    const [params] = React.useContext(ParamsContext);

    React.useEffect(() => {
      if (params.childParam1) {
        console.log('The value of child param is: ', params.childParam1);
        setAircraftId(params.displayAirId)
      }
    }, [params]);

    //const [date, setDate] = React.useState('DDMM');

    //const [roleChecked, setRoleChecked] = React.useState('');

    const [instructor, setInstructor] = React.useState(false);
    const [country, setCountry] = React.useState(false);
    const [instrument, setInstrument] = React.useState(false);
    const [approach, setApproach] = React.useState(false);
    const [isSwitchOn, setIsSwitchOn] = React.useState(false);

    //const [selectedHours, setSelectedHours] = React.useState(0);
    //const [selectedMinutes, setSelectedMinutes] = React.useState('');
    //const [minute,setMinute] = React.useState(0);

//     const [hours, setHours] = React.useState(0);
//     const [minutes, setMinutes] = React.useState(0);
//     const handleChange = (hours, minutes) => {
//     setHours(hours);
//     setMinutes(minutes);
//   };


    // const onToggleSwitch = () => {
    //     setIsSwitchOn(isSwitchOn => !isSwitchOn)
    //   }

    const handleCheckBox = () => {
        setInstructor(!instructor)
    }

    const [df, setDf] = React.useState('')
    
    const { dark, theme, toggle } = React.useContext(ThemeContext);

    const { datee, Dateform, DateFormat, role, roleChecked } = React.useContext(DisplayContext);
    
    // React.useEffect(() => {Update_Display()}, [df,roleChecked,instrument,country,instrument,approach]);

    // const Update_Display = async() => {
    //     let user = await AsyncStorage.getItem('userdetails');
    //     user = JSON.parse(user);
      
    //     await fetch(BaseUrl+'post_settings',{
    //       method : 'POST',
    //       headers:{
    //           'Accept': 'application/json',
    //           'Content-Type': 'application/json'
    //       },
    //       body: JSON.stringify({
    //         "user_id": user.id,
    //         "date_format": df,   
    //         "aircraft_type": params.displayAirType,
    //         "aircraft_id" : aircraftId,
    //         "role": roleChecked,
    //         "academy_instructor" : instructor,
    //         "cross_country" : country,
    //         "approach1"  : approach,
    //         "actual_instrument" : instrument,
    //    })
    //   }).then(res => res.json())
    //   .then(resData => {
    //     //setBuildLogBook(resData.message);
    //     console.log('data1---->', resData.message);
    //     //setData(resData.message);
    //     //console.log('data--------->', data);
    //   });
    // };

    // React.useEffect(() => {get_Display()}, []);
    // const get_Display = async() => {
    //     let user = await AsyncStorage.getItem('userdetails');
    //     user = JSON.parse(user);
      
    //     await fetch(BaseUrl+'get_settings',{
    //       method : 'POST',
    //       headers:{
    //           'Accept': 'application/json',
    //           'Content-Type': 'application/json'
    //       },
    //       body: JSON.stringify({
    //         "user_id": user.id,
    //         //"date_format": date,             
    //    })
    //   }).then(res => res.json())
    //   .then(resData => {
    //     //setBuildLogBook(resData.message);
    //     console.log('data---->', resData.data);
    //     //df.push(resData.data);
    //     console.log('dfjgdhgf--->', df)
    //     for (var j = 0; j < resData.data.length; j++){
    //           console.log(resData.data[j].date_format);
    //           setDf(resData.data[j].date_format);
    //           console.log('df-->', df);
    //           setRoleChecked(resData.data[j].role)
    //           console.log('role---->',roleChecked);
    //           setInstructor(resData.data[j].academy_instructor)
    //           console.log('instructor-->', instructor)
    //           setCountry(resData.data[j].cross_country)
    //           console.log('Cross-country-->', country)
    //           setInstrument(resData.data[j].actual_instrument)
    //           console.log('instrument-->',instrument)
    //           setApproach(resData.data[j].approach1)
    //           console.log('Approach-->',approach)
    //          }
    //     //setData(resData.message);
    //     //console.log('data--------->', data);
    //   });
    // };
    
    return (
        <ScrollView>
        <View style={[styles.container,{ backgroundColor: theme.backgroundColor }]}>
        
        <View style={{flexDirection:'row'}}>
        <TouchableOpacity onPress={()=>navigation.navigate('Settings')}>
          <MaterialCommunityIcons  
           name="arrow-left" color={dark ? theme.icon : '#000'} size={30} style={Platform.OS === 'android' ? {padding: 15,}: 
           {padding: 15, paddingTop: 40}}
           />
        </TouchableOpacity>
        <Text style={Platform.OS === 'android' ? styles.header: styles.headerIos}>Display</Text>
        </View>
        {/* <TouchableOpacity onPress={() => changetheYakenValue()}>
            <Text>Update Token</Text>
        </TouchableOpacity>
        <Text>{userTokenFetch}</Text> */}
        <View style={styles.headline}>
          <Text style={styles.HeadlineText}>Date Format</Text>
        </View>

        <RadioButton.Group value={datee}
        onValueChange={DateFormat}>
        <View style={styles.fieldWithoutBottom}>
        <RadioButton
            value="DDMM"
            //status={ df === 'DDMM' ? 'checked' : 'unchecked' }
            //onPress={()=>{Update_Display();setDf('DDMM')}}
            //onPress={DateFormat}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />
        <Text style={styles.fieldText}>DD MM YYYY</Text>
        <View style={{paddingHorizontal:40, flexDirection:'row'}}>
        <RadioButton
            value="MMDD"
            //status={ df === 'MMDD' ? 'checked' : 'unchecked' }
            //onPress={()=>{Update_Display();setDf('MMDD')}}
            //onPress={DateFormat}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />
        
        <Text style={styles.fieldText}>MM DD YYYY</Text>
        </View>
        </View>
        </RadioButton.Group>

        {/* <View style={styles.fieldWithoutBottom}>
        <RadioButton.Group value={df}
        onValueChange={()=>{Update_Display();setDf('DDMM')}}>
        <RadioButton
            value="DDMM"
            //status={ df === 'DDMM' ? 'checked' : 'unchecked' }
            //onPress={()=>{Update_Display();setDf('DDMM')}}
            //onPress={DateFormat}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />
        </RadioButton.Group>
        <Text style={styles.fieldText}>DD MM YYYY</Text>
        <View style={{paddingHorizontal:40, flexDirection:'row'}}>
        <RadioButton.Group value={df}
        onValueChange = {()=>{setDf('MMDD');Update_Display()}}>
        <RadioButton
            value="MMDD"
            //status={ df === 'MMDD' ? 'checked' : 'unchecked' }
            //onPress={()=>{Update_Display();setDf('MMDD')}}
            //onPress={DateFormat}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />
        </RadioButton.Group>
        <Text style={styles.fieldText}>MM DD YYYY</Text>
        </View>
        </View> */}
    

        <View style={{...styles.headline, ...styles.headline2}}>
          <Text style={styles.HeadlineText}>My Aircraft</Text>
        </View>
        
        <TouchableOpacity onPress={()=>navigation.navigate('Aircraft',{fromScreenDisplay:'Display'})}>
        <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={styles.fieldText}>Type</Text>
           <Text style={styles.fieldText1}>{params.displayAirType?params.displayAirType:<Text>AirCraft Type</Text> }</Text>
        </View>
        </View>
        </TouchableOpacity>

        <View style={{...styles.fieldWithoutBottom, ...styles.otherEnd}}>
           <Text style={styles.fieldText}>Aircraft ID</Text>
           <TextInput 
            placeholder='Aircraft ID'
            value={aircraftId}
            onChangeText = {(inputText)=>setAircraftId(inputText)}
            placeholderTextColor='#393F45'
            style={{marginTop: -5}} />
        </View>

        <View style={styles.headline}>
          <Text style={styles.HeadlineText}>Role</Text>
        </View>

        <RadioButton.Group value={role}
        onValueChange={roleChecked}>
        <View style={styles.fieldWithoutBottom}>
        <RadioButton
            value="AirlineCaptain"
            //status={ roleChecked === 'AirlineCaptain' ? 'checked' : 'unchecked' }
            //onPress={() => {setRoleChecked('AirlineCaptain'); Update_Display()}}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />
        <Text style={styles.fieldText}>Airline Captain</Text>
        <View style={{paddingHorizontal:22, flexDirection:'row'}}> 
        <RadioButton
            value="AirlineFirstOfficer"
            //status={ roleChecked === 'AirlineFirstOfficer' ? 'checked' : 'unchecked' }
            //onPress={() => {setRoleChecked('AirlineFirstOfficer'); Update_Display()}}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />
        <Text style={styles.fieldText}>Airline First Officer</Text>
        </View>
        </View>

        <View style={styles.fieldWithoutBottom}>
        <View style={styles.underline}>

        <RadioButton
            value="AirlineInstructor"
            //status={ roleChecked === 'AirlineInstructor' ? 'checked' : 'unchecked' }
            //onPress={() => {setRoleChecked('AirlineInstructor'); Update_Display()}}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />

        <Text style={styles.fieldText}>Airline Instructor</Text>
        <View style={{paddingHorizontal:10, flexDirection:'row'}}>
        
        <RadioButton
            value="FlightCadet"
            //status={ roleChecked === 'FlightCadet' ? 'checked' : 'unchecked' }
            //onPress={() => {setRoleChecked('FlightCadet'); Update_Display()}}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />
    
        <Text style={styles.fieldText}>Flight Cadet</Text>
        </View>
        <View style={{paddingHorizontal:8, flexDirection:'row'}}>
    
        <RadioButton
            value="Cfi"
            //status={ roleChecked === 'FlightCadet' ? 'checked' : 'unchecked' }
            //onPress={() => {setRoleChecked('FlightCadet'); Update_Display()}}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />
        
        <Text style={styles.fieldText}>CFI</Text>
        </View>

        </View>
        </View>
        </RadioButton.Group>

        {/* <View style={styles.fieldWithoutBottom}>
        <RadioButton.Group value={roleChecked}
        onValueChange={()=>{Update_Display();setRoleChecked('AirlineCaptain')}}>
        <RadioButton
            value="AirlineCaptain"
            //status={ roleChecked === 'AirlineCaptain' ? 'checked' : 'unchecked' }
            //onPress={() => {setRoleChecked('AirlineCaptain'); Update_Display()}}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />
        </RadioButton.Group>
        <Text style={styles.fieldText}>Airline Captain</Text>
        <View style={{paddingHorizontal:22, flexDirection:'row'}}> 
        <RadioButton.Group value={roleChecked}
        onValueChange={()=>{Update_Display();setRoleChecked('AirlineFirstOfficer')}}>
        <RadioButton
            value="AirlineFirstOfficer"
            //status={ roleChecked === 'AirlineFirstOfficer' ? 'checked' : 'unchecked' }
            //onPress={() => {setRoleChecked('AirlineFirstOfficer'); Update_Display()}}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />
        </RadioButton.Group>
        <Text style={styles.fieldText}>Airline First Officer</Text>
        </View>
        </View>

        <View style={styles.fieldWithoutBottom}>
        <View style={styles.underline}>

        <RadioButton.Group value={roleChecked}
        onValueChange={()=>{Update_Display();setRoleChecked('AirlineInstructor')}}>
        <RadioButton
            value="AirlineInstructor"
            //status={ roleChecked === 'AirlineInstructor' ? 'checked' : 'unchecked' }
            //onPress={() => {setRoleChecked('AirlineInstructor'); Update_Display()}}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />
        </RadioButton.Group>
        <Text style={styles.fieldText}>Airline Instructor</Text>
        <View style={{paddingHorizontal:10, flexDirection:'row'}}>
        <RadioButton.Group value={roleChecked}
        onValueChange={()=>{Update_Display();setRoleChecked('FlightCadet')}}>
        <RadioButton
            value="FlightCadet"
            //status={ roleChecked === 'FlightCadet' ? 'checked' : 'unchecked' }
            //onPress={() => {setRoleChecked('FlightCadet'); Update_Display()}}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />
        </RadioButton.Group>
        <Text style={styles.fieldText}>Flight Cadet</Text>
        </View>
        <View style={{paddingHorizontal:8, flexDirection:'row'}}>
        <RadioButton.Group value={roleChecked}
        onValueChange={()=>{Update_Display();setRoleChecked('FlightCadet')}}>
        <RadioButton
            value="FlightCadet"
            //status={ roleChecked === 'FlightCadet' ? 'checked' : 'unchecked' }
            //onPress={() => {setRoleChecked('FlightCadet'); Update_Display()}}
            color = '#256173'
            uncheckedColor = '#256173'
            labelStyle={{marginRight: 20}}
        />
        </RadioButton.Group>
        <Text style={styles.fieldText}>CFI</Text>
        </View>

        </View>
        </View> */}
        
        <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={styles.fieldText}>Cross Country</Text>
           {/* <CheckBox
            value={country}
            onValueChange={setCountry}
            tintColor= '#256173'
            boxType = 'square'
            onTintColor = '#256173'
            onCheckColor = '#fff'
            onFillColor = '#256173'
            tintColors={{ true: '#256173', false: '#256173' }}
            onChange = {()=>Update_Display()}
            /> */}
            <Checkbox
            color = '#256173'
            status={country ? 'checked' : 'unchecked'}
            onPress={() => {
                setCountry(!country);
            }}
            />
        </View>
        </View>

        <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
            <View style={{flexDirection:'column'}}>
                <Text style={styles.fieldText}>Actual Instrument</Text>
                <View style={{flexDirection:'row'}}>
                    <Text style={styles.fieldText}>Block time -</Text>
                    <ModalDropdown options={['15','30','45','60']}
                    defaultValue= "15"
                    isFullWidth = {true}
                    textStyle= {{color: '#000', fontSize:15,}}
                    dropdownStyle={{boderWidth:1, borderColor:'#256173', width:100}}
                    style={{marginTop: 8,}}
                    />
                </View>
           </View>
           {/* <CheckBox
            value={instrument}
            onValueChange={setInstrument}
            tintColor= '#256173'
            boxType = 'square'
            onTintColor = '#256173'
            onCheckColor = '#fff'
            onFillColor = '#256173'
            tintColors={{ true: '#256173', false: '#256173' }}
            onChange = {()=>Update_Display()}
            /> */}
            <Checkbox
            status={instrument ? 'checked' : 'unchecked'}
            color = '#256173'
            onPress={() => {
                setInstrument(!instrument);
            }}
            />
        </View>
        </View>

        {/* {instrument == true? <View style={styles.fieldWithoutBottom}>
        <View style={styles.fields}>
           <Text style={{...styles.fieldText, ...{lineHeight:35,}}}>Time Calculation</Text>
           <View style={{justifyContent:'flex-end', flexDirection:'row'}}>
           <Text style={{marginTop:12}}>Block time-</Text>
           <ModalDropdown options={['15','30','45','60']}
                  defaultValue= "Select Minutes"
                  isFullWidth = {true}
                  textStyle= {{color: '#256173', fontSize:15,}}
                  dropdownStyle={{boderWidth:1, borderColor:'#256173', width:100}}
                  style={{marginTop: 12,}}
                  />
            </View>
        </View>
        </View>: null} */}

        <View style={{...styles.fieldWithoutBottom, ...styles.otherEnd}}>
        
           <Text style={styles.fieldText}>Approach 1</Text>
           {/* <CheckBox
            value={approach}
            onValueChange={setApproach}
            onCheckColor= '#fff'
            boxType = 'square'
            tintColor = '#256173'
            onTintColor = '#256173'
            onFillColor = '#256173'
            tintColors={{ true: '#256173', false: '#256173' }}
            onChange = {()=>Update_Display()}
            /> */}
            <Checkbox
            color = '#256173'
            status={approach ? 'checked' : 'unchecked'}
            onPress={() => {
                setApproach(!approach);
            }}
            />
        </View>

        <View style={{...styles.headline, ...styles.headline2}}>
          <Text style={styles.HeadlineText}>Night Mode</Text>
        </View>

        <View style={{...styles.fieldWithoutBottom, ...styles.otherEnd}}>
            {dark ? <Text style={styles.fieldText}>Change to Day Mode</Text> : <Text style={styles.fieldText}>Change to Dark Mode</Text>}
            <Switch trackColor={{ false: "#5f9ea0", true: "#ccc" }}
                    thumbColor={dark ? "#fff" : "#256173"}
                    onChange={toggle} value = {dark} />
        </View>

        </View>
        </ScrollView>

    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
        //height: Dimensions.get('window').height*1.5,
        
    },
    headline: {
        padding: 20,
        backgroundColor: Colors.primary,
        width: '100%',
        justifyContent:'center',
    },
    headline2:{
        marginTop:10,
    },
    HeadlineText:{
        color:'#fff',
        fontSize: 14,
        fontFamily: 'WorkSans-Regular',
    },
    fields:{
        borderBottomWidth: 0.2,
        borderBottomColor: Colors.accent,
        //paddingHorizontal:15,
        paddingVertical:10,
        width: '100%',
        justifyContent:'space-between',
        flexDirection:'row',
    },
    fieldText: {
        fontSize: 14,
        marginTop: 5,
        fontWeight: '500',
        fontFamily: 'WorkSans-Regular',
        lineHeight: 25,
        color: Colors.primary,
        },
    fieldText1: {
        fontSize: 14,
        marginTop: 5,
        fontWeight: '400',
        fontFamily: 'WorkSans-Regular',
        lineHeight: 25,
        color: Colors.accent
    },
    fieldWithoutBottom: {
        paddingHorizontal:15, 
        paddingVertical:10, 
        width:'100%',
        flexDirection:'row'
    },
    otherEnd: {
        justifyContent: 'space-between'
    },
    underline:{
        borderBottomWidth: 0.2,
        borderBottomColor: Colors.accent,
        //paddingVertical:15,
        width: '100%',
        flexDirection: 'row'
    },
    header:{
        padding:15, 
        fontFamily:'WorkSans-Regular', 
        fontSize: 20, 
        color: Colors.primary,
    },
    headerIos: {
    padding:15, 
    fontFamily:'WorkSans-Regular', 
    fontSize: 20, 
    color: Colors.primary, 
    paddingTop: 42,
    },
});

//make this component available to the app
export default Display;
