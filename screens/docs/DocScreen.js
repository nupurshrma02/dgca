//import liraries
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Alert } from 'react-native';
import DocScreenStyle from '../../styles/docStyles';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// create a component
const Docs = ({navigation}) => {
    return (
        <View style={DocScreenStyle.container}>
          <View style={DocScreenStyle.header}>
          <MaterialCommunityIcons name="arrow-left" color={'#fff'} size={20} style={{padding:6}} onPress={()=>navigation.goBack()} />
          <Text style={DocScreenStyle.aircrafts}>Docs</Text>
          </View>
          <View style={DocScreenStyle.mainTagLine}>
            <Text style={DocScreenStyle.tagLine}>Logbooks</Text>
          </View>
          <View style={DocScreenStyle.tabs}>
            <TouchableOpacity onPress={()=>navigation.navigate('DgcaLogBook')}>
            <Text style={DocScreenStyle.tabText}>DGCA LogBook</Text>
            </TouchableOpacity>
          </View>
          <View style={DocScreenStyle.tabs}>
            <TouchableOpacity>
            <Text style={DocScreenStyle.tabText}>Jeppessen Logbook (USA)</Text>
            </TouchableOpacity>
          </View>
          <View style={DocScreenStyle.tabs}>
            <TouchableOpacity>
            <Text style={DocScreenStyle.tabText}>Jeppessen Logbook (EU)</Text>
            </TouchableOpacity>
          </View>
          <View style={DocScreenStyle.mainTagLine}>
            <Text style={DocScreenStyle.tagLine}>Reports</Text>
          </View>
          <View style={DocScreenStyle.tabs}>
            <TouchableOpacity>
            <Text style={DocScreenStyle.tabText}>DGCA (CA-39)</Text>
            </TouchableOpacity>
          </View>
          <View style={DocScreenStyle.tabs}>
            <TouchableOpacity>
            <Text style={DocScreenStyle.tabText}>ATPL Hours</Text>
            </TouchableOpacity>
          </View>
          <View style={DocScreenStyle.tabs}>
            <TouchableOpacity onPress={()=>Alert.alert('coming Soon ')}>
            <Text style={DocScreenStyle.tabText}>Experience Certificate</Text>
            </TouchableOpacity>
          </View>
          <View style={DocScreenStyle.tabs}>
            <TouchableOpacity onPress={()=>Alert.alert('coming Soon ')}>
            <Text style={DocScreenStyle.tabText}>Flights by city</Text>
            </TouchableOpacity>
          </View>
          <View style={DocScreenStyle.lastTab}>
            <TouchableOpacity onPress={()=>Alert.alert('coming Soon ')}>
            <Text style={DocScreenStyle.tabText}>Flights by country</Text>
            </TouchableOpacity>
          </View>
        </View>
    );
};

// define your styles

//make this component available to the app
export default Docs;
