import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import PDFView from 'react-native-view-pdf';

const ShowPDF = ({route}) => {
  const resources = {
    file: route.params.filepath,
    base64: route.params.base64,
  };
  const resourceType = 'base64';
  return (
    <View style={{flex: 1}}>
      {/* Some Controls to change PDF resource */}
      <PDFView
        fadeInDuration={250.0}
        style={{flex: 1}}
        resource={resources[resourceType]}
        resourceType={resourceType}
        onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
        onError={error => console.log('Cannot render PDF', error)}
      />
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity
          style={{
            backgroundColor: 'red',
            padding: 20,
            width: '50%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: '#fff'}}>Export for print</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            backgroundColor: 'blue',
            padding: 20,
            width: '50%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: '#fff'}}>Share Via Email</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ShowPDF;
