//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

// create a component
const DgcaLogBook = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Button title='pdf Generator' onPress={()=>navigation.navigate('PdfGenerator')}/>
            <View style={{paddingTop: 10}}>
            {/* <Button title='Show Pdf' onPress={()=>navigation.navigate('ShowPDF')}/> */}
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default DgcaLogBook;
