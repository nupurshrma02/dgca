//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, Platform, Dimensions, Alert } from 'react-native';
import  Colors  from '../../components/colors';
import ImagePicker from 'react-native-image-picker';
import { ScrollView } from 'react-native-gesture-handler';
import { RadioButton,} from 'react-native-paper';
import { ThemeContext } from '../../theme-context';
import {Logbook} from '../../styles/styles';
import AsyncStorage from '@react-native-community/async-storage';

import {BaseUrl} from '../../components/url.json';

import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase(
  {
    name: 'autoflightlogdb.db',
    createFromLocation: 1,
    //location: 'www/autoflightlogdb.db',
  },
  () => {
    //alert('successfully executed');
  },
  error => {
    alert('db error');
  },
);

// create a component
const SetPeople = ({navigation}) => {
    const [image, setImage] = React.useState(null);
    const [imageData, setImageData] = React.useState('');
    const [imageFilename, setImageFilename] = React.useState('');

    // const [category, setCategory] = React.useState('');
    // const [engine, setEngine] = React.useState('');
    // const [Class, setClass] = React.useState('');

    const [name, setName] = React.useState('')
    const [airlineCode, setAirlineCode] = React.useState('')
    const [egcaId, setEgcaId ] = React.useState('')
    const [comments, setComments] = React.useState('')

    const selectingImage = () => {
        ImagePicker.showImagePicker({quality: 0.3}, responseGet => {
          console.log('Response = ', responseGet);
   
          if (responseGet.didCancel) {
            console.log('User cancelled image picker');
          } else if (responseGet.error) {
            console.log('ImagePicker Error: ', responseGet.error);
          } else {
            const source = {uri: responseGet.uri};
            console.log(source)
               setImage(source)

          }
        });
      };

    //   const Add_People = async() => {
    //     let user = await AsyncStorage.getItem('userdetails');
    //     user = JSON.parse(user);
      
    //     await fetch(BaseUrl+'add_people',{
    //       method : 'POST',
    //       headers:{
    //           'Accept': 'application/json',
    //           'Content-Type': 'application/json'
    //       },
    //       body: JSON.stringify({
    //         "user_id": user.id,
    //         "name" : name,
    //         "emp_code" : egcaId,
    //         "comments" : comments,
    //         "airline" : airlineCode,
    //    })
    //   }).then(res => res.json())
    //   .then(resData => {
    //     console.log('data---->', resData.data);
    //     console.log('data---->', resData.message);
    //     Alert.alert(resData.message);
    //     // if(resData.message === 'Record inserted successfully .'){
    //     //     navigation.goBack();
    //     // }
    //  });
    // };

    const { dark, theme, toggle } = React.useContext(ThemeContext);

    //sqlite 
  
    const insertQuery = async() => {
      if (!name) {
        alert('Please fill name');
        return;
      }
      if (!airlineCode) {
        alert('Please fill Airline Code');
        return;
      }
      if (!egcaId) {
        alert('Please fill Egca ID');
        return;
      }
      let user = await AsyncStorage.getItem('userdetails');
      user = JSON.parse(user);
      db.transaction(tx => {
        tx.executeSql(
          'INSERT INTO people (user_id, name, airline, emp_code, comments) VALUES ("'+user.id+'","'+name+'", "'+airlineCode+'", "'+egcaId+'", "'+comments+'")',
        );
      });
      alert('Inserted successfully');
    };

    //sqlite ends

    return (
      <ScrollView>
        <View style={[styles.container, {backgroundColor: theme.backgroundColor}]}>
            
            <TouchableOpacity onPress={()=> navigation.goBack()}>
                <Text style={Platform.OS=== 'android' ? styles.save: styles.saveIos}>Back</Text>
            </TouchableOpacity>

            {/* image */}
            <View style={[{padding: 20, backgroundColor: '#fff', width:'100%', alignItems:'center'}, 
            {backgroundColor: theme.backgroundColor}]}>
                    <TouchableOpacity onPress={()=>selectingImage()}>
                    { dark ? <Image source={
                          image === null
                          ? require('../../images/userWhite.png')
                          : image
                          } 
                          style={{height:90, width:90}}/>
                          :<Image source={
                          image === null
                          ? require('../../images/user.png')
                          : image
                          } 
                         style={{height:70, width:70}}/>}
                    </TouchableOpacity>     
            </View>

            <View style={Logbook.fieldWithoutBottom}>
                <View style={Logbook.fields}>
                <Text style={{...Logbook.fieldText, ...{lineHeight:35,}}}>Name</Text>
                <TextInput 
                    placeholder='Enter Name'
                    placeholderTextColor='#393F45'
                    value = {name}
                    onChangeText = {(name)=>setName(name)} 
                    style={{marginTop: -5}} />
                </View>
            </View>

            <View style={Logbook.fieldWithoutBottom}>
                <View style={Logbook.fields}>
                <Text style={{...Logbook.fieldText, ...{lineHeight:35,}}}>Airline Code</Text>
                <TextInput 
                    placeholder='Airline code'
                    placeholderTextColor='#393F45'
                    value={airlineCode}
                    onChangeText={(airlineCode)=>setAirlineCode(airlineCode)}
                    style={{marginTop: -5}} />
                </View>
            </View>
        
            <View style={Logbook.fieldWithoutBottom}>
                <View style={Logbook.fields}>
                <Text style={{...Logbook.fieldText, ...{lineHeight:35,}}}>EGCA ID</Text>
                <TextInput 
                    placeholder='EGCA ID'
                    value={egcaId}
                    onChangeText={(egcaId)=> setEgcaId(egcaId)}
                    placeholderTextColor='#393F45'
                    style={{marginTop: -5}} />
                </View>
            </View>

            <View style={Logbook.fieldWithoutBottom}>
                <View style={Logbook.fields}>
                <View style={Logbook.CommentsBox}>
                <TextInput
                    placeholder='Comments'
                    placeholderTextColor='#393F45'
                    value={comments}
                    onChangeText={(comments)=>setComments(comments)}
                    style={Platform.OS=== 'android' ? {marginTop: -20}: {marginTop:-10}} />
                </View>
                </View>
            </View>

            <TouchableOpacity onPress={insertQuery}>
                <View style= {{alignItems:'center'}}>
                <View style={styles.button}>
                <Text style={styles.buttonText}>Save</Text>
                </View>
                </View>
            </TouchableOpacity>

        </View>
        </ScrollView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        //alignItems: 'center',
        backgroundColor: '#fff',
    },
    save: {
        padding:10, 
        fontFamily:'WorkSans-Regular', 
        fontSize:20, 
        color: Colors.primary
      },
      saveIos: {
        paddingTop:40, 
        paddingLeft: 10, 
        fontFamily:'WorkSans-Regular', 
        fontSize:20, 
        color: Colors.primary
      },
      button: {
        backgroundColor: Colors.primary,
        padding: 15,
        marginTop: 20,
        width: Dimensions.get('window').width*0.5,
        borderRadius:10,
        alignItems:'center'
    },
    buttonText:{
      fontWeight: 'bold',
      color: '#fff',
    },
});

//make this component available to the app
export default SetPeople;
