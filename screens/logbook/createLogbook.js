//import liraries
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Button, Modal, Pressable, StyleSheet, Alert, SafeAreaView, Platform, TouchableWithoutFeedback } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
//import DatePicker from 'react-native-datepicker';
import { RadioButton } from 'react-native-paper';
import CheckBox from '@react-native-community/checkbox';
import { MaskedTextInput } from "react-native-mask-text";
import AsyncStorage from '@react-native-community/async-storage';
import { ParamsContext } from '../../params-context';
import { ConfigContext } from '../../config-Context';
import { DisplayContext } from '../../display-context';
import DateTimePicker from '@react-native-community/datetimepicker';


import { BaseUrl } from '../../components/url.json';
import SQLite from 'react-native-sqlite-storage';

const prePopulateddb = SQLite.openDatabase(
    {
        name: 'autoflightlogdb.db',
        createFromLocation: 1,
        //location: 'www/autoflightlogdb.db',
    },
    () => {
        //alert('successfully executed');
    },
    error => {
        alert('db error');
    },
);


import { Logbook, ModalView } from '../../styles/styles';
import { ScrollView } from 'react-native-gesture-handler';
import { List } from 'native-base';

// create a component
const CreateLogbook = ({ navigation }) => {

    //custom input fields start

    // this will be attached with each input onChangeText
    const [textValue, setTextValue] = React.useState('');
    // our number of inputs, we can add the length or decrease
    const [numInputs, setNumInputs] = React.useState(1);
    // all our input fields are tracked with this array
    const refInputs = React.useRef([textValue]);

    //custom input fields end

    const [chocksOff, setChocksOff] = React.useState('');
    const [takeOff, setTakeOff] = React.useState('');
    const [landing, setLanding] = React.useState('');
    const [chocksOn, setChocksOn] = React.useState('');

    //time section
    const [totalTime, setTotalTime] = React.useState('');
    const [day, setDay] = React.useState('');
    const [night, setNight] = React.useState('');
    const [filghtTimeM, setfilghtTimeM] = React.useState('')
    const [dayTime, setDayTime] = React.useState('')
    const [nightTime, setNightTime] = React.useState('')
    const [ai, setAi] = React.useState('');
    const [dual_day, setDual_day] = React.useState('');
    const [dual_night, setDual_night] = React.useState('');
    const [p1_us_day, setP1_us_day] = React.useState('');
    const [p1_us_night, setP1_us_night] = React.useState('');
    const [p1_ut_day, setP1_ut_day] = React.useState('');
    const [p1_ut_night, setP1_ut_night] = React.useState('');
    const [pic_day, setPic_day] = React.useState('');
    const [pic_night, setPic_night] = React.useState('');
    const [si, setSi] = React.useState('');
    const [sic_day, setSic_day] = React.useState('');
    const [sic_night, setSic_night] = React.useState('');
    const [xc_day, setXc_day] = React.useState('');
    const [xc_night, setXc_night] = React.useState('');
    const [xc_day_leg, setXc_dayLeg] = React.useState('');
    const [xc_night_leg, setXc_nightLeg] = React.useState('');

    //const [maskedValue, setMaskedValue] = React.useState("");
    //const [unMaskedValue, setUnmaskedValue] = React.useState("");


    const [date, setDate] = React.useState(new Date());
    const [mode, setMode] = React.useState('date');
    const [show, setShow] = React.useState(false);
    //const [date, setDate] = React.useState('15-05-2021');
    const [fr, setFr] = React.useState('ifr');
    const [stl, setStl] = React.useState(false);
    const [df, setDf] = React.useState([]);
    const [aircraft_name, setAircraft_name] = React.useState('')
    const [flight, setFlight] = React.useState('')
    const [aircraftId, setAircraftId] = React.useState('') //needsa to get change (dummy)
    const [route, setRoute] = React.useState('')
    const [remark, setRemark] = React.useState('')

    //landing
    const [dayLanding, setDayLanding] = React.useState('');
    const [day_to, setDay_to] = React.useState('');
    const [nightLanding, setNightLanding] = React.useState('');
    const [night_to, setNight_to] = React.useState('');

    const [modalVisible, setModalVisible] = React.useState(false);
    const [FlightmodalVisible, setFlightModalVisible] = React.useState(false);
    const [TimemodalVisible, setTimeModalVisible] = React.useState(false);
    const [LandingmodalVisible, setLandingModalVisible] = React.useState(false);

    //params constants
    const [from, setFrom] = React.useState('')
    const [to, setTo] = React.useState('')
    const [fromLatdest, setFromLatdest] = React.useState('')
    const [toLatdest, setToLatdest] = React.useState('')
    const [fromLongdest, setFromLongdest] = React.useState('')
    const [toLongdest, setToLongdest] = React.useState('')
    const [fromIcao, setFromIcao] = React.useState('')
    const [toIcao, setToIcao] = React.useState('')
    const [pic_p1, setPic_p1] = React.useState('')
    const [sic_p2, setSic_p2] = React.useState('')
    const [approach1, setApproach1] = React.useState('')

    //Hidden fields //params
    const [instructor, setInstructor] = React.useState('')
    const [reliefCrew1, setReliefCrew1] = React.useState('')
    const [reliefCrew2, setReliefCrew2] = React.useState('')
    const [reliefCrew3, setReliefCrew3] = React.useState('')
    const [reliefCrew4, setReliefCrew4] = React.useState('')
    const [student, setStudent] = React.useState('')
    const [approach2, setApproach2] = React.useState('')

    //Hidden Fields
    const [instructional, setInstructional] = React.useState('')
    const [autoLanding, setAutoLanding] = React.useState('')
    const [fullStop, setFullStop] = React.useState('')
    const [touchGo, setTouchGo] = React.useState('')
    const [waterLanding, setWaterLanding] = React.useState('')
    const [water_to, setWater_to] = React.useState('')

    //from listingLogbook
    const [listId, setListId] = React.useState('')
    const [listDate, setListDate] = React.useState('')
    const [listFlight, setListFlight] = React.useState('')
    const [listAircraftType, setListAircraftType] = React.useState('')
    const [listAircraftId, setListAircraftId] = React.useState('')
    const [listFrom, setListFrom] = React.useState('')
    const [listTo, setListTo] = React.useState('')
    const [listChocksOff, setListChocksOff] = React.useState('')
    const [listTakeOff, setListTakeOff] = React.useState('')
    const [listLanding, setListLanding] = React.useState('')
    const [listChocksOn, setListChocksOn] = React.useState('')
    const [listRoute, setListRoute] = React.useState('')
    const [listInstructor, setListInstructor] = React.useState('')
    const [listP1, setListP1] = React.useState('')
    const [listRc1, setListRc1] = React.useState('')
    const [listRc2, setListRc2] = React.useState('')
    const [listRc3, setListRc3] = React.useState('')
    const [listRc4, setListRc4] = React.useState('')
    const [listP2, setListP2] = React.useState('')
    const [listStudent, setListStudent] = React.useState('')
    const [listTotalTime, setListTotalTime] = React.useState('')
    const [listDay, setListDay] = React.useState('')
    const [listNight, setListNight] = React.useState('')
    const [listAi, setListAi] = React.useState('')
    const [listDualDay, setListDualDay] = React.useState('')
    const [listDualNight, setListDualNight] = React.useState('')
    const [listInstructional, setListInstructional] = React.useState('')
    const [listFr, setListFr] = React.useState('')
    const [listP1UsDay, setListP1UsDay] = React.useState('')
    const [listP1UsNight, setListP1UsNight] = React.useState('')
    const [listP1UtDay, setListP1UtDay] = React.useState('')
    const [listP1UtNight, setListP1UtNight] = React.useState('')
    const [listPicDay, setListPicDay] = React.useState('')
    const [listPicNight, setListPicNight] = React.useState('')
    const [listStl, setListStl] = React.useState('')
    const [listSi, setListSi] = React.useState('')
    const [listSicDay, setListSicDay] = React.useState('')
    const [listSicNight, setListSicNight] = React.useState('')
    const [listXcDay, setListXcDay] = React.useState('')
    const [listXcNight, setListXcNight] = React.useState('')
    const [listXcDayLegs, setListXcDayLegs] = React.useState('')
    const [listXcNightLegs, setListXcNightLegs] = React.useState('')
    const [listAutoLanding, setListAutoLanding] = React.useState('')
    const [listDayLanding, setListDayLanding] = React.useState('')
    const [listDayTO, setListDayTO] = React.useState('')
    const [listFullStop, setListFullStop] = React.useState('')
    const [listNightLanding, setListNightLanding] = React.useState('')
    const [listNightTO, setListNightTO] = React.useState('')
    const [listTouchGo, setListTouchGo] = React.useState('')
    const [listWaterLanding, setListWaterLanding] = React.useState('')
    const [listWaterTO, setListWaterTO] = React.useState('')
    const [listApproach1, setListApproach1] = React.useState('')
    const [listApproach2, setListApproach2] = React.useState('')
    const [listRemark, setListRemark] = React.useState('')

    //from roaster
    const [rosterDate, setRosterDate] = React.useState('')
    const [rosterFrom, setRosterFrom] = React.useState('')
    const [rosterChocksOff, setRosterChocksOff] = React.useState('')
    const [rosterTo, setRosterTo] = React.useState('')
    const [rosterChocksOn, setRosterChocksOn] = React.useState('')
    const [rosterAType, setRosterAType] = React.useState('')
    const [rosterAId, setRosterAId] = React.useState('')
    const [rosterTotalTime, setRosterTotalTime] = React.useState('')
    const [rosterNamePic, setRosterNamePic] = React.useState('')
    const [rosterDayLand, setRosterDayLand] = React.useState('')
    const [rosterNightLand, setRosterNightLand] = React.useState('')
    const [rosterDayTakeOff, setRosterDayTakeOff] = React.useState('')
    const [rosterNightTakeOff, setRosterNightTakeOff] = React.useState('')

    //for simu
    const [St, setSt] = React.useState('')
    const [location, setLocation] = React.useState('')
    const [Sim_exercise, setSim_exercise] = React.useState('')
    const [totalTo_time, setTotalTo_time] = React.useState('')
    const [pfHours, setPfHours] = React.useState('')
    const [pmHours, setPmHours] = React.useState('')
    const [sf, setSf] = React.useState('')


    //from configuration
    const [f, setF] = React.useState(true)


    //for configuration
    const [config, setConfig] = React.useState(false);

    const { datee, Dateform, DateFormat, role } = React.useContext(DisplayContext);

    const [params] = React.useContext(ParamsContext);
    const [Destparams] = React.useContext(ParamsContext);
    const [Peopleparams] = React.useContext(ParamsContext);
    const [Approachparams] = React.useContext(ParamsContext);
    const [Listparams] = React.useContext(ParamsContext);
    const [ConfigParams] = React.useContext(ParamsContext);

    React.useEffect(() => {
        if (ConfigParams.childParam) {
            console.log('air Type', ConfigParams.itemRoleWise);
            setF(ConfigParams.itemRoleWise)
        }

    }, [ConfigParams]);

    React.useEffect(() => {
        if (params.childParam2) {
            console.log('air Type', params.logBookAirType);
            setAircraft_name(params.logBookAirType)
            //setAircraftId(params.logBookAircraftId)
        }

    }, [params]);

    React.useEffect(() => {
        if (Destparams.childParam) {
            console.log('Child param is', Destparams.childParam);
            setFrom(Destparams.FromItemCode)
            setTo(Destparams.ToItemCode)
            // setFromLatdest(Destparams.fromItemlat)
            // setToLatdest(Destparams.ToItemlat)
            // setFromLongdest(Destparams.fromItemLong)
            // setToLongdest(Destparams.ToItemLong)
        }
    }, [Destparams]);

    React.useEffect(() => {
        if (Approachparams.childParam) {
            console.log('Child param is', Approachparams.childParam);
            setApproach1(Approachparams.ApproachMixture)
            setApproach2(Approachparams.ApproachMixture)
        }
    }, [Approachparams]);

    React.useEffect(() => {
        if (Listparams.childParamList) {
            console.log('Child param is', Listparams.childParamList);
            setListId(Listparams.itemId)
            setListDate(Listparams.itemDate)
            setListFlight(Listparams.itemFlight)
            setListAircraftType(Listparams.itemAircraftType)
            setListAircraftId(Listparams.itemAircraftId)
            setListFrom(Listparams.itemFrom)
            setListTo(Listparams.itemTo)
            setListChocksOff(Listparams.itemChocksOff)
            setListTakeOff(Listparams.itemTakeOff)
            setListLanding(Listparams.itemLanding)
            setListChocksOn(Listparams.itemChocksOn)
            setListRoute(Listparams.itemRoute)
            setListInstructor(Listparams.itemInstructor)
            setListP1(Listparams.itemP1)
            setListRc1(Listparams.itemRc1)
            setListRc2(Listparams.itemRc2)
            setListRc3(Listparams.itemRc3)
            setListRc4(Listparams.itemRc4)
            setListP2(Listparams.itemP2)
            setListStudent(Listparams.itemStudent)
            setListTotalTime(Listparams.itemTotalTime)
            setListDay(Listparams.itemDay)
            setListNight(Listparams.itemNight)
            setListAi(Listparams.itemAi)
            setListDualDay(Listparams.itemDualDay)
            setListDualNight(Listparams.itemDualNight)
            setListInstructional(Listparams.itemInstructional)
            setListFr(Listparams.itemFr)
            setListP1UsDay(Listparams.itemP1UsDay)
            setListP1UsNight(Listparams.itemP1UsNight)
            setListP1UtDay(Listparams.itemP1UtDay)
            setListP1UtNight(Listparams.itemP1UtNight)
            setListPicDay(Listparams.itemPicDay)
            setListPicNight(Listparams.itemPicNight)
            setListStl(Listparams.itemStl)
            setListSi(Listparams.itemSi)
            setListSicDay(Listparams.itemSicDay)
            setListSicNight(Listparams.itemSicNight)
            setListXcDay(Listparams.itemXcDay)
            setListXcNight(Listparams.itemXcNight)
            setListXcDayLegs(Listparams.itemDayLegs)
            setListXcNightLegs(Listparams.itemNightLegs)
            setListAutoLanding(Listparams.itemAutoLanding)
            setListDayLanding(Listparams.itemDayLanding)
            setListDayTO(Listparams.itemDayTO)
            setListFullStop(Listparams.itemFullStop)
            setListNightLanding(Listparams.itemNightLanding)
            setListNightTO(Listparams.itemNightTO)
            setListTouchGo(Listparams.itemTouchGo)
            setListWaterLanding(Listparams.itemWaterLanding)
            setListWaterTO(Listparams.itemWaterTO)
            setListApproach1(Listparams.itemApproach1)
            setListApproach2(Listparams.itemApproach2)
            setListRemark(Listparams.itemRemarks)
            // From Roaster
            setRosterDate(Listparams.RoasterDate)
            setRosterFrom(Listparams.RoasterFrom)
            setRosterChocksOff(Listparams.RoasterChocksOff)
            setRosterTo(Listparams.RoasterTo)
            setRosterChocksOn(Listparams.RoasterChocksOn)
            setRosterAType(Listparams.RoasterAType)
            setRosterAId(Listparams.RoasterAId)
            setRosterTotalTime(Listparams.RoasterTotalTime)
            setRosterNamePic(Listparams.RoasterNamePic)
            setRosterDayLand(Listparams.RoasterDayLanding)
            setRosterNightLand(Listparams.RoasterNightLanding)
            setRosterDayTakeOff(Listparams.RoasterDayTakeOff)
            setRosterNightTakeOff(Listparams.RoasterNightTakeOff)

        }
    }, [Listparams]);

    console.log('date --->', listDate)


    React.useEffect(() => {

        console.log('Child param is', Peopleparams.childParam);
        setPic_p1(Peopleparams.PICItemName)
        setSic_p2(Peopleparams.SICItemName)
        setInstructor(Peopleparams.InstructorItemName)
        setReliefCrew1(Peopleparams.Rc1ItemName)
        setReliefCrew2(Peopleparams.Rc2ItemName)
        setReliefCrew3(Peopleparams.Rc3ItemName)
        setReliefCrew4(Peopleparams.Rc4ItemName)
        setStudent(Peopleparams.StudentItemName)

    }, [Peopleparams]);

    const {
        // flight 1
        flightToggle,
        flightRoleToggle,
        routeToggle,

        //flight 2
        instructorToggle,
        Pic_toggle,
        rc1Toggle,
        rc2Toggle,
        rc3Toggle,
        rc4Toggle,
        sic_toggle,
        studentToggle,

        //Time
        totalTimeToggle,
        dayToggle,
        nightToggle,
        AiToggle,
        dualDayToggle,
        dualNightToggle,
        instructionalToggle,
        ifr_vfrToggle,
        p1_us_dayToggle,
        p1_us_nightToggle,
        p1_ut_dayToggle,
        p1_ut_nightToggle,
        Pic_dayToggle,
        Pic_nightToggle,
        stlToggle,
        siToggle,
        sic_dayToggle,
        sic_nightToggle,
        xc_dayToggle,
        xc_nightToggle,

        //Landing
        autoLandingToggle,
        dayLandingToggle,
        day_toToggle,
        fullStopToggle,
        nightLandingToggle,
        night_toToggle,
        touchGoToggle,
        waterLandingToggle,
        water_toToggle,

        //Approach
        Approach2Toggle,

        //simu
        STToggle,
        takeOffToggle,
        landingToggle,
        TO_totalTimeToggle,
        PFToggle,
        PMToggle,
        SFToggle,
    } = React.useContext(ConfigContext);
    console.log('flight check', flightToggle)
    //console.log('check===>', roleCheck)

    const Configuration = () => {
        setConfig(config => !config);
        setModalVisible(!modalVisible);
    };

    const HideDoneConfig = () => {
        setConfig(config => !config);
        //setModalVisible(!modalVisible);
    };

    const Add_Logbook = async () => {
        let user = await AsyncStorage.getItem('userdetails');
        user = JSON.parse(user);
        console.log('iddddddd', user.id)

        await fetch(BaseUrl + 'addLogbook', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "user_id": user.id,
                "date": Listparams.RoasterDate ? rosterDate : dateChoose,
                "flight_no": flight,
                "aircraftReg": Listparams.RoasterAId ? rosterAId : aircraftId.toUpperCase(),
                "aircraftType": Listparams.RoasterAType ? rosterAType : aircraft_name,
                "route": route,
                "from_city": Listparams.RoasterFrom ? rosterFrom : from,
                "to_city": Listparams.RoasterTo ? rosterTo : to,
                "instructor": instructor,
                "p1": Listparams.RoasterNamePic ? rosterNamePic : pic_p1,
                "reliefCrew1": reliefCrew1,
                "reliefCrew2": reliefCrew2,
                "reliefCrew3": reliefCrew3,
                "reliefCrew4": reliefCrew4,
                "p2": sic_p2,
                "student": student,
                "totalTime": Listparams.RoasterTotalTime ? rosterTotalTime : totalTime,
                "day": day,
                "night": night,
                "actual_Instrument": ai,
                "dual_day": dual_day,
                "dual_night": dual_night,
                "instructional": instructional,
                "ifr_vfr": fr,
                "p1_us_day": p1_us_day,
                "p1_us_night": p1_us_night,
                "p1_ut_day": p1_ut_day,
                "p1_ut_night": p1_ut_night,
                "pic_day": pic_day,
                "pic_night": pic_night,
                "sim_instrument": si,
                "stl": stl,
                "sic_day": sic_day,
                "sic_night": sic_night,
                "x_country_day": xc_day,
                "x_country_night": xc_night,
                "x_country_day_leg": xc_day_leg,
                "x_country_night_leg": xc_night_leg,
                "dayLanding": Listparams.RoasterDayLanding ? rosterDayLand : dayLanding,
                "nightLanding": Listparams.RoasterNightLanding ? rosterNightLand : nightLanding,
                "dayTO": Listparams.RoasterDayTakeOff ? rosterDayTakeOff : day_to,
                "nightTO": Listparams.RoasterNightTakeOff ? rosterNightTakeOff : night_to,
                "waterLanding": waterLanding,
                "waterTO": water_to,
                "touch_n_gos": touchGo,
                "fullStop": fullStop,
                "autolanding": autoLanding,
                "remark": remark,
                "approach1": approach1,
                "onTime": Listparams.RoasterChocksOn ? rosterChocksOn : chocksOn,
                "offTime": Listparams.RoasterChocksOff ? rosterChocksOff : chocksOff,
                "inTime": landing,
                "outTime": takeOff,
                "sim_type": St,
                "simLocation": location,
                "sim_exercise": Sim_exercise,
                "totaltime_landing": totalTo_time,
                "pf_hours": pfHours,
                "pm_hours": pmHours,
                "sfi_sfe": sf
            })
        }).then(res => res.json())
            .then(resData => {
                console.log('data---->', resData.data);
                Alert.alert(resData.message);
            });
    };

    console.log('SIIIICC', pic_p1)

    const deleteLogbbok = async () => {
        let user = await AsyncStorage.getItem('userdetails');
        user = JSON.parse(user);

        await fetch(BaseUrl + 'deletelogbook', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "user_id": user.id,
                "id": listId
            })
        }).then(res => res.json())
            .then(resData => {
                console.log('data---->', resData.data);
                Alert.alert(resData.message);
            });
    };

    const updateLogbbok = async () => {
        let user = await AsyncStorage.getItem('userdetails');
        user = JSON.parse(user);
        console.log('iddddddd', user.id)

        await fetch(BaseUrl + 'updateLogbook', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "user_id": user.id,
                "id": listId,
                "from_city": listFrom,
                "to_city": listTo,
                "p1": listP1,
                "p2": listP2,
                "date": listDate,
                "flight_no": listFlight,
                "aircraftReg": listAircraftId,
                "aircraftType": listAircraftType,
                "route": listRoute,
                "totalTime": listTotalTime,
                "day": listDay,
                "night": listNight,
                "actual_Instrument": listAi,
                "dual_day": listDualDay,
                "dual_night": listDualNight,
                "ifr_vfr": listFr,
                "p1_us_day": listP1UsDay,
                "p1_us_night": listP1UsNight,
                "p1_ut_day": listP1UtDay,
                "p1_ut_night": listP1UtNight,
                "pic_day": listPicDay,
                "pic_night": listPicNight,
                "sim_instrument": listSi,
                "stl": listStl,
                "sic_day": listSicDay,
                "sic_night": listSicNight,
                "x_country_day": listXcDay,
                "x_country_night": listXcNight,
                "x_country_day_leg": listXcDayLegs,
                "x_country_night_leg": listXcNightLegs,
                "dayLanding": listDayLanding,
                "nightLanding": listNightLanding,
                "dayTO": listDayTO,
                "nightTO": listNightTO,
                "remark": listRemark,
                "approach1": listApproach1,
                "approach2": listApproach2,
                "onTime": listChocksOn,
                "offTime": listChocksOff,
                "inTime": listLanding,
                "outTime": listTakeOff,
                "reliefCrew1": reliefCrew1,
                "reliefCrew2": reliefCrew2,
                "reliefCrew3": reliefCrew3,
                "reliefCrew4": reliefCrew4,
                "instructor": instructor,
                "instructional": listInstructional,
                "student": student,
                "waterLanding": waterLanding,
                "waterTO": water_to,
                "touch_n_gos": touchGo,
                "fullStop": fullStop,
                "autolanding": autoLanding,
            })
        }).then(res => res.json())
            .then(resData => {
                console.log('data---->', resData.data);
                Alert.alert(resData.message);
            });
    };
    //Alert.alert('params from roaster import', Listparams.RoasterImportData)
    console.log('name_PIc------>', rosterNamePic)

    //sqlite starts

    React.useEffect(() => { getDataQuery() }, []);

    const getDataQuery = async () => {
        let user = await AsyncStorage.getItem('userdetails');
        user = JSON.parse(user);
        let temData = [];
        prePopulateddb.transaction(tx => {
            tx.executeSql('SELECT latitude,longitude from Airport_table WHERE ident = "' + from + '" OR ident = "' + rosterFrom + '" OR ICAO_code = "' + rosterFrom + '" OR ICAO_code = "' + from + '" OR IATA_code = "' + from + '" OR IATA_code = "' + rosterFrom + '" OR ident = "' + to + '" OR ident = "' + rosterTo + '" OR ICAO_code = "' + rosterTo + '" OR ICAO_code = "' + to + '" OR IATA_code = "' + rosterTo + '" OR IATA_code = "' + to + '"', [], (tx, result) => {
                //setOffset(offset + 10);
                if (result.rows.length > 0) {
                    //alert('data available ');
                    console.log('result', result)
                }
                for (let i = 0; i <= result.rows.length; i++) {
                    temData.push({
                        lat: result.rows.item(i).latitude,
                        long: result.rows.item(i).longitude,
                    });
                    console.log('airport data', temData);
                    console.log('fromLat', result.rows.item(0).latitude)
                    console.log('fromLong', result.rows.item(0).longitude)
                    console.log('toLat', result.rows.item(1).latitude)
                    console.log('toLong', result.rows.item(1).longitude)
                    //setData(temData);
                    //setFilteredData(temData);
                    setFromLatdest(result.rows.item(0).latitude)
                    setToLatdest(result.rows.item(0).longitude)
                    setFromLongdest(result.rows.item(1).latitude)
                    setToLongdest(result.rows.item(1).longitude)
                }
            });
        });
    };

    //sqlite ends

    //date settings starts

    //const OnlyDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
    const onlyDate = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear()
    console.log('onlyDate', onlyDate);
    const ddmmyy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
    const mmddyy = (date.getMonth() + 1) + "-" + date.getDate() + "-" + date.getFullYear()

    //date settings stops

    //calculations 
    // var ChocksOffTime = chocksOff === '' ? '00:00' : chocksOff //H = in hours
    // var ChocksOnTime = chocksOn === '' ? '00:00' : chocksOn //H = in hours

    // var ChocksOffTimeTemp = ChocksOffTime.split(":");
    // var ChocksOnTimeTemp = ChocksOnTime.split(":");

    // if(isNaN(ChocksOffTimeTemp[1])){
    //     ChocksOffTimeTemp[1] = '0'
    // }
    // if(isNaN(ChocksOnTimeTemp[1])){
    //     ChocksOnTimeTemp[1] = '0'
    // }

    // var ChocksOffTimeM1 = (Number(ChocksOffTimeTemp[0]) * 60) + Number(ChocksOffTimeTemp[1]); //M = in Minuts
    // var ChocksOnTimeM2 = (Number(ChocksOnTimeTemp[0]) * 60) + Number(ChocksOnTimeTemp[1]); //M = in Minuts

    // var ChocksTDiffTimeM = (Number(ChocksOnTimeM2) - Number(ChocksOffTimeM1))

    // var Chockshours = Math.floor(ChocksTDiffTimeM / 60);  
    // var Chocksminutes = ChocksTDiffTimeM % 60;
    // if (Chocksminutes + ''.length < 2) {
    //     Chocksminutes = '0' + Chocksminutes; 
    // }

    // const TotalChocksTime = Chockshours + ":" + Chocksminutes
    // console.log('total Chocks',TotalChocksTime );


    //Get Data from form

    const day_night_calc = () => {

        var depT = Listparams.RoasterChocksOff ? rosterChocksOff : chocksOff; 
        let onlyDate1 = '';
        var depTemp = depT.split(":");
        var depH = Number(depTemp[0]);
        var depM = Number(depTemp[1]);
        var arrT = Listparams.RoasterChocksOn ? rosterChocksOn : chocksOn;
        var arrTemp = arrT.split(":");
        var arrH = Number(arrTemp[0]);
        var arrM = Number(arrTemp[1]);
        //var dateOfFlight = new Date('2021-07-22'); //yyyy-mm-dd
        var departureTimeM = new Date('2021-07-22'); //yyyy-mm-dd
        onlyDate1 = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
        console.log('dateSet', onlyDate1)
        var dateOfFlight =  new Date(onlyDate1);
        console.log("flight date hello", dateOfFlight);
        var departureTimeM = new Date ( onlyDate1 );
        var test = departureTimeM.setHours(depH);
        departureTimeM.setMinutes(depM);
        var arrivelTimeM = new Date(onlyDate1); //yyyy-mm-dd
        //var arrivelTimeM = new Date ( ddmmyy );
        arrivelTimeM.setHours(arrH);
        arrivelTimeM.setMinutes(arrM);
        var fromLat = fromLatdest;
        var fromLong = fromLongdest;
        var toLat = toLatdest;
        var toLong = toLongdest;

        //console.log('date Of Flight', date);


        console.log(depT);
        console.log(arrT);
        console.log(dateOfFlight);
        console.log(departureTimeM);
        console.log('From lat', fromLat);
        console.log('From long', fromLong);
        console.log('to lat', toLat);
        console.log('to long', toLong);
        console.log('test', test);

        //total flight time
        var filghtTimeM = getFlightTime(dateToMinuts(departureTimeM), dateToMinuts(arrivelTimeM));
        if (departureTimeM > arrivelTimeM && departureTimeM.getHours() > 20 && arrivelTimeM.getHours() < 10) {
            departureTimeM.setDate(departureTimeM.getDate() - 1);
        } else if (departureTimeM > arrivelTimeM && departureTimeM.getHours() > 12 && arrivelTimeM.getHours() < 12) {
            arrivelTimeM.setDate(arrivelTimeM.getDate() + 1);
        }

        //Distance of departure and destination
        var distance = distance(fromLat, fromLong, toLat, toLong, 'N'); // in nm

        //SunRise and SunSet of departure and destination
        var sunRiseDepartureTimeM = (solar_event(dateOfFlight, fromLat, fromLong, true, 90.833333));//23:30
        var sunSetDepartureTimeM = (solar_event(dateOfFlight, fromLat, fromLong, false, 90.833333));//12:30
        var sunRiseDestinationTimeM = (solar_event(dateOfFlight, toLat, toLong, true, 90.833333));//00:36
        var sunSetDestinationTimeM = (solar_event(dateOfFlight, toLat, toLong, false, 90.833333));//13:11
        if (sunRiseDepartureTimeM.getHours() > 20) {
            sunRiseDepartureTimeM.setDate(sunRiseDepartureTimeM.getDate() - 1);
        }
        if (sunRiseDestinationTimeM.getHours() > 20) {
            sunRiseDestinationTimeM.setDate(sunRiseDestinationTimeM.getDate() - 1);
        }
        if (sunSetDepartureTimeM.getHours() < 5) {
            sunSetDepartureTimeM.setDate(sunSetDepartureTimeM.getDate() + 1);
        }
        if (sunSetDestinationTimeM.getHours() < 5) {
            sunSetDestinationTimeM.setDate(sunSetDestinationTimeM.getDate() + 1);
        }

        // for pilots rules add 30 mints in sun sunsets and subtract 30 mints in sunrises
        sunRiseDepartureTimeM.setMinutes(sunRiseDepartureTimeM.getMinutes() - 30);
        sunSetDepartureTimeM.setMinutes(sunSetDepartureTimeM.getMinutes() + 30);
        sunRiseDestinationTimeM.setMinutes(sunRiseDestinationTimeM.getMinutes() - 30);
        sunSetDestinationTimeM.setMinutes(sunSetDestinationTimeM.getMinutes() + 30);


        //Speed of AirCraft
        var airCraftSpeed = distance / filghtTimeM;


        // console.log("departureTimeM "+departureTimeM);
        // console.log("arrivelTimeM "+arrivelTimeM);
        // console.log("sunRiseDepartureTimeM "+sunRiseDepartureTimeM);
        // console.log("sunRiseDestinationTimeM "+sunRiseDestinationTimeM);
        // console.log("sunSetDepartureTimeM "+sunSetDepartureTimeM);
        // console.log("sunSetDestinationTimeM "+sunSetDestinationTimeM);


        if ((sunRiseDepartureTimeM < sunRiseDestinationTimeM) && (sunSetDepartureTimeM < sunSetDestinationTimeM)) {
            console.log('west');
            //difference in sunrises
            var diffSunRiseTime = (dateToMinuts(sunRiseDestinationTimeM) - dateToMinuts(sunRiseDepartureTimeM));
            if (diffSunRiseTime < 0) { diffSunRiseTime = diffSunRiseTime + 1440; }
            if (diffSunRiseTime > 1440) { diffSunRiseTime = diffSunRiseTime - 1440; }

            var diffSunSetTime = (dateToMinuts(sunSetDestinationTimeM) - dateToMinuts(sunSetDepartureTimeM));
            if (diffSunSetTime < 0) { diffSunSetTime = diffSunSetTime + 1440; }
            if (diffSunSetTime > 1440) { diffSunSetTime = diffSunSetTime - 1440; }

            if ((departureTimeM < sunRiseDepartureTimeM) && (arrivelTimeM > sunRiseDestinationTimeM)) {

                //speed difference betwwen sun and aircraft
                var sunSpeed = distance / diffSunRiseTime;
                var SpeedDiff = sunSpeed - airCraftSpeed;

                // calculate time diffrence between Departure time and sunrise time
                var flyingTimeBeforeSR = dateToMinuts(sunRiseDepartureTimeM) - dateToMinuts(departureTimeM);

                //AirCraft Travel in flyingTimeBeforeSRM Time
                // var travelBeforeSR = (distance * flyingTimeBeforeSR) / filghtTimeM;
                var travelBeforeSR = airCraftSpeed * flyingTimeBeforeSR;

                //Time taken by sun to catch up the flight in minuts
                var catchupTime = travelBeforeSR / SpeedDiff;

                //So totaltime travel in night
                var nightTime = catchupTime + flyingTimeBeforeSR;
                if (nightTime <= 0) {
                    nightTime = 0;
                } else if (nightTime > filghtTimeM) {
                    nightTime = filghtTimeM;
                }

                // So DayTime = TotalTime - NightTime
                var dayTime = filghtTimeM - nightTime;

                var dayTimeH = timeConvert(dayTime);
                var filghtTimeH = timeConvert(filghtTimeM);
                var nightTimeH = timeConvert(nightTime);

                var result = "N-D : filghtTimeM:  " + setfilghtTimeM(filghtTimeH) + "------nightTime:  " + setNightTime(nightTimeH) + "----DayTime: " + setDayTime(dayTimeH);
                //$('#result').html(result);
                console.log(result)

            } else if ((departureTimeM > sunRiseDepartureTimeM) && (arrivelTimeM < sunSetDestinationTimeM)) {

                var dayTime = filghtTimeM;
                var nightTime = 0;

                var dayTimeH = timeConvert(dayTime);
                var filghtTimeH = timeConvert(filghtTimeM);
                var nightTimeH = timeConvert(nightTime);

                var result = "D-D : filghtTimeM:  " + setfilghtTimeM(filghtTimeH) + "------nightTime:  " + setNightTime(nightTimeH) + "----DayTime: " + setDayTime(dayTimeH);
                //$('#result').html(result);
                console.log(result)

            } else if ((departureTimeM > sunSetDepartureTimeM) && (arrivelTimeM > sunSetDestinationTimeM)) {

                var nightTime = filghtTimeM;
                var dayTime = 0;

                var dayTimeH = timeConvert(dayTime);
                var filghtTimeH = timeConvert(filghtTimeM);
                var nightTimeH = timeConvert(nightTime);

                var result = "N-N : filghtTimeM:  " + setfilghtTimeM(filghtTimeH) + "------nightTime:  " + setNightTime(nightTimeH) + "----DayTime: " + setDayTime(dayTimeH);
                //$('#result').html(result);
                console.log(result)

            } else if ((departureTimeM < sunSetDepartureTimeM) && (arrivelTimeM > sunSetDestinationTimeM)) {
                // total day time
                var dayTime = dateToMinuts(sunSetDepartureTimeM) - dateToMinuts(departureTimeM);

                // calculate Sun speed
                var sunSpeedM = distance / diffSunSetTime; // in nm

                // crossing speed
                var crossingSpeed = sunSpeedM - airCraftSpeed;

                // travel by AirCraft in day time of flight
                var travelAC = airCraftSpeed * dayTime;

                var distanceLeft = distance - travelAC;

                if (distanceLeft <= 50) {
                    var totalDayTime = filghtTimeM;
                } else {
                    var time_by_sun_to_catch_aircraft = travelAC / crossingSpeed;
                    var totalDayTime = dayTime + time_by_sun_to_catch_aircraft;
                }
                var nightTime = filghtTimeM - totalDayTime;

                var dayTimeH = timeConvert(totalDayTime);
                var filghtTimeH = timeConvert(filghtTimeM);
                var nightTimeH = timeConvert(nightTime);

                var result = "D-N : filghtTimeM:  " + setfilghtTimeM(filghtTimeH) + "------nightTime:  " + setNightTime(nightTimeH) + "----DayTime: " + setDayTime(dayTimeH);
                //$('#result').html(result);
                console.log(result)
            }
        } else {
            console.log('east');
            //difference in sunrises
            var diffSunRiseTime = (dateToMinuts(sunRiseDepartureTimeM) - dateToMinuts(sunRiseDestinationTimeM));
            if (diffSunRiseTime < 0) { diffSunRiseTime = diffSunRiseTime + 1440; }
            if (diffSunRiseTime > 1440) { diffSunRiseTime = diffSunRiseTime - 1440; }

            var diffSunSetTime = (dateToMinuts(sunSetDepartureTimeM) - dateToMinuts(sunSetDestinationTimeM));
            if (diffSunSetTime < 0) { diffSunSetTime = diffSunSetTime + 1440; }
            if (diffSunSetTime > 1440) { diffSunSetTime = diffSunSetTime - 1440; }

            if ((departureTimeM < sunRiseDepartureTimeM) && (arrivelTimeM > sunRiseDestinationTimeM)) {


                //speed difference betwwen sun and aircraft
                var sunSpeed = distance / diffSunRiseTime;
                var crossingSpeed = sunSpeed + airCraftSpeed;

                // calculate time diffrence between Departure time and sunrise time
                var sunRiseAndDepTime = dateToMinuts(sunRiseDepartureTimeM) - dateToMinuts(departureTimeM);
                if (sunRiseAndDepTime < 0) {
                    sunRiseAndDepTime = 1440 + sunRiseAndDepTime;
                }
                if (sunRiseAndDepTime > 1440) {
                    sunRiseAndDepTime = 1440 - sunRiseAndDepTime;
                }

                //distance bt sun at departure
                var sunAndDepTravel = sunSpeed * sunRiseAndDepTime;


                //aircraft will meet sun at time
                var meetingTime = sunAndDepTravel / crossingSpeed;

                //So totaltime travel in night
                var nightTime = meetingTime;
                if (nightTime <= 0) {
                    nightTime = 0;
                } else if (nightTime > filghtTimeM) {
                    nightTime = filghtTimeM;
                }

                // So DayTime = TotalTime - NightTime
                var dayTime = filghtTimeM - nightTime;

                var dayTimeH = timeConvert(dayTime);
                var filghtTimeH = timeConvert(filghtTimeM);
                var nightTimeH = timeConvert(nightTime);

                var result = "N-D : filghtTimeM:  " + setfilghtTimeM(filghtTimeH) + "------nightTime:  " + setNightTime(nightTimeH) + "----DayTime: " + setDayTime(dayTimeH);
                //$('#result').html(result);
                console.log(result)

            } else if ((departureTimeM >= sunRiseDepartureTimeM) && (arrivelTimeM <= sunSetDestinationTimeM)) {

                var dayTime = filghtTimeM;
                var nightTime = 0;

                var dayTimeH = timeConvert(dayTime);
                var filghtTimeH = timeConvert(filghtTimeM);
                var nightTimeH = timeConvert(nightTime);

                var result = "D-D : filghtTimeM:  " + setfilghtTimeM(filghtTimeH) + "------nightTime:  " + setNightTime(nightTimeH) + "----DayTime: " + setDayTime(dayTimeH);
                //$('#result').html(result);
                console.log(result)

            } else if ((departureTimeM >= sunSetDepartureTimeM) && (arrivelTimeM >= sunSetDestinationTimeM)) {

                var nightTime = filghtTimeM;
                var dayTime = 0;

                var dayTimeH = timeConvert(dayTime);
                var filghtTimeH = timeConvert(filghtTimeM);
                var nightTimeH = timeConvert(nightTime);

                var result = "N-N : filghtTimeM:  " + setfilghtTimeM(filghtTimeH) + "------nightTime:  " + setNightTime(nightTimeH) + "----DayTime: " + setDayTime(dayTimeH);
                //$('#result').html(result);
                console.log(result)

            } else if ((departureTimeM < sunSetDepartureTimeM) && (arrivelTimeM > sunSetDestinationTimeM)) {


                // total day time
                var dayTime = dateToMinuts(sunSetDepartureTimeM) - dateToMinuts(departureTimeM);

                // calculate Sun speed
                var sunSpeedM = distance / diffSunSetTime; // in nm

                var crossingSpeed = sunSpeedM + airCraftSpeed;

                // travel by sun in day time of flight
                // var travelSun = sunSpeedM * dayTime;

                // travel by AirCraft in day time of flight
                var travelAC = airCraftSpeed * dayTime;

                // Aircraft will travel this distance in this time=
                var acDayTime = travelAC / crossingSpeed;

                // var sunDayTime = sunDayTravel / airCraftSpeed;


                totalDayTime = dayTime - acDayTime;

                if (totalDayTime > filghtTimeM) {
                    totalDayTime = totalDayTime - filghtTimeM;
                } else if (totalDayTime < 0) {
                    totalDayTime = filghtTimeM;
                }
                var nightTime = filghtTimeM - totalDayTime;

                var dayTimeH = timeConvert(totalDayTime);
                var filghtTimeH = timeConvert(filghtTimeM);
                var nightTimeH = timeConvert(nightTime);

                var result = "D-N : filghtTimeM:  " + setfilghtTimeM(filghtTimeH) + "------nightTime:  " + setNightTime(nightTimeH) + "----DayTime: " + setDayTime(dayTimeH);
                //$('#result').html(result);
                console.log(result)

            }
        }

        // functions

        function getFlightTime(departureTimeM, arrivelTimeM) {
            if (arrivelTimeM > departureTimeM) {
                filghtTimeM = arrivelTimeM - departureTimeM;
            } else if (departureTimeM > 720) {
                var ab = (1440 - departureTimeM);
                filghtTimeM = arrivelTimeM + ab;
            }
            return filghtTimeM;
        }

        function dateToMinuts(timeH) {
            var timeM = (timeH.getHours() * 60) + (timeH.getMinutes()); //M = in Minuts
            return timeM;
        }

        function hourToMinuts(timeH) {
            var timeTemp = timeH.split(":");
            var timeM = (Number(timeTemp[0]) * 60) + Number(timeTemp[1]); //M = in Minuts
            return timeM;
        }

        function timeConvert(n) {
            var num = n;
            var hours = (num / 60);
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            if (rminutes < 10) { rminutes = "0" + rminutes; }
            return rhours + ":" + rminutes;
        }

        function solar_event(date, latitude, longitude, rising, zenith) {
            var year = date.getFullYear(),
                month = date.getMonth() + 1,
                day = date.getDate();
            var floor = Math.floor,
                degtorad = function (deg) {
                    return Math.PI * deg / 180;
                },
                radtodeg = function (rad) {
                    return 180 * rad / Math.PI;
                },
                sin = function (deg) {
                    return Math.sin(degtorad(deg));
                },
                cos = function (deg) {
                    return Math.cos(degtorad(deg));
                },
                tan = function (deg) {
                    return Math.tan(degtorad(deg));
                },
                asin = function (x) {
                    return radtodeg(Math.asin(x));
                },
                acos = function (x) {
                    return radtodeg(Math.acos(x));
                },
                atan = function (x) {
                    return radtodeg(Math.atan(x));
                },
                modpos = function (x, m) {
                    return ((x % m) + m) % m;
                };

            // 1. first calculate the day of the year
            var N1 = floor(275 * month / 9),
                N2 = floor((month + 9) / 12),
                N3 = (1 + floor((year - 4 * floor(year / 4) + 2) / 3)),
                N = N1 - (N2 * N3) + day - 30;

            // 2. convert the longitude to hour value and calculate an approximate time
            var lngHour = longitude / 15,
                t = N + (((rising ? 6 : 18) - lngHour) / 24);

            // 3. calculate the Sun's mean anomaly
            var M = (0.9856 * t) - 3.289;

            // 4. calculate the Sun's true longitude
            var L = M + (1.916 * sin(M)) + (0.020 * sin(2 * M)) + 282.634;
            L = modpos(L, 360); // NOTE: L potentially needs to be adjusted into the range [0,360) by adding/subtracting 360
            // 5a. calculate the Sun's right ascension
            var RA = atan(0.91764 * tan(L));
            RA = modpos(RA, 360); // NOTE: RA potentially needs to be adjusted into the range [0,360) by adding/subtracting 360
            // 5b. right ascension value needs to be in the same quadrant as L
            var Lquadrant = (floor(L / 90)) * 90,
                RAquadrant = (floor(RA / 90)) * 90;
            RA = RA + (Lquadrant - RAquadrant);

            // 5c. right ascension value needs to be converted into hours
            RA = RA / 15;

            // 6. calculate the Sun's declination
            var sinDec = 0.39782 * sin(L),
                cosDec = cos(asin(sinDec));

            // 7a. calculate the Sun's local hour angle
            var cosH = (cos(zenith) - (sinDec * sin(latitude))) / (cosDec * cos(latitude));
            var H;

            if (cosH > 1) {
                return undefined; // the sun never rises on this location (on the specified date)
            } else if (cosH < -1) {
                return undefined; // the sun never sets on this location (on the specified date)
            }

            // 7b. finish calculating H and convert into hours
            if (rising) {
                H = 360 - acos(cosH);
            } else {
                H = acos(cosH);
            }
            H = H / 15;

            // 8. calculate local mean time of rising/setting
            var T = H + RA - (0.06571 * t) - 6.622;

            // 9. adjust back to UTC
            var UT = T - lngHour;
            UT = modpos(UT, 24); // NOTE: UT potentially needs to be adjusted into the range [0,24) by adding/subtracting 24
            // console.log(UT);

            var hours = floor(UT),
                minutes = Math.round(60 * (UT - hours));
            var result = new Date(year, month - 1, day, hours, minutes);
            console.log('resultssssss->', result)
            return result;
        }

        function distance(lat1, lon1, lat2, lon2, unit) {
            if ((lat1 == lat2) && (lon1 == lon2)) {
                return 0;
            }
            else {
                var radlat1 = Math.PI * lat1 / 180;
                var radlat2 = Math.PI * lat2 / 180;
                var theta = lon1 - lon2;
                var radtheta = Math.PI * theta / 180;
                var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                if (dist > 1) {
                    dist = 1;
                }
                dist = Math.acos(dist);
                dist = dist * 180 / Math.PI;
                dist = dist * 60 * 1.1515;
                if (unit == "K") { dist = dist * 1.609344 }
                if (unit == "N") { dist = dist * 0.8684 }
                return dist;
            }
        }

    }
    //calculations ends

    const resetHandler = () => {
        setFlight("");
        setAircraft_name("")
        setAircraftId("")
        setFrom("")
        setTo("")
        setChocksOff("")
        setChocksOn("")
        setTakeOff("")
        setLanding("")
        setRoute("")
        setPic_p1("")
        setSic_p2("")
        setInstructor("")
        setReliefCrew1("")
        setReliefCrew2("")
        setReliefCrew3("")
        setReliefCrew4("")
        setStudent("")
        setDay("")
        setNight("")
        setAi("")
        setDual_day("")
        setDual_night("")
        setP1_us_day("")
        setP1_us_night("")
        setP1_ut_day("")
        setP1_ut_night("")
        setPic_day("")
        setPic_night("")
        setStl(false)
        setSi("")
        setSic_day("")
        setSic_night("")
        setXc_day("")
        setXc_night("")
        setXc_dayLeg("")
        setXc_nightLeg("")
        setInstructional("")
        setAutoLanding("")
        setDayLanding("")
        setDay_to("")
        setNightLanding("")
        setNight_to("")
        setFullStop("")
        setTouchGo("")
        setWaterLanding("")
        setWater_to("")
        setApproach1("")
        setRemark("")

        setModalVisible(false);

    }

    // for custom fileds starts

    // const inputs: JSX.Element[] = [];
    // for (let i = 0; i<numInputs; i++)
    // {
    //     inputs.push(
    //         <TouchableOpacity onPress={()=> navigation.navigate('People', {from:'pic'})}>
    //             <View style={Logbook.fieldWithoutBottom}>
    //             <View style={Logbook.fields}>
    //             <Text style={{...Logbook.fieldText, ...{lineHeight:35,}}}>PIC/P1<Text style={{color:'red'}}>*</Text></Text>
    //             <View style={{justifyContent: 'flex-end',flexDirection:'row'}}>
    //             <Text style={{...Logbook.fieldText, ...{lineHeight:35}}}>{Listparams.RoasterNamePic?rosterNamePic: !pic_p1? setPic_p1('Self'): sic_p2==='Self'?pic_p1: pic_p1}</Text>
    //             <TouchableOpacity onPress={()=> navigation.navigate('People', {from:'pic'})}>
    //             <MaterialCommunityIcons  
    //             name="chevron-right" color={'#256173'} size={25} style={{lineHeight:35}}/>
    //             </TouchableOpacity>
    //             </View>
    //             </View>
    //             </View>
    //         </TouchableOpacity>
    //     )
    // }

    // for custom fileds ends

    console.log('date', datee);

    const logic = (P1, P2) => {
        if (P1 === 'Self') {
            P1 = rosterNamePic
            P2 != 'Self'
            return P1
        }
        else if (P2 === 'Self') {
            P2 = listP2
            rosterNamePic = ''
            return P2
        }
    }

    const flightDate = (event, selectedDate) => {
        console.log('selected date',selectedDate )
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    console.log('date ---- >', date)

    return (

        <SafeAreaView style={modalVisible || FlightmodalVisible || TimemodalVisible || LandingmodalVisible ? { ...Logbook.container, ...{ backgroundColor: 'rgba(0,0,0,0.4)' } } : Logbook.container}>

            <View style={{ flexDirection: 'row', borderBottomColor: '#393F45', borderBottomWidth: 0.5, width: '100%' }}>
                <TouchableOpacity onPress={() => navigation.navigate('LogBookListing')}>
                    <MaterialCommunityIcons
                        name="arrow-left" color={'#000'} size={30} style={Platform.OS === 'android' ? { padding: 15, } :
                            { padding: 15, paddingTop: 40 }}
                    />
                </TouchableOpacity>
                <Text style={Platform.OS === 'android' ? Logbook.header : Logbook.headerIos}>Back</Text>
                {config ?
                    <Text onPress={HideDoneConfig} style={{ color: '#256173', fontSize: 18, paddingTop: Platform.OS === 'ios' ? 43 : 18, paddingLeft: 120 }}>Done Config</Text> :
                    null}
            </View>

            <ScrollView>
                <TouchableOpacity onPress={day_night_calc}>
                    <View style={Logbook.headline}>
                        <Text style={Logbook.HeadlineText}>Flight</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={showDatepicker} >
                    <View style={Logbook.fieldWithoutBottom}>
                        <View style={Logbook.fields}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Date <Text style={{ color: 'red' }}>*</Text></Text>
                            {/* <DatePicker
                    //style={styles.datePickerStyle}
                    date={Listparams.RoasterDate? rosterDate : date} // Initial date from state
                    //date={date} // Initial date from state
                    mode="date" // The enum of date, datetime and time
                    placeholder="Date"
                    placeholderTextColor = "#266173"
                    format={datee == 'DDMM'? "DD-MM-YYYY" : "MM-DD-YYYY"}
                    format={"DD-MM-YYYY"}
                    //minDate="01-01-2016"
                    //maxDate="01-01-2019"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    suffixIcon={null}
                    customStyles={{
                        dateInput: {
                        //borderWidth:0.2,
                        //borderRadius: 5,
                        borderColor: 'transparent',
                        width: '100%',
                        marginRight:-80,
                        //padding:20,
                        },
                        dateIcon: {
                        width:0,
                        height:0,
                        },
                    }}
                    onDateChange={date => setDate(date)}
                    // onDateChange={(date) => {setDate({date})}}
                    /> */}
                            {show && (
                                <DateTimePicker
                                    testID="dateTimePicker"
                                    value={date}
                                    // mode={mode}
                                    //is24Hour={true}
                                    display="default"
                                    onChange={flightDate}
                                />
                            )}
                            {/* <Text style={{paddingTop:8}}>{onlyDate}</Text> */}
                            <Text style={{ paddingTop: 8 }}>{datee == 'DDMM' ? ddmmyy : mmddyy}</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                {flightToggle == true && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Flight</Text>
                        <TextInput
                            placeholder='Flight'
                            placeholderTextColor='grey'
                            value={flight}
                            onChangeText={(flight) => setFlight(flight)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View> : null}

                <TouchableOpacity onPress={() => navigation.navigate('Aircraft', { fromScreenLogbook: 'createLogBook' })}>
                    <View style={Logbook.fieldWithoutBottom}>
                        <View style={Logbook.fields}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Aircraft Type <Text style={{ color: 'red' }}>*</Text></Text>
                            <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                                <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35 } }}>{Listparams.RoasterAType ? rosterAType : aircraft_name}</Text>
                                <TouchableOpacity onPress={() => { navigation.navigate('SetAircraft') }}>
                                    <MaterialCommunityIcons
                                        name="alert-circle-outline" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>

                <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Aircraft ID <Text style={{ color: 'red' }}>*</Text></Text>
                        <Text style={{ fontSize: 10, }}>(Type "SIMU" for Simulator Menu)</Text>
                        <TextInput
                            placeholder='Aircraft ID'
                            placeholderTextColor='grey'
                            value={Listparams.RoasterAId ? rosterAId : aircraftId.toUpperCase()}
                            onChangeText={(aircraftId) => setAircraftId(aircraftId)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View>

                {STToggle && aircraftId.toUpperCase() === 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Simulator Type</Text>
                        {/* <Text style={{fontSize:10,}}>(Type "SIMU" for Simulator Menu)</Text> */}
                        <TextInput
                            placeholder='Simulator Type'
                            placeholderTextColor='grey'
                            value={St}
                            onChangeText={(St) => setSt(St)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View> : null}

                <TouchableOpacity onPress={() => navigation.navigate('Destination', { from: 'From' })}>
                    <View style={Logbook.fieldWithoutBottom}>
                        <View style={Logbook.fields}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>From<Text style={{ color: 'red' }}>*</Text></Text>
                            <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                                <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35 } }}>{Listparams.RoasterFrom ? rosterFrom : from}</Text>
                                <TouchableOpacity onPress={() => navigation.navigate('SetDestination')}>
                                    <MaterialCommunityIcons
                                        name="alert-circle-outline" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate('Destination', { from: 'to' })}>
                    <View style={Logbook.fieldWithoutBottom}>
                        <View style={Logbook.fields}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>To<Text style={{ color: 'red' }}>*</Text></Text>
                            <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                                <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35 } }}>{Listparams.RoasterTo ? rosterTo : to}</Text>
                                <TouchableOpacity onPress={() => navigation.navigate('SetDestination')}>
                                    <MaterialCommunityIcons
                                        name="alert-circle-outline" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>

                {aircraftId.toUpperCase() === 'SIMU' && (<View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Location<Text style={{ color: 'red' }}>*</Text></Text>
                        {/* <Text style={{fontSize:10,}}>(Type "SIMU" for Simulator Menu)</Text> */}
                        <TextInput
                            placeholder='Location'
                            placeholderTextColor='grey'
                            value={location}
                            onChangeText={(location) => setLocation(location)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View>)}

                {aircraftId.toUpperCase() === 'SIMU' && (<View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Sim_exercise</Text>
                        {/* <Text style={{fontSize:10,}}>(Type "SIMU" for Simulator Menu)</Text> */}
                        <TextInput
                            placeholder='Sim_exercise'
                            placeholderTextColor='grey'
                            value={Sim_exercise}
                            onChangeText={(Sim_exercise) => setSim_exercise(Sim_exercise)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View>)}

                {aircraftId.toUpperCase() === 'SIMU' && (<View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Instructor</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35 } }}>{Listparams.itemInstructor ? listInstructor : instructor}</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('People', { from: 'instructor' })}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>)}

                {/* chocks-off/chocks-on */}


                {aircraftId.toUpperCase() !== 'SIMU' && (<View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Chocks Off<Text style={{ color: 'red' }}>*</Text></Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.RoasterChocksOff ? rosterChocksOff : chocksOff}
                            onChangeText={chocksOff => setChocksOff(chocksOff)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View>)}

                {takeOffToggle ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Take-Off </Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.itemTakeOff ? listTakeOff : takeOff}
                            onChangeText={takeOff => setTakeOff(takeOff)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {landingToggle ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Landing </Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.itemLanding ? listLanding : landing}
                            onChangeText={landing => setLanding(landing)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {TO_totalTimeToggle && aircraftId.toUpperCase() === 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Total time</Text>
                        <MaskedTextInput
                            mask='99:99'
                            //value={Listparams.itemLanding?listLanding:landing}
                            onChangeText={inputText => setTotalTo_time(inputText)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {PFToggle && aircraftId.toUpperCase() === 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>PF Hours</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={pfHours}
                            onChangeText={pfHours => setPfHours(pfHours)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {PMToggle && aircraftId.toUpperCase() === 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>PM Hours</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={pmHours}
                            onChangeText={pmHours => setPmHours(pmHours)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {SFToggle && aircraftId.toUpperCase() === 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>SFI/SFE</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={sf}
                            onChangeText={sf => setSf(sf)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {/* 
                <View style={{flexDirection:'row'}}>

                <View style={Logbook.fieldWithoutBottom1}>
                <View style={Logbook.fields}>
                <Text style={{...Logbook.fieldText, ...{lineHeight:35,}}}>Chocks On*</Text>
                <MaskedTextInput
                    mask= '99:99'
                    value={chocksOn}
                    onChangeText={chocksOn => setChocksOn(chocksOn)}
                    keyboardType="numeric"
                    placeholder="hh:mm"
                />
                </View>
                </View>

                <View style={Logbook.fieldWithoutBottom2}>
                <View style={Logbook.fields}>
                <MaterialCommunityIcons  
                name="clock-time-nine" color={'#256173'} size={20} style={Logbook.clock}/>
                </View>
                </View>

                </View> */}

                {aircraftId.toUpperCase() !== 'SIMU' && (<View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Chocks On<Text style={{ color: 'red' }}>*</Text></Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.RoasterChocksOn ? rosterChocksOn : chocksOn}
                            onChangeText={chocksOn => setChocksOn(chocksOn)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View>)}

                {/* end of chocks-on/chocks-off */}

                {routeToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Route</Text>
                        <TextInput
                            placeholder='Route'
                            placeholderTextColor='#393F45'
                            value={Listparams.itemRoute ? listRoute : route}
                            onChangeText={(route) => setRoute(route)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View> : null}

                {config && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, color: 'blue' } }}>Select Choices....</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            {/* <Text style={{...Logbook.fieldText, ...{lineHeight:35}}}>Select Choices....</Text> */}
                            <TouchableOpacity onPress={() => navigation.navigate('Configuration', { from: 'flight1', toggled: params.flight })}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View> : null}

                {instructorToggle ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Instructor</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35 } }}>{Listparams.itemInstructor ? listInstructor : instructor}</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('People', { from: 'instructor' })}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View> : null}

                {Pic_toggle && aircraftId.toUpperCase() !== 'SIMU' ? <TouchableOpacity onPress={() => navigation.navigate('People', { from: 'pic' })}>
                    <View style={Logbook.fieldWithoutBottom}>
                        <View style={Logbook.fields}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>PIC/P1<Text style={{ color: 'red' }}>*</Text></Text>
                            <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                                <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35 } }}>{Listparams.RoasterNamePic ? logic(rosterNamePic) : pic_p1}</Text>
                                <TouchableOpacity onPress={() => navigation.navigate('People', { from: 'pic' })}>
                                    <MaterialCommunityIcons
                                        name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity> : null}

                {rc1Toggle ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Relief Crew 1</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35 } }}>{Listparams.itemRc1 ? listRc1 : reliefCrew1}</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('People', { from: 'rc1' })}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View> : null}

                {rc2Toggle ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Relief Crew 2</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35 } }}>{Listparams.itemRc2 ? listRc2 : reliefCrew2}</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('People', { from: 'rc2' })}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View> : null}

                {rc3Toggle ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Relief Crew 3</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35 } }}>{Listparams.itemRc3 ? listRc3 : reliefCrew3}</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('People', { from: 'rc3' })}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View> : null}

                {rc4Toggle ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Relief Crew 4</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35 } }}>{Listparams.itemRc4 ? listRc4 : reliefCrew4}</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('People', { from: 'rc4' })}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View> : null}

                {sic_toggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>SIC/P2</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35 } }}>{Listparams.itemP2 ? listP2 : sic_p2} </Text>
                            <TouchableOpacity onPress={() => navigation.navigate('People', { from: 'sic' })}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View> : null}

                {studentToggle ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Student</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35 } }}>{Listparams.itemStudent ? listStudent : student}</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('People', { from: 'student' })}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View> : null}

                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={FlightmodalVisible}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        setFlightModalVisible(!FlightmodalVisible);
                    }}
                >
                    <View style={ModalView.FlightcenteredView}>
                        <View style={ModalView.flightModalView}>
                            <Text style={styles.modalText}>Set Title</Text>
                            <Text style={ModalView.modalText}>Please Set Name for custom field</Text>
                            <TextInput
                                placeholder='Enter Custom Field Name'
                                placeholderTextColor='#393F45'
                                style={{ borderColor: '#000', borderWidth: 0.2, padding: 2 }} />

                            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                <Pressable
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={() => { }}
                                >
                                    <Text style={styles.textStyle}>ok</Text>
                                </Pressable>
                                <Pressable
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={() => setFlightModalVisible(!FlightmodalVisible)}
                                >
                                    <Text style={styles.textStyle}>Cancel</Text>
                                </Pressable>
                            </View>
                        </View>
                    </View>
                </Modal>


                {config ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, color: 'blue' } }}>Select Choices....</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            {/* <Text style={{...Logbook.fieldText, ...{lineHeight:35}}}>Select Choices....</Text> */}
                            <TouchableOpacity onPress={() => navigation.navigate('Configuration', { from: aircraftId.toUpperCase() === 'SIMU' ? 'simu' : 'flight2' })}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View> :
                    <View style={Logbook.fieldWithoutBottom}>
                        <View style={Logbook.fields}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Custom</Text>
                            <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => setFlightModalVisible(true)}>
                                    <MaterialCommunityIcons
                                        name="plus-circle" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>}

                {/* Time */}

                {aircraftId.toUpperCase() !== 'SIMU' && (<View style={Logbook.headline}>
                    <Text style={Logbook.HeadlineText}>Time</Text>
                </View>)}

                {totalTimeToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Total Time</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.RoasterTotalTime ? rosterTotalTime : filghtTimeM}
                            onChangeText={totalTime => setTotalTime(totalTime)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {dayToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Day</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.itemDay ? listDay : dayTime}
                            onChangeText={day => setDay(day)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {nightToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Night</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.itemNight ? listNight : nightTime}
                            onChangeText={night => setNight(night)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {AiToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Actual Instrument</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.itemAi ? listAi : ai}
                            onChangeText={ai => setAi(ai)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {dualDayToggle === true && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Dual (Day)</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.itemDualDay ? listDualDay : dual_day}
                            onChangeText={dual_day => setDual_day(dual_day)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {dualNightToggle === true && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Dual (Night)</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.itemDualNight ? listDualNight : dual_night}
                            onChangeText={dual_night => setDual_night(dual_night)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {instructionalToggle === true ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Instructional</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.itemInstructional ? listInstructional : instructional}
                            onChangeText={instructional => setInstructional(instructional)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {/* IFR/VFR */}
                {ifr_vfrToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fieldsRadio}>

                        <View style={{ flexDirection: 'row', }}>
                            <RadioButton.Group value={Listparams.itemFr ? listFr : fr}
                                onValueChange={fr => setFr(fr)}>
                                <RadioButton
                                    value="ifr"
                                    status={fr === 'ifr' ? 'checked' : 'unchecked'}
                                    onPress={() => setFr('ifr')}
                                    color='#256173'
                                    uncheckedColor='#256173'
                                    labelStyle={{ marginRight: 20 }}
                                />
                            </RadioButton.Group>
                            <Text style={Logbook.fieldTextRadio}>IFR</Text>
                        </View>
                        <View style={{ flexDirection: 'row', }}>
                            <RadioButton.Group value={Listparams.itemFr ? listFr : fr}
                                onValueChange={fr => setFr(fr)}>
                                <RadioButton
                                    value="vfr"
                                    //status={ fr === 'vfr' ? 'checked' : 'unchecked' }
                                    //onPress={() => setFr('vfr')}
                                    color='#256173'
                                    uncheckedColor='#256173'
                                    labelStyle={{ marginRight: 20 }}
                                />
                            </RadioButton.Group>
                            <Text style={Logbook.fieldTextRadio}>VFR</Text>
                        </View>

                    </View>
                </View> : null}
                {/* IFR/VFR */}

                {p1_us_dayToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> P1 U/S (Day)</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.itemP1UsDay ? listP1UsDay : p1_us_day}
                            onChangeText={p1_us_day => setP1_us_day(p1_us_day)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {p1_us_nightToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> P1 U/S (Night)</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.itemP1UsNight ? listP1UsNight : p1_us_night}
                            onChangeText={p1_us_night => setP1_us_night(p1_us_night)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {p1_ut_dayToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> P1 U/T (Day)</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.itemP1UtDay ? listP1UtDay : p1_ut_day}
                            onChangeText={p1_ut_day => setP1_ut_day(p1_ut_day)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {p1_ut_nightToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> P1 U/T (Night)</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.itemP1UtNight ? listP1UtNight : p1_ut_night}
                            onChangeText={p1_ut_night => setP1_ut_night(p1_ut_night)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {Pic_dayToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> PIC (Day)</Text>
                        <MaskedTextInput
                            mask='99:99'
                            // value={Listparams.itemPicDay?listPicDay:pic_day}
                            value={Listparams.PICItemName === 'Self' ? dayTime : pic_day}
                            onChangeText={pic_day => setPic_day(pic_day)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {Pic_nightToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> PIC (Night)</Text>
                        <MaskedTextInput
                            mask='99:99'
                            //value={Listparams.itemPicNight?listPicNight:pic_night}
                            value={Listparams.PICItemName === 'Self' ? nightTime : pic_night}
                            onChangeText={pic_night => setPic_night(pic_night)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {stlToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={Logbook.fieldText}>STL</Text>
                        <CheckBox
                            value={Listparams.itemStl ? listStl : stl}
                            onValueChange={stl => setStl(stl)}
                            tintColor='#256173'
                            boxType='square'
                            onTintColor='#256173'
                            onCheckColor='#fff'
                            onFillColor='#256173'
                            tintColors={{ true: '#256173', false: '#256173' }}
                        />
                    </View>
                </View> : null}

                {siToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Simulated Instrument</Text>
                        <MaskedTextInput
                            mask='99:99'
                            value={Listparams.itemSi ? listSi : si}
                            onChangeText={si => setSi(si)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {sic_dayToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> SIC (Day)</Text>
                        <MaskedTextInput
                            mask='99:99'
                            //value={Listparams.itemSicDay?listSicDay:sic_day}
                            value={Listparams.SICItemName === 'Self' ? dayTime : sic_day}
                            onChangeText={sic_day => setSic_day(sic_day)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {sic_nightToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> SIC (Night)</Text>
                        <MaskedTextInput
                            mask='99:99'
                            //value={Listparams.itemSicNight?listSicNight:sic_night}
                            value={Listparams.SICItemName === 'Self' ? nightTime : sic_night}
                            onChangeText={sic_night => setSic_night(sic_night)}
                            keyboardType="numeric"
                            placeholder="hh:mm"
                        />
                    </View>
                </View> : null}

                {xc_dayToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>XC (Day)/Legs</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <MaskedTextInput
                                mask='99:99'
                                value={Listparams.itemXcDay ? listXcDay : xc_day}
                                onChangeText={xc_day => setXc_day(xc_day)}
                                keyboardType="numeric"
                                placeholder="hh:mm"
                            />
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> /</Text>
                            <TextInput
                                placeholder='Number'
                                placeholderTextColor='grey'
                                value={Listparams.itemDayLegs ? listXcDayLegs : xc_day_leg}
                                onChangeText={(xc_day_leg) => setXc_dayLeg(xc_day_leg)}
                                style={{ marginTop: -1 }} />
                        </View>
                    </View>
                </View> : null}

                {xc_nightToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>XC (Night)/Legs</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <MaskedTextInput
                                mask='99:99'
                                value={Listparams.itemXcNight ? listXcNight : xc_night}
                                onChangeText={xc_night => setXc_night(xc_night)}
                                keyboardType="numeric"
                                placeholder="hh:mm"
                            />
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> /</Text>
                            <TextInput
                                placeholder='Number'
                                placeholderTextColor='grey'
                                value={Listparams.itemNightLegs ? listXcNightLegs : xc_night_leg}
                                onChangeText={(xc_night_leg) => setXc_nightLeg(xc_night_leg)}
                                style={{ marginTop: -1 }} />
                        </View>
                    </View>
                </View> : null}

                {/* Modal for Time */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={TimemodalVisible}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        setTimeModalVisible(!TimemodalVisible);
                    }}
                >
                    <View style={ModalView.FlightcenteredView}>
                        <View style={ModalView.flightModalView}>
                            <Text style={styles.modalText}>Set Title</Text>
                            <Text style={ModalView.modalText}>Please Set Name for custom field</Text>
                            <TextInput
                                placeholder='Enter Custom Field Name'
                                placeholderTextColor='#393F45'
                                style={{ borderColor: '#000', borderWidth: 0.2, padding: 2 }} />

                            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                <Pressable
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={() => { }}
                                >
                                    <Text style={styles.textStyle}>ok</Text>
                                </Pressable>
                                <Pressable
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={() => setTimeModalVisible(!TimemodalVisible)}
                                >
                                    <Text style={styles.textStyle}>Cancel</Text>
                                </Pressable>
                            </View>
                        </View>
                    </View>
                </Modal>

                {config ?
                    <View style={Logbook.fieldWithoutBottom}>
                        {aircraftId.toUpperCase() !== 'SIMU' && (<View style={Logbook.fields}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, color: 'blue' } }}>Select Choices....</Text>
                            <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                                {/* <Text style={{...Logbook.fieldText, ...{lineHeight:35}}}>Select Choices....</Text> */}
                                <TouchableOpacity onPress={() => navigation.navigate('Configuration', { from: 'Time' })}>
                                    <MaterialCommunityIcons
                                        name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                                </TouchableOpacity>
                            </View>
                        </View>)}
                    </View> :
                    <View style={Logbook.fieldWithoutBottom}>
                        {aircraftId.toUpperCase() !== 'SIMU' && (<View style={Logbook.fields}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Custom</Text>
                            <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => setFlightModalVisible(true)}>
                                    <MaterialCommunityIcons
                                        name="plus-circle" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                                </TouchableOpacity>
                            </View>
                        </View>)}
                    </View>}

                {/* landing */}
                {aircraftId.toUpperCase() !== 'SIMU' && (<View style={Logbook.headline}>
                    <Text style={Logbook.HeadlineText}>Landing</Text>
                </View>)}

                {autoLandingToggle ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> Auto Landing</Text>
                        <TextInput
                            placeholder='Please Enter Number'
                            placeholderTextColor='grey'
                            value={Listparams.itemAutoLanding ? listAutoLanding : autoLanding}
                            onChangeText={(autoLanding) => setAutoLanding(autoLanding)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View> : null}

                {dayLandingToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> Day Landing</Text>
                        <TextInput
                            placeholder='Please Enter Number'
                            placeholderTextColor='grey'
                            value={Listparams.RoasterDayLanding ? rosterDayLand : dayLanding}
                            onChangeText={(dayLanding) => setDayLanding(dayLanding)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View> : null}

                {day_toToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> Day T/O</Text>
                        <TextInput
                            placeholder='Please Enter Number'
                            placeholderTextColor='grey'
                            value={Listparams.RoasterDayTakeOff ? rosterDayTakeOff : day_to}
                            onChangeText={(day_to) => setDay_to(day_to)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View> : null}


                {fullStopToggle ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> Full Stop</Text>
                        <TextInput
                            placeholder='Please Enter Number'
                            placeholderTextColor='grey'
                            value={Listparams.itemFullStop ? listFullStop : fullStop}
                            onChangeText={(fullStop) => setFullStop(fullStop)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View> : null}

                {nightLandingToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> Night Landing</Text>
                        <TextInput
                            placeholder='Please Enter Number'
                            placeholderTextColor='grey'
                            value={Listparams.RoasterNightLanding ? rosterNightLand : nightLanding}
                            onChangeText={(nightLanding) => setNightLanding(nightLanding)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View> : null}

                {night_toToggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> Night T/O</Text>
                        <TextInput
                            placeholder='Please Enter Number'
                            placeholderTextColor='grey'
                            value={Listparams.RoasterNightTakeOff ? rosterNightTakeOff : night_to}
                            onChangeText={(night_to) => setNight_to(night_to)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View> : null}

                {touchGoToggle ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> Touch & Go's</Text>
                        <TextInput
                            placeholder='Please Enter Number'
                            placeholderTextColor='grey'
                            value={Listparams.itemTouchGo ? listTouchGo : touchGo}
                            onChangeText={(touchGo) => setTouchGo(touchGo)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View> : null}

                {waterLandingToggle ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> Water Landing</Text>
                        <TextInput
                            placeholder='Please Enter Number'
                            placeholderTextColor='grey'
                            value={Listparams.itemWaterLanding ? listWaterLanding : waterLanding}
                            onChangeText={(waterLanding) => setWaterLanding(waterLanding)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View> : null}

                {water_toToggle ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}> Water T/O</Text>
                        <TextInput
                            placeholder='Please Enter Number'
                            placeholderTextColor='grey'
                            value={Listparams.itemWaterTO ? listWaterTO : water_to}
                            onChangeText={(water_to) => setWater_to(water_to)}
                            style={{ marginTop: -5 }} />
                    </View>
                </View> : null}

                {/* Modal for landing */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={LandingmodalVisible}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        setLandingModalVisible(!LandingmodalVisible);
                    }}
                >
                    <View style={ModalView.FlightcenteredView}>
                        <View style={ModalView.flightModalView}>
                            <Text style={styles.modalText}>Set Title</Text>
                            <Text style={ModalView.modalText}>Please Set Name for custom field</Text>
                            <TextInput
                                placeholder='Enter Custom Field Name'
                                placeholderTextColor='#393F45'
                                style={{ borderColor: '#000', borderWidth: 0.2, padding: 2 }} />

                            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                <Pressable
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={() => { }}
                                >
                                    <Text style={styles.textStyle}>ok</Text>
                                </Pressable>
                                <Pressable
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={() => setLandingModalVisible(!LandingmodalVisible)}
                                >
                                    <Text style={styles.textStyle}>Cancel</Text>
                                </Pressable>
                            </View>
                        </View>
                    </View>
                </Modal>

                {config ? <View style={Logbook.fieldWithoutBottom}>
                    {aircraftId.toUpperCase() !== 'SIMU' && (<View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, color: 'blue' } }}>Select Choices....</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            {/* <Text style={{...Logbook.fieldText, ...{lineHeight:35}}}>Select Choices....</Text> */}
                            <TouchableOpacity onPress={() => navigation.navigate('Configuration', { from: 'landing' })}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>)}
                </View> :
                    <View style={Logbook.fieldWithoutBottom}>
                        {aircraftId.toUpperCase() !== 'SIMU' && (<View style={Logbook.fields}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Custom</Text>
                            <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => setLandingModalVisible(true)}>
                                    <MaterialCommunityIcons
                                        name="plus-circle" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                                </TouchableOpacity>
                            </View>
                        </View>)}
                    </View>}

                {/* Approaches */}
                {aircraftId.toUpperCase() !== 'SIMU' && (<View style={Logbook.headline}>
                    <Text style={Logbook.HeadlineText}>Approaches</Text>
                </View>)}

                {aircraftId.toUpperCase() !== 'SIMU' && (<View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Approach 1</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <Text style={{ paddingTop: 10 }}>{Listparams.itemApproach1 ? listApproach1 : approach1}</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('Approach')}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>)}

                {Approach2Toggle && aircraftId.toUpperCase() !== 'SIMU' ? <View style={Logbook.fieldWithoutBottom}>
                    <View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Approach 2</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <Text style={{ paddingTop: 10 }}>{Listparams.itemApproach2 ? listApproach2 : approach2}</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('Approach')}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View> : null}

                {config ? <View style={Logbook.fieldWithoutBottom}>
                    {aircraftId.toUpperCase() !== 'SIMU' && (<View style={Logbook.fields}>
                        <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, color: 'blue' } }}>Select Choices....</Text>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            {/* <Text style={{...Logbook.fieldText, ...{lineHeight:35}}}>Select Choices....</Text> */}
                            <TouchableOpacity onPress={() => navigation.navigate('Configuration', { from: 'Approach' })}>
                                <MaterialCommunityIcons
                                    name="chevron-right" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                            </TouchableOpacity>
                        </View>
                    </View>)}
                </View> :
                    <View style={Logbook.fieldWithoutBottom}>
                        {aircraftId.toUpperCase() !== 'SIMU' && (<View style={Logbook.fields}>
                            <Text style={{ ...Logbook.fieldText, ...{ lineHeight: 35, } }}>Custom</Text>
                            <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                                <TouchableOpacity>
                                    <MaterialCommunityIcons
                                        name="plus-circle" color={'#256173'} size={25} style={{ lineHeight: 35 }} />
                                </TouchableOpacity>
                            </View>
                        </View>)}
                    </View>}

                {/*Remark*/}
                <View style={Logbook.headline}>
                    <Text style={Logbook.HeadlineText}>Remark</Text>
                </View>

                <View style={{ padding: 20, }}>
                    <View style={Logbook.remarksBox}>
                        <TextInput
                            placeholder=' Your Remarks'
                            placeholderTextColor='#393F45'
                            value={Listparams.itemRemarks ? listRemark : remark}
                            onChangeText={(remark) => setRemark(remark)}
                            style={Platform.OS === 'android' ? { marginTop: -20 } : { marginTop: -10 }} />
                    </View>
                </View>

            </ScrollView>


            <View style={Logbook.buttonView}>
                <View style={{ flexDirection: 'row' }}>
                    {/* {Listparams.childParamList?<TouchableOpacity onPress={updateLogbbok}>
                <View style={Logbook.button}>
                <Text style={Logbook.buttonText}>Update</Text>
                </View>
            </TouchableOpacity>: */}
                    <TouchableOpacity onPress={Add_Logbook}>
                        <View style={Logbook.button}>
                            <Text style={Logbook.buttonText}>Save</Text>
                        </View>
                    </TouchableOpacity>
                    {/* } */}
                    <TouchableOpacity onPress={() => setModalVisible(true)}>
                        <MaterialCommunityIcons
                            name="menu" color={'#000'} size={30} style={{ marginLeft: 10 }} />
                    </TouchableOpacity>
                </View>
            </View>

            <View style={styles.centeredView}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    presentationStyle='overFullScreen'
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        setModalVisible(!modalVisible);
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <View style={ModalView.Modal}>
                                <Text style={ModalView.ModalHeading}>Select option</Text>
                            </View>
                            <TouchableOpacity style={ModalView.Modal} onPress={Configuration}>
                                <View>
                                    {config ? <Text style={ModalView.ModalListingText}> Done Config</Text> : <Text style={ModalView.ModalListingText}>Config</Text>}
                                </View>
                            </TouchableOpacity>
                            {Listparams.childParamList ?
                                <TouchableOpacity style={ModalView.Modal} onPress={deleteLogbbok}>
                                    <View>
                                        <Text style={ModalView.ModalListingText}>Delete</Text>
                                    </View>
                                </TouchableOpacity> :
                                <TouchableOpacity style={ModalView.Modal} onPress={resetHandler}>
                                    <View>
                                        <Text style={ModalView.ModalListingText}>Add</Text>
                                    </View>
                                </TouchableOpacity>}
                            <TouchableOpacity onPress={resetHandler}>
                                <View style={ModalView.Modal}>
                                    <Text style={ModalView.ModalListingText}>New Flight</Text>
                                </View>
                            </TouchableOpacity>
                            <View style={ModalView.Modal}>
                                <Text style={ModalView.ModalListingText}>Copy Trip</Text>
                            </View>
                            <View style={ModalView.Modal}>
                                <Text style={ModalView.ModalListingText}>Return Trip</Text>
                            </View>
                            <View style={ModalView.Modal}>
                                <Text style={ModalView.ModalListingText}>Next Leg</Text>
                            </View>
                            <View style={ModalView.Modal}>
                                <Text style={ModalView.ModalListingText}>Roster Import</Text>
                            </View>
                            <View style={ModalView.Modal}>
                                <Text style={ModalView.ModalListingText}>Import Pilot List</Text>
                            </View>
                            {/* <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={styles.textStyle}>Hide Modal</Text>
            </Pressable> */}
                        </View>
                    </View>

                    <View style={styles.centeredViewCancel}>
                        <TouchableOpacity style={styles.modalViewCancel} onPress={() => setModalVisible(!modalVisible)}>
                            <View >
                                <View style={{ padding: 15 }}><Text>Cancel</Text></View>
                            </View>
                        </TouchableOpacity>
                    </View>

                </Modal>
            </View>


        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "flex-start",
        // marginTop: '5%',

    },
    centeredViewCancel: {
        //flex: 1,
        justifyContent: "center",
        alignItems: "flex-start",
        paddingBottom: '50%',
        //marginBottom: 10,
        //padding:10,
    },
    modalView: {
        marginLeft: '5%',
        backgroundColor: "white",
        borderRadius: 10,
        //padding: 35,
        alignItems: "flex-start",
        shadowColor: "#000",
        //   shadowOffset: {
        //     width: 0,
        //     height: 2
        //   },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: '90%',
        position: 'absolute',
        bottom: '1%'
    },
    modalViewCancel: {
        marginLeft: '5%',
        backgroundColor: "white",
        borderRadius: 10,
        //padding: 35,
        alignItems: "flex-start",
        shadowColor: "#000",
        //   shadowOffset: {
        //     width: 0,
        //     height: 2
        //   },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: '90%',
        position: 'absolute',
        top: '10%'
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },

});

//make this component available to the app
export default CreateLogbook;
