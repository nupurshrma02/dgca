//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput, FlatList } from 'react-native';
import { LogbookListing } from '../../styles/styles';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {LogBookList} from '../../components/dummyLogBookListing';
import { ScrollView } from 'react-native-gesture-handler';
import { Colors } from '../../components/colors';
import AsyncStorage from '@react-native-community/async-storage';
import { ParamsContext } from '../../params-context';

import {BaseUrl} from '../../components/url.json';
import { useDispatch, useSelector } from 'react-redux';
import { rosterImportdata } from '../../store/actions/rosterImportsAction';
import { Divider } from 'react-native-paper';

// create a component
const LogBookListing = ({navigation}) => {

  const [roasterData, setRoasterData] = React.useState('')

    const dataDispatcher = useDispatch()
    //dataDispatcher(rosterImportdata({data: ['abhishek', 'Nupur', 'Nisha']}))

    const getReduxData = useSelector(state => state.rosterImport.data);
    console.log('from logbooklisting details', getReduxData);

    // var data = getReduxData.data.map(function(item) {
    //   return {
    //     id : item.id,
    //     Dept_date: item.Dept_date,
    //     Dept_place: item.Dept_place,
    //     Dept_time: item.Dept_time,
    //     Arrival_place : item.Arrival_place,
    //     Arrival_time : item.Arrival_time,
    //     Aircraft_Reg: item.Aircraft_Reg
    //   };
    // });
    
    //console.log('required data', getReduxData.data)

    //setRoasterData(data);

    const[selectedId, setSelectedId] = React.useState('')
    const [focused,setFocused] = React.useState(false);

    const onFocusChange = () => setFocused(true);
    const onFocusCancelled = () => setFocused(false);

    const[date, setDate] = React.useState('')
    const[logbookData, setlogbookData] = React.useState([]);

    const [, setParamsLogbook ] = React.useContext(ParamsContext);

    React.useEffect(() => {Display_logbook()}, []);
    const Display_logbook = async() => {
        let user = await AsyncStorage.getItem('userdetails');
        user = JSON.parse(user);
      
        await fetch(BaseUrl+'listlogbook',{
          method : 'POST',
          headers:{
              'Accept': 'application/json',
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            "user_id": user.id,
       })
      }).then(res => res.json())
      .then(resData => {
        //setBuildLogBook(resData.message);
        //console.log('data1---->', resData.data);
        for (var j = 0; j < resData.data.length; j++){
            console.log(resData.data[j].date);
            setDate(resData.data[j].date)
            console.log('date-->', date)
        }
        setlogbookData(resData.data)
        //setData(resData.message);
        //console.log('data--------->', data);
        //dataDispatcher(rosterImportdata({data: resData.data}))
      });
    };

    

    console.log('roasted data', getReduxData.data)
    //console.log('data', logbookData)

    const Item = ({ item, onPress, backgroundColor, textColor }) => (
        <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>
        <ScrollView style={LogbookListing.listing}>
            {/* <View>
            <Text>{item.date}</Text>
            <Text style={{fontWeight:'bold'}}>{item.from_city}-{item.to_city}  {item.offTime}-{item.onTime} ({item.totalTime})</Text>
            </View> */}
            <View style = {{width:'100%'}}>
            <Text>{item.Dept_date}</Text>
            <Divider />
            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <Text style={{fontWeight:'bold', paddingTop:5}}>{item.Dept_place}</Text>
            <MaterialCommunityIcons  name="ray-start-arrow" color={'#000'} size={30} style = {{paddingHorizontal:10}} />
            <Text style={{fontWeight:'bold' , paddingTop:5}}>{item.Arrival_place}</Text>
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <Text>({item.Dept_time})</Text>
            <Text style={{paddingLeft: 28}}>({item.Arrival_time})</Text>
            </View>
            </View>
        </ScrollView>
        </TouchableOpacity>
      );
    
      const renderItem = ({item}) => {
        //console.log('typeeeee---->',item.Airline_code);
        const backgroundColor = item.id === selectedId ? "#6e3b6e" : "#f9c2ff";
        const color = item.id === selectedId ? 'white' : 'black';
        // const fetchToBuildLogBook = item.id === selectedId ? navigation.navigate('BuildLogbook',{
        //   itemId: item.id,
        //   itemName: item.aircraft_name,
        // }) : '';
    
        const selectParams = () =>{ 
        if(item.id === selectedId)
        {
            setParamsLogbook(previousParams => ({
            ...(previousParams || {}),
            childParamList: 'Listvalue',
            itemId: item.id,
            itemDate : item.date,
            itemFlight : item.flight_no,
            itemAircraftType : item.aircraftType,
            itemAircraftId : item.aircraftReg,
            itemFrom : item.from_city,
            itemTo : item.to_city,
            itemChocksOff : item.offTime ,
            itemChocksOn : item.onTime,
            itemTakeOff : item.outTime,
            itemLanding : item.inTime,
            itemRoute : item.route,
            itemInstructor : item.instructor,
            itemP1 : item.p1,
            itemRc1 : item.reliefCrew1,
            itemRc2 : item.reliefCrew2,
            itemRc3 : item.reliefCrew3,
            itemRc4 : item.reliefCrew4,
            itemP2 : item.p2,
            itemStudent : item.student,
            itemTotalTime : item.totalTime,
            itemDay : item.day,
            itemNight : item.night,
            itemAi : item.actual_Instrument,
            itemDualDay : item.dual_day,
            itemDualNight : item.dual_night,
            itemInstructional : item.instructional,
            itemFr : item.ifr_vfr,
            itemP1UsDay : item.p1_us_day,
            itemP1UsNight : item.p1_us_night,
            itemP1UtDay : item.p1_ut_day,
            itemP1UtNight : item.p1_ut_night,
            itemPicDay : item.pic_day,
            itemPicNight : item.pic_night,
            itemStl : item.stl,
            itemSi : item.sim_instrument,
            itemSicDay : item.sic_day,
            itemSicNight : item.sic_night,
            itemXcDay : item.x_country_day,
            itemXcNight : item.x_country_night,
            itemDayLegs : item.x_country_day_leg,
            itemNightLegs : item.x_country_night_leg,
            itemAutoLanding : item.autolanding,
            itemDayLanding : item.dayLanding,
            itemDayTO : item.dayTO,
            itemFullStop: item.fullStop,
            itemNightLanding : item.nightLanding,
            itemNightTO : item.nightTO,
            itemTouchGo : item.touch_n_gos,
            itemWaterLanding : item.waterLanding,
            itemWaterTO : item.waterTO,
            itemApproach1 : item.approach1,
            itemApproach2 : item.approach2,
            itemRemarks : item.remark,
            //RoasterImportData : item.Dept_date+item.Dept_place+item.Arrival_place+item.Dept_time+item.Arrival_time
            RoasterDate : item.Dept_date,
            RoasterFrom : item.Dept_place, 
            RoasterChocksOff : item.Dept_time,
            RoasterTo : item.Arrival_place,
            RoasterChocksOn : item.Arrival_time,
            RoasterAType : item.Aircraft_type,
            RoasterAId : item.Aircraft_Reg,
            RoasterTotalTime : item.total_time_flight,
            RoasterNamePic : item.Name_PIC,
            RoasterDayLanding : item.Lands_Day,
            RoasterNightLanding : item.Lands_Night,
            RoasterDayTakeOff : item.TkkOff_Day,
            RoasterNightTakeOff : item.TkkOff_Night,
            }));
          navigation.navigate('CreateLogbook');
        }
      }
    
        return (
          <Item
            item={item}
            onPress={() => {setSelectedId(item.id); selectParams()}}
            backgroundColor={{ backgroundColor }}
            textColor={{ color }}
          />
        );
        
    }

   return (
        <View style={styles.container}>
            <TouchableOpacity onPress={()=> navigation.navigate('CreateLogbook')}>
            <View style={LogbookListing.arrow}>
            <MaterialCommunityIcons  
                name="arrow-right" color={'#000'} size={30} />
            </View>
            </TouchableOpacity>

            <View style={{backgroundColor:'#c0c0c0', padding:10, flexDirection:'row'}}>
               <View style={(focused) ? LogbookListing.searchbar2 : LogbookListing.searchbar}>
                 <MaterialCommunityIcons name="magnify" color={'#c0c0c0'} size={25} style={{padding:2}} />
                 <TextInput 
                 onFocus={onFocusChange}
                 placeholder='Search' 
                 placeholderTextColor = "#393F45"
                 style={{marginTop: -10, fontSize:15, width:100, lineHeight:25}}
                 />
               </View>
               {focused ? <Text style={LogbookListing.cancelButton} onPress={onFocusCancelled}>Cancel</Text> : null}
            </View>

            {/* <View style={LogbookListing.listing}>
                <Text style={{fontWeight:'bold'}}> 02-01-2021 VAAH-VAPO 6:16-7:26 (1:10)</Text>
                <Text style={{marginTop: 5,}}> HFFG/VTIZK SELF/</Text>
                <Text style={{marginTop: 5,}}> D-01:10 N-0:00 ACT-00:00 SIM-00:00 P1</Text>
            </View> */}

            {/* <FlatList
            style={{width:'100%'}}
            data={logbookData}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            numColumns={1}
            extraData={selectedId}
            /> */}

            
            <Text>From Roaster Import</Text>

            <FlatList
            style={{width:'100%'}}
            data={getReduxData.data}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            numColumns={1}
            extraData={selectedId}
            />


            <View style={LogbookListing.bottomView}>
                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                <Text> TOTAL ON TYPE-</Text>
                <View style={{backgroundColor: '#256173',paddingHorizontal:8, paddingVertical:2}}>
                    <Text style={{color:'#fff'}}>0:00</Text>
                </View>
                </View>

                <View style={{flexDirection:'row', justifyContent:'space-between', paddingTop:10,}}>
                <Text> TOTAL FLYING HOURS-</Text>
                <View style={{backgroundColor: '#256173',paddingHorizontal:8, paddingVertical:2}}>
                    <Text style={{color:'#fff'}}>0:00</Text>
                </View>
                </View>

                <View style={{flexDirection:'row', justifyContent:'space-between', paddingTop:10,}}>
                <Text> LAST BACKUP-</Text>
                <Text> 2121-08-02 18:42:22</Text>
                </View>
            </View>

        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default LogBookListing;
