//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, FlatList, Dimensions, TouchableOpacity, ActivityIndicator } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { ThemeContext } from '../../theme-context';
import {Places} from '../../components/dummyLogBookListing';
import { ParamsContext } from '../../params-context';
import AsyncStorage from '@react-native-community/async-storage';
import {Filter} from '../../styles/styles'

import {BaseUrl} from '../../components/url.json';

import SQLite from 'react-native-sqlite-storage';

const prePopulateddb = SQLite.openDatabase(
  {
    name: 'autoflightlogdb.db',
    createFromLocation: 1,
    //location: 'www/autoflightlogdb.db',
  },
  () => {
    //alert('successfully executed');
  },
  error => {
    alert('db error');
  },
);
 
// create a component
const Destination = ({navigation,route}) => {
 const [focused,setFocused] = React.useState(false);

 const onFocusChange = () => setFocused(true);
 const onFocusCancelled = () => setFocused(false);

 const { dark, theme, toggle } = React.useContext(ThemeContext);
 const [, setParamsFrom ] = React.useContext(ParamsContext);
 const [, setParamsTo ] = React.useContext(ParamsContext);
 const [, setParamsApproaches ] = React.useContext(ParamsContext);

 const [selectedId, setSelectedId] = React.useState('');

 const[data, setData] = React.useState([]) 
 const[filteredData, setFilteredData] = React.useState([])
 const[search, setSearch] = React.useState('')
 const[iata, setIata] = React.useState('')
 const [icao, setIcao] = React.useState('')
 //const [id,setId] = React.useState('')
 //const [ident, setIdent] = React.useState('');
 const [dataFetched , setDataFetched ] = React.useState(false)

const [loading, setLoading] = React.useState(false);
const [offset, setOffset] = React.useState(0);
const [isListEnd, setIsListEnd] = React.useState(false);

 React.useEffect(() => {getPrepopulatedDataQuery()}, []);

//  const AllPlaces = async() => {
//   let user = await AsyncStorage.getItem('userdetails');
//   user = JSON.parse(user);

//   await fetch(BaseUrl+'display_places',{
//     method : 'POST',
//     headers:{
//         'Accept': 'application/json',
//         'Content-Type': 'application/json'
//     },
//     body: JSON.stringify({
//       //"user_id": user.id,
//       //"date_format": date,             
//  })
// }).then(res => res.json())
// .then(resData => {
//   //console.log('mesg==>', resData.message)
//   //console.log('data---->', resData.data);
//   //setData(resData.data);
//   //setFilteredData(resData.data);
//   // console.log('dfjgdhgf--->', df)
//    for (var j = 0; j < resData.data.length; j++){
//          //console.log(resData.data[j].ident);
//          //setIdent(resData.data[j].ident)
//          //setId(resData.data[j].id)
//          //console.log('data---->', data)
//   //       setDf(resData.data[j].date_format);
//   //       console.log('df-->', df);
//   //       console.log ('name-->',resData.data[j].aircraft_type)
//        }
// });
// };

// const MinePlaces = async() => {
//   let user = await AsyncStorage.getItem('userdetails');
//   user = JSON.parse(user);

//   await fetch(BaseUrl+'display_places',{
//     method : 'POST',
//     headers:{
//         'Accept': 'application/json',
//         'Content-Type': 'application/json'
//     },
//     body: JSON.stringify({
//       "user_id": user.id,
//       //"date_format": date,             
//  })
// }).then(res => res.json())
// .then(resData => {
//   console.log('mesg==>', resData.message)
//   console.log('data---->', resData.data);
//   setData(resData.data);
//   // console.log('dfjgdhgf--->', df)
//    for (var j = 0; j < resData.data.length; j++){
//          console.log(resData.data[j].ident);
//          //setIdent(resData.data[j].ident)
//          //setId(resData.data[j].id)
//          //console.log('data---->', data)
//   //       setDf(resData.data[j].date_format);
//   //       console.log('df-->', df);
//   //       console.log ('name-->',resData.data[j].aircraft_type)
//        }
// });
// };

const searchQuery = (dataToSearch) => {
  let SearchedData = [];
  let SingleResult = '';
  setSearch(dataToSearch)
  console.log('Searching for ', dataToSearch);
  prePopulateddb.transaction(tx => {
    tx.executeSql('SELECT * FROM Airport_table WHERE ident  LIKE "%'+dataToSearch+'%" OR name  LIKE "%'+dataToSearch+'%" OR city1 LIKE "%'+dataToSearch+'%" OR city2 LIKE "%'+dataToSearch+'%" OR ICAO_code LIKE "%'+dataToSearch+'%" OR IATA_code LIKE "%'+dataToSearch+'%" limit 10', [], (tx, result1) => {
      if (result1.rows.length > 0) {
        //alert('data available ');
        console.log('Searched result raw: ', result1)
        for (let i = 0; i <= result1.rows.length; i++) {
          SingleResult = {
            id : result1.rows.item(i).airportID,
            ident: result1.rows.item(i).ident,
            Airport_name: result1.rows.item(i).name,
            city: result1.rows.item(i).city1,
            city2: result1.rows.item(i).city2,
            country: result1.rows.item(i).country,
            lat: result1.rows.item(i).latitude,
            long:result1.rows.item(i).longitude,
            elevation: result1.rows.item(i).elevation,
            timezone : result1.rows.item(i).timezone,
            icao_code : result1.rows.item(i).ICAO_code,
            iata_code : result1.rows.item(i).IATA_code,
          }
          SearchedData.push(SingleResult);
          console.log('single', SingleResult)
          console.log(' Searched data', SearchedData);
          setFilteredData(SearchedData);
        }
        //setFilteredData(SearchedData);
        console.log('Searched Result array: ', SearchedData)
      }else{
        setFilteredData([]);
        console.log('No Data found')
      }
    });
  });
}

const getPrepopulatedDataQuery = () => {
  let data = [];
  //console.log('testing');
  prePopulateddb.transaction(tx => {
    tx.executeSql('SELECT * from Airport_table LIMIT 20', [], (tx, result) => {
      // if (result.rows.length > 0) {
      //   setOffset(offset + 1);
      //   // After the response increasing the offset
      //   setLoading(false);
      // } else {
      //   setIsListEnd(true);
      //   setLoading(false);
      // }
      //alert('consoling');
      console.log(result);
      for (let i = 0 ; i <= result.rows.length ; i++) {
        data.push({
          id :  result.rows.item(i).airportID,
          ident: result.rows.item(i).ident,
          Airport_name: result.rows.item(i).name,
          // type: result.rows.item(i).type,
           city: result.rows.item(i).city1,
          // country: result.rows.item(i).country,
           lat: result.rows.item(i).latitude,
           long: result.rows.item(i).longitude,
          // altitude: result.rows.item(i).Altitutde,
          // timeZone: result.rows.item(i).timeZone,
          // Day_light_saving: result.rows.item(i).Day_light_saving,
           icao_code: result.rows.item(i).ICAO_code,
           iata_code: result.rows.item(i).IATA_code,
        });
        console.log('places data',data);
        setDataFetched(false)
      
      // setData(data);
      setFilteredData(data);
      }
      });
  });
};

const renderFooter = () => {
  return (
    // Footer View with Loader
    <View style={styles.footer}>
      {loading ? (
        <ActivityIndicator
          color="black"
          style={{margin: 15}} />
      ) : null}
    </View>
  );
};

// const IATO_ICAO = async() => {
//   let user = await AsyncStorage.getItem('userdetails');
//   user = JSON.parse(user);

//   await fetch(BaseUrl+'display_places',{
//     method : 'POST',
//     headers:{
//         'Accept': 'application/json',
//         'Content-Type': 'application/json'
//     },
//     body: JSON.stringify({
//       "user_id": user.id,
//       "nameIATA": iata,
//       "nameICAO": icao,
//       //"date_format": date,             
//  })
// }).then(res => res.json())
// .then(resData => {
//   console.log('mesg==>', resData.message)
//   console.log('data---->', resData.data);
//   setData(resData.data);
//   // console.log('dfjgdhgf--->', df)
//    for (var j = 0; j < resData.data.length; j++){
//          console.log(resData.data[j].ident);
//          setIata(resData.data[j].nameIATA)
//          setIcao(resData.data[j].nameICAO)
//          //setIdent(resData.data[j].ident)
//          //setId(resData.data[j].id)
//          //console.log('data---->', data)
//   //       setDf(resData.data[j].date_format);
//   //       console.log('df-->', df);
//   //       console.log ('name-->',resData.data[j].aircraft_type)
//        }
// });
// };

//sqlite
const getDataQuery = () => {
  let data = [];
  db.transaction(tx => {
    tx.executeSql('SELECT * from Airport_table LIMIT 20 ', [], (tx, result) => {
      if (result.rows.length > 0) {
        alert('data available ');
        console.log('result', result)
      }
      else{
        alert('helo')
      }
      for (let i = 0 ; i <= result.rows.length ; i++) {
        //console.log('name: ', result.rows.item(i).ident, 'loginlink: ', result.rows.item(i).loginUrl)
        data.push({
          id :  result.rows.item(i).airportID,
          ident: result.rows.item(i).ident,
          Airport_name: result.rows.item(i).name,
          // type: result.rows.item(i).type,
           city: result.rows.item(i).city1,
          // country: result.rows.item(i).country,
           lat: result.rows.item(i).latitude,
           long: result.rows.item(i).longitude,
          // altitude: result.rows.item(i).Altitutde,
          // timeZone: result.rows.item(i).timeZone,
          // Day_light_saving: result.rows.item(i).Day_light_saving,
          icao_code: result.rows.item(i).ICAO_code,
          iata_code: result.rows.item(i).IATA_code,
          
        });

      console.log(data);
      setDataFetched(false)
      
      setData(data);
      // data.map((person, index) => (
      //   console.log('Hello', person.ident)
      // ))
      setFilteredData(data);
      }
      //console.log(result);
      //console.log(result.rows.item(0).airline_name)
      // result.rows.item.map((index, content) => {
      //   data.push({name:content.airline_name, loginlink: content.loginUrl})
      // });
      // );
    });
    setDataFetched(true)
  });
  setDataFetched(true)
};


//sqlite

const searchFilter = (inputText) => {
  if(inputText) {
    const newData = data.filter((item) =>{
      const itemData = item.ident ? item.ident.toUpperCase()
      : "".toUpperCase();
      const textData = inputText.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    setFilteredData(newData);
    setSearch(inputText);
}
else {
  setFilteredData(data);
  setSearch(inputText);
}
}

 const Item = ({ item, onPress, backgroundColor, textColor }) => (
    <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>
    <View style = {{flexDirection:'row', justifyContent: 'space-between'}}>
    <View style= {{flexDirection:'column'}}>
      <Text style={[styles.ident, textColor]}>{item.ident}</Text>
      <Text style={[styles.city, textColor]}>{item.city}</Text>
    </View>
    <Text style={[styles.airportName, textColor]}>{item.Airport_name}</Text>
    </View>
    </TouchableOpacity>
  );

  const renderItem = ({item}) => {
    //console.log('typeeeee---->',item.name);
    const backgroundColor = item.id === selectedId ? "#E8E8E8" : "#fff";
    const color = 'black';
    // const fetchToBuildLogBook = item.id === selectedId ? navigation.navigate('BuildLogbook',{
    //   itemId: item.id,
    //   itemName: item.aircraft_name,
    // }) : '';

    const selectParams = () =>{ 
    if(item.id === selectedId && route.params.from === 'From')
    {
      setParamsFrom(previousParams => ({
        ...(previousParams || {}),
        childParam: 'value',
        FromItemCode : item.ident,
        fromItemlat : item.lat,
        fromItemLong : item.long,
        fromItemicao : item.icao_code
       
      }));
      navigation.goBack();
    }
    else if(item.id === selectedId && route.params.from === 'to')
    {
        setParamsTo(previousParams => ({
        ...(previousParams || {}),
        childParam1 : 'value1',
        ToItemCode : item.ident,
        ToItemlat : item.lat,
        ToItemLong : item.long,
        Itemicao : item.icao_code
      }));
      navigation.goBack(); 
    }
    else if(item.id === selectedId && route.params.from === 'Approaches')
    {
      setParamsApproaches(previousParams => ({
        ...(previousParams || {}),
        childParam2 : 'value2',
        ApproachItemCode : item.ident,
      }));
      navigation.goBack();
    }
  }

    return (
      <Item
        item={item}
        onPress={() => {setSelectedId(item.id); selectParams()}}
        backgroundColor={{ backgroundColor }}
        textColor={{ color }}
      />
    );
}

    return (
        <View style={[styles.container, {backgroundColor: theme.backgroundColor}]}>
            <View style={styles.header}>
            <MaterialCommunityIcons name="arrow-left" color={'#fff'} size={20} style={{padding:6}} onPress={()=>navigation.goBack()} />
            <Text style={styles.aircrafts}>Places</Text>
            </View>
            <View style={{backgroundColor:'#F3F3F3', padding:10, flexDirection:'row'}}>
               <View style={(focused) ? styles.searchbar2 : styles.searchbar}>
                 <MaterialCommunityIcons name="magnify" color={'#000'} size={25} style={{padding:6}} />
                 <TextInput 
                 onFocus={onFocusChange}
                 placeholder='Search'
                 value={search}
                 onChangeText={(inputText)=>searchQuery(inputText)} 
                 placeholderTextColor = "#D0D0D0"
                 style={{marginTop: -7, fontSize:15, width:100, lineHeight:25}}
                 />
               </View>
               {focused ? <Text style={styles.cancelButton} onPress={onFocusCancelled}>Cancel</Text> : null}
            </View>
            <View style={{backgroundColor:'#c0c0c0',}}>
              {focused? <View style={{ paddingHorizontal:10, justifyContent:'center', alignItems:'center',}}>
                <View style={Filter.filterBox}>
                  <TouchableOpacity onPress={{}}>
                  <Text>IATA/ICAO</Text>
                  </TouchableOpacity>
                  <View style={Filter.VerticalLine}></View>
                  <TouchableOpacity onPress={{}}>
                  <Text style={{paddingLeft:30}}>ALL</Text>
                  </TouchableOpacity >
                  <View style={Filter.VerticalLine}></View>
                  <TouchableOpacity onPress={{}}>
                  <Text style={{paddingLeft:30}}>MINE</Text>
                  </TouchableOpacity>
                </View>
              </View>
              :null}
            </View>
            <FlatList
            data={filteredData}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            numColumns={1}
            // ListFooterComponent={renderFooter}
            // onEndReached={getPrepopulatedDataQuery}
            // onEndReachedThreshold={2}
            extraData={selectedId}
            />
           <View style={styles.footer}>
            <TouchableOpacity onPress={()=> navigation.navigate('SetDestination')}>
                <View style={styles.button}>
                <Text style={styles.buttonText}>ADD</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=> navigation.goBack()}>
                <View style={styles.button}>
                <Text style={styles.buttonText}>Dismiss</Text>
                </View>
            </TouchableOpacity>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        //alignItems: 'center',
        //backgroundColor: '#2c3e50',
    },
    header:{
      padding: 5,
      flexDirection: 'row',
      backgroundColor: '#256173'
    },
    aircrafts: {
      fontSize: 15,
      color: '#fff',
      fontWeight: '700',
      fontFamily:'WorkSans-Regular',
      paddingTop: 5
    },
    searchbar: {
      //paddingLeft: 10,
      backgroundColor: '#fff',
      //padding: 10,
      width: '100%',
      //borderRadius: 10,
      flexDirection: 'row',
      //paddingVertical: 10,
    },
    searchbar2: {
        //paddingLeft: 10,
        backgroundColor: '#fff',
        //padding: 10,
        width: '80%',
        //borderRadius: 10,
        flexDirection: 'row',
        //paddingVertical: 10,
      },
      cancelButton: {
          fontSize: 15,
          marginLeft: 10,
          marginTop: 5,
          //paddingHorizontal:150,
      },
      item: {
        padding: 10,
        borderBottomWidth:1,
        borderBottomColor:'#E5E5E5',
      },
      ident: {
        fontFamily:'WorkSans-Regular',
        fontSize: 16
      },
      airportName: {
        fontFamily:'WorkSans-Regular',
        fontSize: 12,
        paddingVertical : 15,
      },
      city: {
        fontFamily:'WorkSans-Regular',
        fontSize: 13,
      },
      footer: {
        padding: 20,
        flexDirection: 'row',
        justifyContent: 'space-around'
      },
      button: {
        backgroundColor: '#256173',
        padding: 15,
        width: Dimensions.get('window').width*0.3,
        borderRadius:30,
        alignItems:'center'
    },
    buttonText:{
      fontWeight: 'bold',
      color: '#fff',
    }
});

//make this component available to the app
export default Destination;
