//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TextInput, TouchableOpacity, Alert } from 'react-native';
import Colors from '../components/colors';
import Icon from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';

import {BaseUrl} from '../components/url.json';
// import axios from 'axios';

// const BASE_URL = 'https://dummyapi.io/data/api';
// const APP_ID = '60ed8864a110fb2c8dc22dff';


// create a component
const Login = ({navigation}) => {

  const [email, setEmail] = React.useState('');
  const [pwd, setPwd] = React.useState('');

  const myfun = async() => {
    //Alert.alert(petname);
    await fetch(BaseUrl + 'login',{
        method : 'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "email":email,
          "password":pwd,
    })
    }).then(res => res.json())
    .then(resData => {
       console.log(resData);
       //Alert.alert(resData.msg);
       if(resData.msg === 'loggedin'){
        AsyncStorage.setItem('id', JSON.stringify(resData.data.id));
        console.log(resData.data.id);
        AsyncStorage.setItem(
          'userdetails',
          JSON.stringify({
            id: resData.data.id,
            name: resData.data.name,
            email: resData.data.email,
            password: resData.data.password,
            //age: responseJson.data.age,
            //address: responseJson.data.address,
          }),
        );
        console.log('idddd--->>',resData.data.id)
        console.log(resData.data.email)
        navigation.navigate('SettingScreen')
       }
    });
 }

  return (
    <ImageBackground source={require('../images/loginbg.png')}
      imageStyle={{
        resizeMode: "cover",
        opacity: 0.2,
        //alignSelf: "flex-end"
      }}
      style={styles.backgroundImage}>
      <View style={{ width: '100%', paddingHorizontal: 30 }}>
        <View style={styles.card}>
          <Text style={styles.login}>Login</Text>
          <Text style={styles.mainLine}>Please enter your email & {'\n'} phone</Text>

          <View style={styles.inputBox}>
            <Icon name="envelope" size={20} color='#266173' style={styles.icon} />
            <TextInput style={styles.textInputBox} 
            placeholder='email' 
            placeholderTextColor="#266173" 
            value={email}
            onChangeText={email => setEmail(email)}/>
          </View>
          <View style={styles.inputBox}>
            <Icon name="unlock-alt" size={20} color='#266173' style={styles.icon} />
            <TextInput style={styles.textInputBox} 
            placeholder='Password' 
            placeholderTextColor="#266173" 
            value={pwd}
            onChangeText={pwd => setPwd(pwd)}/>
          </View>
          <TouchableOpacity onPress={()=> alert('changing password')} style={styles.fullWidth}>
            <Text style={[styles.mainLine, styles.alignRight]}>Forgot password</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={myfun}>
            <View style={styles.button}>
              <Text style={styles.buttonText}>Login</Text>
            </View>
          </TouchableOpacity>
          
            <View style={{flexDirection:'row'}}>
              <Text style={styles.mainLine}>Don't have account? </Text>
              <TouchableOpacity onPress={()=>navigation.navigate('Register')}><Text style={[styles.mainLine, styles.link]}>Signup</Text></TouchableOpacity>
            </View>
          
        </View>
      </View>
    </ImageBackground>
  );
};

// define your styles
const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    
    //height: Dimensions.get('window').height * 1.3,
  },
  fullWidth:{
    width:'100%',
  },
  card: {
    backgroundColor: '#E6FAFF',
    alignItems: 'center',
    borderRadius: 50,
    opacity: 0.9,
    alignItems: 'center',
    width: '100%',
    paddingHorizontal: 20,
    paddingVertical: 50,
    shadowOpacity: 0.5,
    shadowColor: 'black',
    elevation: 8,
  },
  login: {
    fontFamily: 'AbrilFatface-Regular',
    fontSize: 34,
    color: Colors.primary,
  },
  mainLine: {
    textAlign: 'center',
    marginTop: 25,
    fontSize: 14,
    color: Colors.accent,
  },
  alignRight: {
    alignContent: 'flex-end',
    textAlign: 'right',
    width: '100%'
  },
  button: {
    backgroundColor: Colors.primary,
    marginTop: 50,
    padding: 15,
    //alignItems: 'center',
    borderRadius: 10,
    width: '100%',
    minWidth: 330,
    maxWidth: '100%',
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center'
  },
  link: {
    color: '#0d70f2',
    fontSize: 15,
    textAlign:'center',
    marginLeft: 7,
  },
  icon: {
    marginHorizontal:5,
    marginTop: 15,
    position: 'absolute',
    left: 0,
    top: 0,
  },
  inputBox: {
    flexDirection: 'row',
    marginTop:20,
    borderBottomWidth: 0.8,
    width:'100%',
    maxWidth:'100%',
    position: 'relative',
    paddingLeft: 40,
    height: 50,
  },
  textInputBox:{
    width: '100%',
    color: '#266173'
  },

});

//make this component available to the app
export default Login;
