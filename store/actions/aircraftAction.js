export const SET_AIRCRAFTS = "SET_AIRCRAFTS";

export const setAircrafts = (aircrafts) => ({
  type: SET_AIRCRAFTS,
  payload: aircrafts,
});
