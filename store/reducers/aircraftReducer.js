import {Alert} from 'react-native'
import {setAircrafts, SET_AIRCRAFTS } from "../actions/aircraftAction"
import {BaseUrl} from '../../components/url.json';
import AsyncStorage from '@react-native-community/async-storage';

const initialState = {
    data: ['codeinit', 'Microsoft'],
    getdate: 'Codeinht'
  }

export const aircraftReducer = (state = initialState, action) => {
    switch(action.type){
        case SET_AIRCRAFTS: {
            return {...state, data: action.payload}
          }
          default:
            return state
    }
}

export const saveAircrafts = () => async (dispatch, getState) => {
    let user = await AsyncStorage.getItem('userdetails');
    user = JSON.parse(user);
    const data = getState().data;
    await fetch(BaseUrl+'save_logbook', {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    Alert.alert("Success")

    //Alert.alert('hello');
  } 
