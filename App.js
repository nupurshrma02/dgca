import * as React from 'react';
import {Alert, Button, Text, View} from 'react-native';
//import {NavigationContainer} from '@react-navigation/native';
//import { createStackNavigator } from '@react-navigation/stack';
import SQLite from 'react-native-sqlite-storage';


const db = SQLite.openDatabase(
  {
    name: 'autoflightlogdb',
    location: 'default',
  },
  () => {
    //alert('successfully executed');
  },
  error => {
    alert('db error');
  },
);

const prePopulateddb = SQLite.openDatabase(
  {
    name: 'autoflightlogdb.db',
    createFromLocation: 1,
    //location: 'www/AutoFlightLogDB_V5.db',
  },
  () => {
    alert('successfully connected to prepopulated db');
  },
  error => {
    alert('db error');
  },
);

// const Stack = createStackNavigator();

// const HomeScreen = ({navigation}) => {
//   const [datalength, setDatalength] = React.useState();
//   const createTable = () => {
//     db.transaction(tx => {
//       tx.executeSql(
//         'CREATE TABLE AirlineTable ( airline_name	TEXT, loginUrl	TEXT)',
//       );
//     });

//     db.transaction(tx => {
//       tx.executeSql(
//         'CREATE TABLE AirportTable ( airport_name	TEXT, icao_name	TEXT)',
//       );
//     });
//     alert('table created');
//   };

//   const insertQuery = () => {
//     db.transaction(tx => {
//       tx.executeSql(
//         'INSERT INTO AirlineTable (airline_name, loginUrl) VALUES ("Vijay", "hello world")',
//       );
//     });

//     db.transaction(tx => {
//       tx.executeSql(
//         'INSERT INTO AirportTable (airport_name, icao_name) VALUES ("Indiragandhi airport", "DEL")',
//       );
//     });
//     alert('insertion executed');
//   };
  const getPrepopulatedDataQuery = () => {
    console.log('testing');
    db.transaction(tx => {
      tx.executeSql('SELECT * from Airline_table', [], (tx, result) => {
        console.log('Default DB', result);
      });
    });
    prePopulateddb.transaction(tx => {
      tx.executeSql('SELECT * from users', [], (tx, result) => {
        console.log('Custom DB', result);
      });
    });
  };

  const getDataQuery = () => {
    let data = [];
    db.transaction(tx => {
      tx.executeSql('SELECT * from Airline_table', [], (tx, result) => {
        if (result.rows.length > 0) {
          alert('data available ');
        }
        console.log(result);
        setDatalength(result.rows.length);
        for (let i = 1; i <= result.rows.length; i++) {
          //console.log('name: ', result.rows.item(i).airline_name, 'loginlink: ', result.rows.item(i).loginUrl)
          //setDatalength(result.rows.item(i));
          data.push({
            name: result.rows.item(i).airline_name,
            loginlink: result.rows.item(i).loginUrl,
          });
        }
        //console.log(result);
        //console.log(result.rows.item(0).airline_name)
        // result.rows.item.map((index, content) => {
        //   data.push({name:content.airline_name, loginlink: content.loginUrl})
        // });
        // );
      });
    });
    console.log(data);
  };


const ProfileScreen = ({navigation, route}) => {
  return <Text>This is {route.params.name}'s profile</Text>;
};

const App = () => {
  return (
    
      <View>
        <Button
          title="Go to Jane's profile"
          onPress={() => navigation.navigate('Profile', {name: 'Jane'})}
        />
        <Text />
        <Button title="Create Table" onPress={() => createTable()} />
        <Text />
        <Button title="Insert Data" onPress={() => insertQuery()} />
        <Text />
        <Button title="Get Data" onPress={() => getDataQuery()} />
        <Text /><Text /><Text />
        <Button title="Get PrePopulated Data" onPress={() => getPrepopulatedDataQuery()} />
        {/* <Text> Length: {datalength}</Text> */}
      </View>
    );
};
export default App;